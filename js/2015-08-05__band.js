/**
 * Created by user on 2015-08-06.
 */

/*
$('select').select2({
 minimumResultsForSearch: Infinity,
 theme : ($(this).attr('class') === 'band-nav') ? 'bandnav' : 'band'
});
 */

$('select').each(function(){
    var _this = $(this);

    _this.select2({
        minimumResultsForSearch: Infinity,
        theme: 'band',
        placeholder: "Select a State"

        //theme : ($(this).attr('class') === 'band-nav') ? 'bandnav' : 'band'
    });
});



$('.progress').each(function(){
    var _this = $(this),
        _parent=_this.parent();
        _parentPadding=parseInt(_parent.css('padding-left'));
        _this.width(_parent.width()-($('.zero').width()+$('.finish').width())-6+"px");

    _this.progressbar({
        value: 30,
        create: function(e,ui){
            _this.after($('<strong/>',{

                'class' : 'currentNumber',
                'style' :
                    'left:'
                    +((_this.width()/100)*$(e.target).attr('aria-valuenow')
                    + $('.zero').width()-3
                    +_parentPadding)
                    +'px;',
                'text' : $(e.target).attr('aria-valuenow')
            }));

            _this.find('.ui-progressbar-value')
        }
    })
});
$('#gnb>li>a').on('click',function(e){
    e.preventDefault();
    var _this=$(this),
        _sub=_this.find('ul').length;

    if (_this.hasClass('_active')){
        _this.next('ul').slideUp();
        _this.removeClass('_active')
    }
    else {
        _this.parents('.gnb').find('a').removeClass('_active');
        _this.parents('.gnb').find('ul').slideUp();
        _this.addClass("_active");
        _this.next('ul').slideDown();
    }
});


$("#gnb ul ._active").parents('ul').show()
                     .closest('ul').parent('li').children('a').addClass('_active');


$('#gnb>li ul a').on('click',function(){
    var _this=$(this);
    _this.parent('li').siblings('li').find('a').removeClass('_active')
    _this.addClass("_active");
    return false;
});


/*


var double_height=[];
$('[data-layout]>div').each(function(i){
    double_height[i]=$(this).height();
});
$('[data-layout]>div').css("height",Math.max.apply(0, double_height));
*/



var jqDatepicker = {
    weekHeader : 'Wk',
    dateFormat : 'yy-mm-dd', //형식(2012-03-03)
    autoSize : true, //오토리사이즈(body등 상위태그의 설정에 따른다)
    //changeMonth : true, //월변경가능
    //changeYear : true, //년변경가능
    showMonthAfterYear : true //년 뒤에 월 표시
    //maxDate : '0d',
    //minDate : '-90d'
    //buttonImageOnly: true, //이미지표시
    //buttonImage: './common/img/icon_calendar.png', //이미지주소
    //showOn: "both", //엘리먼트와 이미지 동시 사용
    ///yearRange : '2012:2013'
}
$('input[data-type="date"]').datepicker(jqDatepicker);

/*$('.scroll-wrap').perfectScrollbar();*/
