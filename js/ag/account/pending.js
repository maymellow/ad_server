function printHistoryList(accountId) {
	wise.ajax({
		url : rootPath + '/pag/account/get_account_history_list.ajax',
		data : {
			accountId : accountId,
			position : 'ag'
		},
		dataType : 'HTML',
		success : function(html) {
			$('#approve_history').html(html).animate({
				scrollTop : $('#approve_history > ul').height()
			}, 'slow');

		}
	});
}

function setPeriod() {
	var time = moment($('#contract_period_end').val(), 'YYYY.MM.DD') - moment($('#contract_period_start').val(), 'YYYY.MM.DD');
	if (time < 0) {
		$('#contract_period_end').val($('#contract_period_start').val());
	}
}

function viewToggleStart(_boolean, $that) {
	var _targetName = $that.closest('[data-field-disable]').attr('data-field-disable');
	if (_boolean) {
		$('[data-field-disable]').each(function() {
			var $this = $(this);
			var _fieldName = $this.attr('data-field-disable');
			switch (_fieldName) {
			case 'true':
				$this.addClass('_uncorrectable');
				$this.find('div button').attr('disabled', true);
				break;
			default:
				if (_fieldName == _targetName) {
					$this.addClass('_correct');
					$this.find('div button').attr('disabled', false);
				} else {
					$this.addClass('_uncorrectable');
					$this.find('div button').attr('disabled', true);
				}
				break;
			}
		});
	} else {
		$('[data-field-disable]').each(function() {
			var $this = $(this);
			var _fieldName = $this.attr('data-field-disable');
			switch (_fieldName) {
			case 'true':
				$this.removeClass('_uncorrectable');
				$this.find('div button').attr('disabled', false);
				break;
			default:
				if (_fieldName == _targetName) {
					$this.removeClass('_correct');
					$this.find('div button').attr('disabled', true);
				} else {
					$this.removeClass('_uncorrectable');
					$this.find('div button').attr('disabled', false);
				}
				break;
			}
		});
	}
}

function editAgencyInfoStart($this) {
	viewToggleStart(true, $this);

	$('#business_number').val($('#business_number_view').text());
	$('#address').val($('#address_view').text());

	$('#corporate_licens_list_edit').html($('#corporate_licens_list_view').html()) //
	.attr('data-remove-files', '') //
	.find('._hide').each(function() {
		$(this).removeClass('_hide');
	});
	$('#corporate_licens_list_edit').find('li a').each(function() {
		$(this).text($(this).data('title'));
	})
	$('#contract_period_start').val($('#contract_period_start_view').text());
	$('#contract_period_end').val($('#contract_period_end_view').text());
	$('#corporate_licens_list_edit').find('li a').tooltip(ellipsisTooltip);
}

function editManagerInfoStart($this) {
	viewToggleStart(true, $this);

	$('#manager_name').val($('#manager_name_view').text());
	$('#manager_email').val($('#manager_email_view').text());
	$('#manager_phone').val($('#manager_phone_view').text());

	$('#empl_cer_file_list_edit').html($('#empl_cer_file_list_view').html()) //
	.attr('data-remove-files', '') //
	.find('._hide').each(function() {
		$(this).removeClass('_hide');
	});
}

function editPaymentInfoStart($this) {
	viewToggleStart(true, $this);

	$('#payment_name').val($('#payment_name_view').text());
	$('#payment_email').val($('#payment_email_view').text());
	$('#payment_phone').val($('#payment_phone_view').text());
}

function SubmitAgencyInfo($this) {
	var $form = $this.closest('[data-field-disable]').find('form');

	if (wise.isValidate($form)) {
		if (fileComponent.valid()) {
			var _formData = fileComponent.makeFormData($form);
			_formData.append('p', 'pendign');
			wise.ajax({
				url : rootPath + '/pag/account/agency/me/edit_agency_info.ajax',
				data : _formData,
				processData : false,
				contentType : false,
				success : function(data) {
					location.reload();
					// viewToggleStart(false, $this);
					// printHistoryList(SESSION.id);
					//
					// $('.approval-status .status > span').attr('class', '_pending').text('Pending now');
					//
					// $('#business_number_view').text($('#business_number').val());
					// $('#address_view').text($('#address').val());
					// $form.find('[data-view-name=file_list]').html($form.find('[data-view-name=edit_file_list]').html()) //
					// .find('.btn-delete').each(function() {
					// var _this = $(this);
					// _this.addClass('_hide');
					// // 업로드된 파일 버튼기능 바꾸기.
					// if (_this.val() * 0 != 0) {
					// $.each(data, function(imgName, imgId) {
					// if (_this.val() == imgName) {
					// _this.val(imgId);
					// _this.attr('onclick', 'removeFile($(this));');
					// }
					// });
					// }
					// });
					// $('#corporate_licens_list_view').find('li a').each(function() {
					// $(this).text($(this).data('title'));
					// })
					// $('#corporate_licens_list_view').find('li a').tooltip(ellipsisTooltip);

				} // end success
			}); // end ajax
		}
	} // end if
}

function SubmitManagerInfo($this) {
	var $form = $this.closest('[data-field-disable]').find('form');

	if (wise.isValidate($form)) {
		wise.ajax({
			url : rootPath + '/pag/account/agency/me/edit_manager_info.ajax',
			data : $form.serialize(),
			dataType : 'text',
			success : function(data) {
				viewToggleStart(false, $this);
				printHistoryList(SESSION.id);

				$('#manager_name_view').text($('#manager_name').val());
				$('#manager_email_view').text($('#manager_email').val());
				$('#manager_phone_view').text($('#manager_phone').val());
				// $form.find('[data-view-name=file_list]').html($form.find('[data-view-name=edit_file_list]').html()) //
				// .find('.btn-delete').each(function() {
				// var _this = $(this);
				// _this.addClass('_hide');
				// // 업로드된 파일 버튼기능 바꾸기.
				// if (_this.val() * 0 != 0) {
				// $.each(data, function(imgName, imgId) {
				// if (_this.val() == imgName) {
				// _this.val(imgId);
				// _this.attr('onclick', 'removeFile($(this));');
				// }
				// });
				// }
				// });

			} // end success
		}); // end ajax
	} // end if
}

function SubmitPaymentInfo($this) {
	var $form = $('#payment_info_form');

	if (wise.isValidate($form)) {
		wise.ajax({
			url : rootPath + '/pag/account/agency/me/edit_payment_info.ajax',
			data : $form.serialize(),
			dataType : 'text',
			success : function() {
				viewToggleStart(false, $this);
				printHistoryList(SESSION.id);

				$('#payment_name_view').text($('#payment_name').val());
				$('#payment_email_view').text($('#payment_email').val());
				$('#payment_phone_view').text($('#payment_phone').val());
			}
		});
	}
}