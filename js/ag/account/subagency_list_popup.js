function addSubAgencyPopup() {
	wisePopupComponent.popup('/ag/account/popup.popup?p=addSubAgency', {
		addClass : "popup-add-sub-agency",
		popupTitle : 'Add Sub Agency',
		onFormOpenned : function() {
			addAdvertiserComponent.initSetting();
		},
		okBtnCallBack : function() {
			createSubAgency();
			return false;
		}
	});
}

function createSubAgency() {
	var $form = $("#sub_ag_form");
	if (wise.isValidate($form)) {
		if (createSubAgencyValidate()) {
			wise.ajax({
				url : rootPath + "/ag/account/subagency/create.ajax",
				data : fileComponent.makeFormData($form),
				processData : false,
				contentType : false,
				dataType : 'text',
				success : function() {
					printSubAgencyList({
						search : _search,
						status : $("#status").val()
					});
					alert('Account request has been submitted.');
					wisePopupComponent.x();
				}
			});
		}
	}
}

function createSubAgencyValidate() {
	if (fileComponent.valid() == false) {
		return false;
	}
	return true;
}

function subAgencyInfoPopup(accountId, status) {
	if (status == 3) {
		if (SESSION.choosedRole == 'ADMIN') {
			subAgencyInfoPopupForAdmin(accountId);
		} else {
			wisePopupComponent.popup('/ag/account/popup.popup?p=subAgencyInfoRead', {
				addClass : 'popup-sub-agency-account-info',
				popupTitle : 'View Sub Agency',
				removeFooter : true,
				onFormOpenned : function() {
					$('.popup .file__list').find('li a').tooltip(ellipsisTooltip);
					$('.popup .add-advertiser__tr').find('li span.dotdotdot_ele').tooltip(ellipsisTooltip);

				}
			}, {
				accountId : accountId
			});
		}
	} else {
		editSubAgencyInfoPopup(accountId);
	}
}

function editSubAgencyInfoPopup(accountId) {
	wisePopupComponent.popup('/ag/account/popup.popup?p=subAgencyInfo', {
		addClass : 'popup-view-sub-agency',
		popupTitle : 'View Sub Agency',
		removeFooter : true,
		beforeOpend : function() {
			wisePopupComponent.x();
		},
		onFormOpenned : function() {
			addAdvertiserComponent.initSetting();
			$('.popup .approved-status').find('span').tooltip(ellipsisTooltip);
			$('.popup #adv_list_view').find(' li span.dotdotdot_ele').tooltip(ellipsisTooltip);
			$('.popup .file__list').find('li a').tooltip(ellipsisTooltip);

			$('.popup-view-sub-agency').each(function() {
				historyResize($(this));
			});
			$('#approve_history').animate({
				scrollTop : $('#approve_history > ul').height()
			}, 'slow');

		}
	}, {
		accountId : accountId
	});
}

function removeAdvertiser($this) {
	var _advId = $this.attr('data-adv_id');
	if (typeof _advId != 'undefined') {
		var $ul = $this.closest('ul');
		var _r = $ul.attr('data-adv_remove_list');
		if (_r == '' || typeof _r == "undefined") {
			$ul.attr('data-adv_remove_list', _advId);
		} else {
			$ul.attr('data-adv_remove_list', '%s,%s'.format(_r, _advId));
		}
	} else {
		// 검색값 제거
		// var v = eval($this.closest('li').find('input').val());
		var v = parseInt($this.closest('li').find('input').val());
		var i = AUTOCOMPLETE_EXIST_KEY_LIST.indexOf(v);
		AUTOCOMPLETE_EXIST_KEY_LIST.splice(i, 1);
	}
	$this.closest('li').remove();
}

function viewToggleStart(_boolean, $that) {
	var _targetName = $that.closest('[data-field-disable]').attr('data-field-disable');
	if (_boolean) {
		$('[data-field-disable]').each(function() {
			var $this = $(this);
			var _fieldName = $this.attr('data-field-disable');
			switch (_fieldName) {
			case 'true':
				$this.addClass('_uncorrectable');
				$this.find('div button').attr('disabled', true);
				break;
			default:
				if (_fieldName == _targetName) {
					$this.addClass('_correct');
					$this.find('div button').attr('disabled', false);
				} else {
					$this.addClass('_uncorrectable');
					$this.find('div button').attr('disabled', true);
				}
				break;
			}
		});
	} else {
		$('[data-field-disable]').each(function() {
			var $this = $(this);
			var _fieldName = $this.attr('data-field-disable');
			switch (_fieldName) {
			case 'true':
				$this.removeClass('_uncorrectable');
				$this.find('div button').attr('disabled', false);
				break;
			default:
				if (_fieldName == _targetName) {
					$this.removeClass('_correct');
					$this.find('div button').attr('disabled', true);
				} else {
					$this.removeClass('_uncorrectable');
					$this.find('div button').attr('disabled', false);
				}
				break;
			}
		});
	}
}

function editSubAgencyInfoStart($this) {
	viewToggleStart(true, $this);
	addAdvertiserComponent.init();
}

function SubmitSubAgencyInfo(sagId, $this) {
	var _formData = $('#edit_add_adv_form').serialize();
	_formData += '&subAgencyId=' + sagId;
	_formData += '&removeAdvertiserList=' + $('#add_advertiser_view').attr('data-adv_remove_list');

	wise.ajax({
		url : rootPath + '/ag/account/subagency/edit_advertiser_list.ajax',
		data : _formData,
		dataType : 'text',
		success : function() {
			// wisePopupComponent.x();
			editSubAgencyInfoPopup(sagId);
		}
	});
}

function editSubAgencyManagerInfoStart($this) {
	viewToggleStart(true, $this);

	$('#sag_manager_name').val($('#sag_manager_name_view').text());
	$('#sag_manager_email').val($('#sag_manager_email_view').text());
	$('#sag_manager_phone').val($('#sag_manager_phone_view').text());

	$('#empl_cer_file_list_edit').html($('#empl_cer_file_list_view').html()) //
	.attr('data-remove-files', '') //
	.find('._hide').each(function() {
		$(this).removeClass('_hide');
	});
	$('#empl_cer_file_list_edit').find('li a').each(function() {
		$(this).text($(this).data('title'));
	})
	$('#empl_cer_file_list_edit').find('li a').tooltip(ellipsisTooltip);

}

function SubmitSubAgencyManagerInfo(sagId, $this) {

	var $form = $('#sag_manager_info_form');
	var _formData = fileComponent.makeFormData($form);
	_formData.append('targetId', sagId);

	if (wise.isValidate($form)) {
		if (fileComponent.valid()) {
			wise.ajax({
				url : rootPath + '/ag/account/subagency/edit_manager_info.ajax',
				data : _formData,
				processData : false,
				contentType : false,
				success : function(data) {
					// wisePopupComponent.x();
					editSubAgencyInfoPopup(sagId);
				}
			});
		}
	}

}

function subAgencyInfoPopupForAdmin(accountId) {
	wisePopupComponent.popup('/ag/account/popup.popup?p=subAgencyInfoRead', {
		addClass : 'popup-sub-agency-account-info',
		popupTitle : 'View Sub Agency',
		okBtnText : 'Approve',
		cancelBtnText : 'Disapprove',
		beforeOpend : function() {
			wisePopupComponent.x();
		},
		okBtnCallBack : function() {
			subAgencyApprove(accountId);
			return false;
		},
		cancelBtnCallBack : function() {
			disapprovePopup(accountId, 'sub agency');
		},
		onFormOpenned : function() {
			$('.popup .file__list').find('li a').tooltip(ellipsisTooltip);
			$('.popup .add-advertiser__tr').find('li span').tooltip(ellipsisTooltip);

		}

	}, {
		accountId : accountId
	});
}

function subAgencyApprove(accountId) {
	wise.ajax({
		url : rootPath + '/sa/account/subagency/approve.ajax',
		data : {
			subagencyId : accountId
		},
		dataType : 'text',
		success : function() {
			printAgencyList({
				status : $('#status').val()
			});

			var _name = $('.popup .popup__container .sub-agency-info__table .name__tr td').text();
			alert(_name + ' has been approved.');

			wisePopupComponent.x();
		}
	});
}