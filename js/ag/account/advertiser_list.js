//$(function() {
//	$('input[name=search]').on('keyup', function(e) {
//		// if (e.keyCode == 13) {
//		_search = $(this).val();
//		printAdvertiserList({
//			search : _search,
//			status : $("#status").val()
//		});
//		// }
//	});
//
//	$("#status").on("change", function() {
//		printAdvertiserList({
//			search : _search,
//			status : $(this).val()
//		});
//	});
//}); // end ready func

function printAdvertiserList(data) {
	if (data === undefined) {
		data = {};
	} else if (data == 'search') {
		data = {
			status : $('#status').val()
		};
	}
	wise.ajax({
		url : rootPath + SESSION.roleUrl + '/account/advertiser_list.ajax',
		data : data,
		dataType : 'HTML',
		success : function(html) {
			if ($('#advertiser_list_table').hasClass('dataTable')) {
				$('#advertiser_list_table').DataTable().destroy();
			}
			$('#print_advertiser_list').html(html);
			var table = $('#advertiser_list_table').dataTable({
				'info' : false,
				'paging' : false,
				'bLengthChange' : false,
				"order" : [ [ 1, "desc" ] ],
				'aoColumnDefs' : [ {
					'bSearchable' : false,
					'aTargets' : [1, 2, 3, 4, 5]
				} ],
				'retrieve' : true,
				'autoWidth' : false
			});
			$('#advertiser_list_table_filter').hide();
			var ex = $('#advertiser_list_table').DataTable();
			$('#advertiser_search_input').on('keyup', function() {
				ex.search(this.value).draw();
			});
			
			
			$('#print_advertiser_list').find('.dotdotdot_ele').tooltip(ellipsisTooltip);
		}
	});
}