function printHistoryList(accountId) {
	wise.ajax({
		url : rootPath + '/sag/account/get_account_history_list.ajax',
		data : {
			accountId : accountId,
			position : 'ag'
		},
		dataType : 'HTML',
		success : function(html) {
			$('#approve_history').html(html).animate({
				scrollTop : $('#approve_history > ul').height()
			}, 'slow');
		}
	});
}

function editManagerInfoStart(_boolean) {
	if (_boolean) {
		$('[data-field-disable=login_info]').addClass('_uncorrectable').attr('disabled', true);
		$('[data-field-disable=manager_info]').addClass('_correct');
		$('#name').val($('#name_view').text());
		$('#email').val($('#email_view').text());
		$('#phone').val($('#phone_view').text());

		$('#empl_cer_file_list_edit').html($('#empl_cer_file_list_view').html()) //
		.attr('data-remove-files', '') //
		.find('._hide').each(function() {
			$(this).removeClass('_hide');
		});
		$('#empl_cer_file_list_edit').find('li a').each(function() {
			$(this).text($(this).data('title'));
		})

		$('#empl_cer_file_list_edit').find('li a').tooltip(ellipsisTooltip);
	} else {
		$('[data-field-disable=login_info]').removeClass('_uncorrectable').attr('disabled', false);
		$('[data-field-disable=manager_info]').removeClass('_correct');
	}
}
function submitManagerInfo() {
	var $form = $('#manager_info_form');

	if (wise.isValidate($form)) {
		if (fileComponent.valid()) {
			var _formData = fileComponent.makeFormData($form);
			wise.ajax({
				// url : rootPath + '/ag/sag/account/agency/me/edit_manager_info.ajax',
				url : rootPath + '/sag/account/agency/me/edit_manager_info.ajax',
				data : _formData,
				processData : false,
				contentType : false,
				success : function(data) {
					location.reload();
					// editManagerInfoStart(false);
					// printHistoryList(ACCOUNT_ID);
					//
					// $('#name_view').text($('#name').val());
					// $('#email_view').text($('#email').val());
					// $('#phone_view').text($('#phone').val());
					// $form.find('[data-view-name=file_list]').html($form.find('[data-view-name=edit_file_list]').html()) //
					// .find('.btn-delete').each(function() {
					// var _this = $(this);
					// _this.addClass('_hide');
					// // 업로드된 파일 버튼기능 바꾸기.
					// if (_this.val() * 0 != 0) {
					// $.each(data, function(imgName, imgId) {
					// if (_this.val() == imgName) {
					// _this.val(imgId);
					// _this.attr('onclick', 'fileComponent.remove($(this));'); // removeFile($(this));
					// }
					// });
					// }
					// });
					// $('#empl_cer_file_list_view').find('li a').each(function() {
					// $(this).text($(this).data('title'));
					// })
					// $('.file__list').find('li a').tooltip(ellipsisTooltip);
				} // end success
			}); // end ajax
		}
	} // end if

}

function historyResize(ele) {
	var _this = ele, _information = _this.find('.manager-commission'), _record = _this.find('.history');

	_record.find('.history-wrap').height((_information.outerHeight() - _record.outerHeight()) + _record.find('.history-wrap').outerHeight());

	_information.resize(function() {
		if (_information.outerHeight() !== _record.outerHeight()) {
			_record.find('.history-wrap').height((_information.outerHeight() - _record.outerHeight()) + _record.find('.history-wrap').outerHeight());
		}
	});
};

function modifyEmailNotificationShow($this) {
	$this.hide();
	$('#email_notification_save').show();
	$('.email_check_ele').removeAttr('disabled');
}

function cancelEmailNotificationOption(){
	$('#email_notification_modify').show();
	$('#email_notification_save').hide();
	$('.email_check_ele').attr('disabled', 'disabled');
}

function modifyEmailNotificationSave() {
	if(wise.isValidate($('#notification_form'))){
		wise.ajax({
			type : 'GET',
			url : rootPath + '/sag/account/edit/notification.ajax',
			dataType : 'JSON',
			data : $('#notification_form').serialize(),
			success : function(data) {
				if(data){
					$('#limit_per').text($('#limit_per_text').val());
					cancelEmailNotificationOption();
				}else{
					alert('실패 ㅠㅠ');
				}
			}
		});
	}
}