$(function() {
	$('input[name=search]').on('keyup', function(e) {
		// if (e.keyCode == 13) {
		_search = $(this).val();
		printSubAgencyList({
			search : _search,
			status : $("#status").val()
		});
		// }
	});

	$("#status").on("change", function() {
		printSubAgencyList({
			search : _search,
			status : $(this).val()
		});
	});

}); // end ready func

function printSubAgencyList(data) {
	if (data === undefined) {
		data = {};
	}
	wise.ajax({
		url : rootPath + '/ag/account/popup.popup?p=subAgencyList',
		data : data,
		dataType : 'HTML',
		success : function(html) {
			$('#print_subagency_list').html(html);
			var table = $('#sub_agency_table').dataTable({
				'info' : false,
				'paging' : false,
				'bLengthChange' : false,
				"order": [[ 0, "desc" ]],
				'aoColumnDefs' : [ {
					'bSearchable' : false,
					'aTargets' : [ 1, 2, 3, 4]    //  검색에서 제외할 컬럼 지정
				} ],
				'retrieve' : true,
				'autoWidth':false
			});
			 $('#sub_agency_table_filter').hide();
			var ex = $('#sub_agency_table').DataTable();
			$('#sub_agency_table_search_box').on('keyup', function() {
				ex.search(this.value).draw();
			});
			$('#print_subagency_list > table').find('.dotdotdot_ele').tooltip(ellipsisTooltip);
		}
	});
}