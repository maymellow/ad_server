function approveAdvertiserPopup(accountId, status) {
	if (status == 3) {
		if (SESSION.role == 'AGENCY') {
			advertiserInfoPopup(accountId);
		} else {
			approveAdvertiserPopup_approve(accountId);
		}
	} else {
		approveAdvertiserPopup_edit(accountId);
	}
}

function advertiserInfoPopup(accountId) {
	wisePopupComponent.popup('/ag/account/popup.popup?p=advertiserInfo', {
		addClass : 'popup-approve-advertiser-account',
		popupTitle : 'View Advertiser',
		removeFooter : true,
		onFormOpenned : function() {
			$('.popup .file__list').find('li a').tooltip(ellipsisTooltip);
			wise.activeTooltipForIcon();
		}
	}, {
		accountId : accountId
	});
}

function approveAdvertiserPopup_approve(accountId) {
	wisePopupComponent.popup('/rev/account/popup.popup?p=approveAdvertiser', {
		addClass : 'popup-approve-advertiser-account',
		popupTitle : 'Approve Advertiser Account',
		okBtnText : 'Approve',
		cancelBtnText : 'Disapprove',
		beforeOpend : function() {
			wisePopupComponent.x();
		},
		onFormOpenned : function() {
			wise.activeTooltipForIcon();
			$('.popup .file__list').find('li a').tooltip(ellipsisTooltip);
		},
		okBtnCallBack : function() {
			approveAdvertiserProcess({
				accountId : accountId,
				comment : 'approved account.',
				isApprove : true
			});
			return false;
		},
		cancelBtnCallBack : function() {
			disapproveAdvertiserPopup(accountId);
		}
	}, {
		accountId : accountId
	});
}

function approveAdvertiserPopup_edit(accountId) {

	var url;
	if (SESSION.role == 'REVIEWER') {
		url = '/rev/account/editAdvertiser.ajax?p=editAdvertiser';
	} else {
		url = '/ag/account/popup.popup?p=editAdvertiser';
	}
	// wisePopupComponent.popup('/ag/account/popup.popup?p=editAdvertiser', {
	wisePopupComponent.popup(url, {
		addClass : 'popup-view-advertiser',
		popupTitle : 'View Advertiser',
		removeFooter : true,
		beforeOpend : function() {
			wisePopupComponent.x();
		},
		onFormOpenned : function() {
			if ($('#creditAmount').val() != '') {
				$('input[name=credit_amount_limit][value=Limit]').prop('checked', true).trigger('change');
			} else {
				$('input[name=credit_amount_limit][value=Limitless]').prop('checked', true).trigger('change');
			}
			printApproveHistoryList(accountId);

			// 시은님 테스트용 start
			var historyResize = function(ele) {
				var _this = ele, _information = _this.find('.information'), _record = _this.find('.record');

				_record.find('.history-wrap').height((_information.outerHeight() - _record.outerHeight()) + _record.find('.history-wrap').outerHeight());

				_information.resize(function() {
					if (_information.outerHeight() !== _record.outerHeight()) {
						_record.find('.history-wrap').height((_information.outerHeight() - _record.outerHeight()) + _record.find('.history-wrap').outerHeight());
					}
				});
			};

			$('.popup-view-agency, .popup-view-advertiser').each(function() {
				historyResize($(this));
			});

			// 시은님 테스트용 end
			$('.popup #status_view').find('.approved-status span').tooltip(ellipsisTooltip);
			$('.popup .file__list').find('li a').tooltip(ellipsisTooltip);

			wise.activeTooltipForIcon();
		}
	}, {
		accountId : accountId
	});
}

function disapproveAdvertiserPopup(accountId) {
	wisePopupComponent.popup('/rev/account/popup.popup?p=disapproveAdvertiser', {
		addClass : 'popup-disapprove-advertiser-account',
		popupTitle : 'Disapprove Advertiser Account',
		okBtnText : 'Back',
		okBtnStyle : 'fail',
		cancelBtnText : 'OK',
		cancelBtnStyle : 'success',
		beforeOpend : function() {
			wisePopupComponent.x();
		},
		okBtnCallBack : function() {
			approveAdvertiserPopup_approve(accountId);
		},
		cancelBtnCallBack : function() {
			approveAdvertiserProcess({
				accountId : accountId,
				comment : $('#comment').val(),
				isApprove : false
			});
			return false;
		}
	}, {
		accountId : accountId
	});
}

function approveAdvertiserProcess(approveData) {
	wise.ajax({
		url : rootPath + '/rev/account/advertiser/approve.ajax', // ?isApprove=true
		data : approveData,
		dataType : 'json',
		success : function(data) {
			printAdvertiserList({
				status : $('#status').val()
			});
			if (approveData.isApprove) {
				alert(data.businessName + ' has been approved.');
			} else {
				alert(data.businessName + ' has been disapproved.');
			}
			wisePopupComponent.x();
		}
	});
}

function printApproveHistoryList(accountId) {
	wise.ajax({
		url : rootPath + SESSION.roleUrl + '/account/get_account_history_list.ajax',
		type : 'POST',
		data : {
			accountId : accountId,
			position : 'adv'
		},
		dataType : 'HTML',
		success : function(html) {
			// $('#approve_history').perfectScrollbar();
			$('#approve_history').html(html).animate({
				scrollTop : $('#approve_history ul').height()
			}, 'slow');
		}
	});
}

function insertComment(accountId) {
	var comment = $('#comment_text').val();
	if (comment != '') {
		wise.ajax({
			url : rootPath + '/ag/account/advertiser/insertcomment.ajax',
			data : {
				targetId : accountId,
				comment : comment
			},
			dataType : 'text',
			success : function() {
				$('#comment_text').val('');
				printApproveHistoryList(accountId);
			}
		});
	} else {
		$('#comment_text').addClass('parsley-error');
		$('#comment_text').tooltip({
			animation : false,
			container : 'body',
			placement : 'top-left',
			title : 'This value is required.'
		});
		$('#comment_text').focus();
		return;
	}
}

function addAdvertiserPopup() {
	wisePopupComponent.popup('/ag/account/popup.popup?p=addAdvertiser', {
		addClass : 'popup-add-advertiser',
		popupTitle : 'Add Advertiser',
		onFormOpenned : function() {
			// 툴팁 활성화
			wise.activeTooltipForIcon();
			// band link init
			linkBandComponent.change('yes');
		},
		okBtnCallBack : function() {
			createAdvertiser();
			return false;
		}
	});
}

function createAdvertiser() {
	var $form = $('#adv_form');
	if (wise.isValidate($form)) {
		if (advInfoValidate()) {
			wise.ajax({
				url : rootPath + '/ag/account/advertiser/create.ajax',
				data : fileComponent.makeFormData($form),
				processData : false,
				contentType : false,
				dataType : 'text',
				success : function() {
					printAdvertiserList({
						status : $('#status').val()
					});
					alert('Account request has been submitted.');
					wisePopupComponent.x();
				}
			});
		}
	}
}

function viewToggleStart(_boolean, $that) {
	if (_boolean) {
		$that.closest('.advertiser-info').addClass('_correct');
		$('.popup .record .comment').addClass('_uncorrectable').find('button').attr('disabled', true);
	} else {
		$that.closest('.advertiser-info').removeClass('_correct');
		$('.popup .record .comment').removeClass('_uncorrectable').find('button').attr('disabled', false);
	}
}

function editAdvertiserInfoStart($this) {
	viewToggleStart(true, $this);

	// role
	if ($('#adv_role_view').data('role') == 'ADVERTISER') {
		$('#role2').prop('checked', true);
	} else {
		$('#role1').prop('checked', true);
	}

	// name
	$('#business_name').val($('#adv_name_view').data('name'));
	$('#name_validation').val($('#adv_name_view').data('name'));

	// link band
	linkBandComponent.init();

	// Band Profile
	bandProFileComponent.init();

	// industry
	industryComponent.init();

	// app
	appComponent.init();

	// corp number
	$('#business_number').val($('#adv_corp_number_view').data('corp_number'));

	// file
	fileComponent.init('c');
}

function submitAdvertiserInfo($this, advId) {
	var $form = $('#adv_info_form');
	if (wise.isValidate($form)) {
		if (advInfoValidate()) {
			var _formData = fileComponent.makeFormData($form);
			_formData.append('advId', advId);
			wise.ajax({
				url : rootPath + '/ag/account/advertiser/edit_info.ajax',
				data : _formData,
				processData : false,
				contentType : false,
				dataType : 'text',
				success : function() {
					// viewToggleStart(false, $this);
					wisePopupComponent.x();
					approveAdvertiserPopup_edit(advId);
				}
			});
		}
	}
}

function advInfoValidate() {
	if (linkBandComponent.valid() == false) {
		return false;
	}
	if (bandProFileComponent.valid() == false) {
		return false;
	}
	if (industryComponent.valid() == false) {
		return false;
	}
	// if (appComponent.valid() == false) {
	// return false;
	// }
	if (fileComponent.valid() == false) {
		return false;
	}
	return true;
}

var linkBandComponent = (function() {
	var $linkView;

	var get = function() {
		$linkView = $('#adv_linkband_view');
	}

	var change = function(yesOrNo) {
		get();
		$linkView.find('.linked-yes, .linked-no').addClass('_hide');
		var _linkData = $linkView.data('link');
		if (yesOrNo == 'yes') {
			$linkView.find('#isBandLink_yes').prop('checked', true);
			$linkView.find('.linked-yes').removeClass('_hide');
			$linkView.find('#website_link, #website_link_tmp').val('');
			if (_linkData.isBandLink == 1) {
				drawLink(_linkData.websiteLink, _linkData.bandId);
			}
		} else if (yesOrNo == 'no') {
			$linkView.find('#isBandLink_no').prop('checked', true);
			$linkView.find('.linked-no').removeClass('_hide');
			$linkView.find('.linked-yes #selected_band').empty();
			$linkView.find('#search_band_link_view').show();
			$linkView.find('#website_link, #search_band').val('');
			if (_linkData.isBandLink == 0) {
				$linkView.find('#website_link, #website_link_tmp').val(_linkData.websiteLink);
			}
		} else {
			alert('비정상 작동 입니다.');
		}
	}

	var drawLink = function(_url, _bandId) {
		get();
		var _li = '<li><input type="hidden" name="bandId" value="%s"/><span><a href="%s">%s</a></span><button type="button" class="btn-close" onclick="linkBandComponent.remove($(this));">close</button></li>'.format(_bandId, _url, _url);
		$linkView.find('#selected_band').html(_li);
		$linkView.find('#website_link').val(_url);
		$linkView.find('#search_band_link_view').hide();
	}

	var remove = function($this) {
		get();
		$this.closest('li').remove();
		$linkView.find('#search_band_link_view').show();
		$linkView.find('#website_link, #search_band').val('');
	}

	var search = function() {
		get();
		var $searchInput = $linkView.find('#search_band');
		var _bandId = $searchInput.val();
		if (wise.isValidate($searchInput) == true) {
			wise.ajax({
				url : rootPath + '/api/band_profile.ajax',
				type : 'get',
				data : {
					bandNo : _bandId
				},
				success : function(data) {
					switch (data.resultCode) {
					case 404:
						alert('api 권한 없습니다.');
						break;
					case 200:
						alert('없는 밴드 입니다.');
						break;
					case 1:
						if (typeof data.resultData.name === 'undefined') {
							alert('없는 밴드 입니다.');
						} else {
							drawLink('http://band.us/#!/band/%s'.format(_bandId), _bandId);
							$('#brand_name').val(data.resultData.name);
						}
						break;
					default:
						console.log(data);
						alert('관리자에게 문의하세요.');
						break;
					}
				}
			});
		}
	}

	var init = function() {
		get();
		var _linkData = $linkView.data('link');
		if (_linkData.isBandLink == 0) {
			change('no');
		} else {
			change('yes');
		}
	}

	var valid = function() {
		get();
		if ($linkView.find('input[type="radio"][name=isBandLink]:checked').attr('id') == 'isBandLink_yes') {
			return wise.validationUi($linkView.find('#website_link'), $linkView.find('#search_band'), 'This value is required.'); // '검색하여 선택해 주세요.'
		} else {
			return wise.validationUi($linkView.find('#website_link'), $linkView.find('#website_link_tmp'), 'This value is required.'); // 'website link를 넣어주세요.'
		}
	}

	// public 메소드
	return {
		change : change,
		// drawLink : drawLink,
		remove : remove,
		search : search,
		init : init,
		valid : valid
	}
}());

var bandProFileComponent = (function() {
	var _viewId = '#adv_profile_view';

	// profile_preview
	var imgSize = function(src, callBack) {
		var img = new Image();
		img.onload = function() {
			if (typeof callBack === 'function') {
				callBack({
					w : this.width,
					h : this.height
				});
			}
		}
		img.src = src; // 'http://lorempixel.com/40/40';
	}

	var change = function(_this) {
		var $view = $(_viewId);
		var reader = new FileReader();
		reader.onload = function(e) {
			$view.find('#profile_preview').attr('src', e.target.result);
			$view.find('#select_profile_file_btn').addClass('_hide');
			$view.find('#remove_profile_file_btn').removeClass('_hide');
			// 이미지 사이즈
			imgSize(e.target.result, function(size) {
				var $file = $view.find('#profile');
				var f = $file.get(0).files[0];
				if (size.w != 100 || size.h != 100 || f.size > 10240) {
					reset();
					alert('Image must be 100x100 JPG and no bigger than 10KB.');
				} else {
					$file.data('is_change', true);
				}
			});
		};
		reader.readAsDataURL(_this.files[0]); // XXX 최신 브라우저만 지원함.
	}

	var reset = function() {
		var $view = $(_viewId);
		$view.find('#profile').val('').data('is_change', false);
		$view.find('#remove_profile_file_btn').addClass('_hide');
		$view.find('#select_profile_file_btn').removeClass('_hide');
		$view.find('#profile_preview').attr('src', '');
	}

	var init = function() {
		reset();
		var $view = $(_viewId);
		var data = $view.data('band_profile');
		if (typeof data.profile != 'undefiend' && data.profile != '') {
			$view.find('#remove_profile_file_btn').removeClass('_hide');
			$view.find('#select_profile_file_btn').addClass('_hide');
			$view.find('#profile_preview').attr('src', data.profile);
			wise.textCount($view.find('#brand_name').val(data.brandName));
		}
	}

	var valid = function() {
		var $view = $(_viewId); // data-is_change
		if ($view.find('#profile').data('is_change')) {
			return wise.validationUi($view.find('#profile'), $view.find('.band-profile .band-img'), 'This value is required.'); // 프로필을 선택해 주세요.
		} else {
			return true;
		}
	}

	// public 메소드
	return {
		change : change,
		reset : reset,
		init : init,
		valid : valid
	}
}());

var industryComponent = (function() {
	var $viewId = $('#adv_industry_view');

	var _viewId = '#adv_industry_view';
	var industryMode = 'account';

	var draw = function($this, e) {
		var $target = $this.closest('[data-draw-industry]');
		if ($target.hasClass('_open')) {
			$target.removeClass('_open');
		} else {
			$target.addClass('_open');
		}
		if (typeof e !== 'undefined') {
			e.stopPropagation();
		}
	}

	var select = function($this) {
		if (industryMode == 'account' && $this.data('industry_type') == 'main_category') {
			draw($this);
		} else {
			var _value = $this.attr('data-industry_name');
			var $iv = $('#industry_view');
			$iv.attr('data-is-selected', true).removeClass('_open');
			$iv.find('._selected').removeClass('_selected');
			$iv.find('.selection').text($this.data('industry_name'));
			$('#industry').val($this.attr('id').replace('industry_id_', ''));
			$this.addClass('_selected');
		}
	}

	var init = function() {
		var $view = $(_viewId);
		var id = $view.data('industry_id'); // industry_
		var $select = $view.find('#industry_id_%s'.format(id));
		$select.closest('li[data-draw-industry]').addClass('_open');
		select($select);
	}

	var valid = function() {
		var $view = $(_viewId);
		return wise.validationUi($view.find('#industry'), $view.find('#industry_view .selection'), 'This value is required.'); // '인더스트리 선택해주세요.'
	}

	var setMode = function(mode) {
		if (mode == 'report') {
			industryMode = mode;
		}
	}

	// public 메소드
	return {
		draw : draw,
		select : select,
		init : init,
		valid : valid,
		setMode : setMode
	}
}());

var appComponent = (function() {

	window.popup2 = $.extend({}, wisePopupComponent);

	var _viewId = '#adv_app_view';

	var popup = function($li) {
		popup2.popup('/ag/account/popup.popup?p=addAppUrl', {
			popupComponentName : 'popup2',
			addClass : 'popup-add-app',
			popupTitle : 'Add App',
			onFormOpenned : function() {
				wise.activeTooltipForIcon();
				if (typeof $li === 'undefined') {
				} else {
					var _spec = $li.data('app_spec');
					$('#popup_app_name').val(_spec.appName);
					$('#popup_ios_url').val(_spec.iosUrl);
					$('#popup_android_url').val(_spec.androidUrl);
				}
			},
			okBtnCallBack : function() {
				if (typeof $li === 'undefined') {
					add();
				} else {
					edit($li);
				}
				return false;
			}
		});
	}

	var popupForEdit = function($this) {
		popup($this.closest('li'));
	}

	var customValidate = function($form) {
		if ($form.find('#popup_ios_url').val() == '' && $form.find('#popup_android_url').val() == '') {
			$form.find('#ok_validate').removeClass('_hide');
			return false;
		} else {
			$form.find('#ok_validate').addClass('_hide');
			return true;
		}
	}

	var validate = function() {
		var $form = $('#app_url_form');
		if (wise.isValidate($form)) {
			if (customValidate($form)) {
				return true;
			}
		}
		return false;
	}

	var checkProtocol = function(_url) {
		if (_url == '' || typeof _url == 'undefined') {
			return '';
		} else {
			var _exp = /((http(s?))\:\/\/)/g;
			if (_exp.test(_url)) {
				return _url;
			} else {
				return 'http://' + _url;
			}
		}
	}

	var checkUrl = function(_url) {
		var urlRegExp = /^(((http(s?))\:\/\/)?)([0-9a-zA-Z\-]+\.)+[a-zA-Z]{2,6}(\:[0-9]+)?(\/\S*)?$/g;
		return urlRegExp.test(_url);
	}

	var makeLiHtml = function(data) {
		var _li = '<li data-app_spec="%s">'.format(wise.escapeHtml(JSON.stringify(data)));
		_li += '		<p>%s</p>'.format(wise.escapeHtml(data.appName));
		if (checkUrl(data.iosUrl)) {
			_li += '	<p><a href="%s">%s</a></p>'.format(data.iosUrl, data.iosUrl);
		}
		if (checkUrl(data.androidUrl)) {
			_li += '	<p><a href="%s">%s</a></p>'.format(data.androidUrl, data.androidUrl);
		}
		_li += '		<button type="button" class="btn-edit" onclick="appComponent.popupForEdit($(this));">Edit</button>';
		_li += '		<button type="button" class="btn-delete" onclick="appComponent.remove($(this));">Delete</button>';
		_li += '	</li>';
		return _li;
	}

	var draw = function(data) {
		var $view = $(_viewId);
		var _li = makeLiHtml(data);
		$view.find('#app_url_view').append(_li);
	}

	var makeSpec = function() {
		var $view = $(_viewId);
		var jsonAppSpec = [];
		$view.find('#app_url_view li').each(function() {
			var app_spec = $(this).data('app_spec');
			jsonAppSpec.push(app_spec);
		});
		$view.find('input#json_app_spec').val(wise.escapeHtml(JSON.stringify(jsonAppSpec)));
	}

	var getSpecFromAddPopup = function() {
		return {
			appName : $('#popup_app_name').val(),
			iosUrl : checkProtocol($('#popup_ios_url').val()),
			androidUrl : checkProtocol($('#popup_android_url').val())
		};
	}

	var add = function() {
		if (validate()) {
			draw(getSpecFromAddPopup());
			makeSpec();
			popup2.x();
		}
	}

	var remove = function($this) {
		var $li = $this.closest('li');
		var spec = $li.data('app_spec');
		if (typeof spec.id == 'undefined') {
			$li.remove();
		} else {
			spec.action = 'delete';
			$li.addClass('_hide');
		}
		makeSpec();
	}

	var edit = function($li) {
		if (validate()) {
			var _spec = $li.data('app_spec');
			var $view = $(_viewId);
			var _reSpec = getSpecFromAddPopup();
			if (typeof _spec.id !== 'undefined') {
				_reSpec.id = _spec.id;
				_reSpec.action = 'edit';
			}
			$li.replaceWith(makeLiHtml(_reSpec));
			makeSpec();
			popup2.x();
		}
	}

	var init = function() {
		var $view = $(_viewId);
		var apps = $view.data('app');
		$view.find('#app_url_view').empty();
		$.each(apps, function(idx, appData) {
			draw(appData);
		});
		makeSpec();
	}

	var valid = function() {
		var $ul = $('#app_url_view');
		if ($ul.find('li:not("._hide")').length == 0) {
			return wise.validationUi($(), $ul.prevAll('button'), 'This value is required.'); // 'app 선택해주세요.'
		} else {
			$ul.prevAll('button').attr('style', '').tooltip('destroy');
			return true;
		}
	}

	return {
		popup : popup,
		init : init,
		remove : remove,
		valid : valid,
		popupForEdit : popupForEdit
	}
}());
