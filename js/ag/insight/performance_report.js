var STATS_ARRAY = [ 'impressions', 'cpm', 'ctr', 'cpc', 'clicks' ];
var COLUMN_NAME_MAP = {
	'impressions' : 'Impression',
	'cpm' : 'CPM',
	'ctr' : 'CTR',
	'cpc' : 'CPC'
}

function changeRoleAndMovePage(ele, page, chooseRole) {
	var $btn = $(ele);
	var objectId = $btn.val();
	var url = null;
	switch (chooseRole) {
	case 'AGENCY': // 관리자가 에이전시로 접속한 경우이다. 다른 아이디를 저장할 필요가 없다.
		switch (page) {
		case 'sales':
			url = '/ag/sales/report?moveMainAgencyId=%s'.format(objectId);
			break;
		case 'spender':
			url = '/ag/spender/report?moveMainAgencyId=%s'.format(objectId);
			break;
		}
		break;
	case 'SUB_AGENCY':
		// 메인 에이전시가 서브 에이전시로 접속했거나, 관리자가 서브 에이전시로 접속한 경우이다. 경우에 따라 메인 에이전시의 아이디를 저장할 필요가 있다.
		switch (page) {
		case 'sales':
			if (SESSION.choosedRole == 'AGENCY' && $btn.val() == SESSION.choosedAgId) {
				url = '/ag/sales/report';
			} else {
				url = '/sag/sales/report?moveSubAgencyId=%s'.format(objectId); //
			}
			break;
		case 'spender':
			if (SESSION.choosedRole == 'AGENCY' && $btn.val() == SESSION.choosedAgId) {
				url = '/ag/spender/report';
			} else {
				url = '/sag/spender/report?moveSubAgencyId=%s'.format(objectId); //
			}
			break;
		}
		break;
	case 'ADVERTISER':
		switch (page) {
		case 'sales':
			url = '/adv/sales/report?moveMainAgencyId=%s&moveSubAgencyId=%s&moveAdvId=%s'.format($btn.data('agency'), $btn.data('sub_agency'), objectId);
			break;
		case 'spender':
			url = '/adv/manage/campaign?moveMainAgencyId=%s&moveSubAgencyId=%s&moveAdvId=%s'.format($btn.data('agency'), $btn.data('sub_agency'), objectId);
			break;
		case 'manage': // 
			url = '/adv/manage/campaign/info?adCampaignId=%s&moveMainAgencyId=%s&moveSubAgencyId=%s&moveAdvId=%s'.format($btn.data('campaign_id'), $btn.data('agency'), $btn.data('sub_agency'), objectId);
			break;
		}
		break;
	}
	location.href = url;
}

function downloadXlsx() {
	$('#export_data').submit();
}

function exportDataSetting() {
	if ($('input[name=filterType]:checked').val() == 'agency') {
		if ($('#search_product_type').val() == '10') {
			$('#export_column').val('["Agency", "Spent", "Impression", "CPM", "CTR[%]", "CPWC", "Website Click"]');
		} else if ($('#search_product_type').val() == '20') {
			$('#export_column').val('["Agency", "Spent", "Impression", "CPM", "CTR[%]", "CPIC", "App Install"]');
		}
	} else if ($('input[name=filterType]:checked').val() == 'adv') {
		if (SESSION.choosedRole == 'ADMIN') {
			if ($('#search_product_type').val() == '10') {
				$('#export_column').val('["Advertiser", "Industry", "Agency", "Spent", "Impression", "CPM", "CTR[%]", "CPWC", "Website Click"]');
			} else if ($('#search_product_type').val() == '20') {
				$('#export_column').val('["Advertiser", "Industry", "Agency", "Spent", "Impression", "CPM", "CTR[%]", "CPIC", "App Install"]');
			}
		} else if (SESSION.choosedRole == 'AGENCY') {
			if ($('#search_product_type').val() == '10') {
				$('#export_column').val('["Advertiser", "Industry", "Sub Agency", "Spent", "Impression", "CPM", "CTR[%]", "CPWC", "Website Click"]');
			} else if ($('#search_product_type').val() == '20') {
				$('#export_column').val('["Advertiser", "Industry", "Sub Agency", "Spent", "Impression", "CPM", "CTR[%]", "CPIC", "App Install"]');
			}
		} else if (SESSION.choosedRole == 'SUB_AGENCY') {
			if ($('#search_product_type').val() == '10') {
				$('#export_column').val('["Advertiser", "Industry", "Spent", "Impression", "CPM", "CTR[%]", "CPWC", "Website Click"]');
			} else if ($('#search_product_type').val() == '20') {
				$('#export_column').val('["Advertiser", "Industry", "Spent", "Impression", "CPM", "CTR[%]", "CPIC", "App Install"]');
			}
		}
	} else if ($('input[name=filterType]:checked').val() == 'cam') {
		if (SESSION.choosedRole == 'ADMIN') {
			if ($('#search_product_type').val() == '10') {
				$('#export_column').val('["Ad Campaign Name", "Agency", "Advertiser", "Spent", "Impression", "CPM", "CTR[%]", "CPWC", "Website Click"]');
			} else if ($('#search_product_type').val() == '20') {
				$('#export_column').val('["Ad Campaign Name", "Agency", "Advertiser", "Spent", "Impression", "CPM", "CTR[%]", "CPIC", "App Install"]');
			}
		} else if (SESSION.choosedRole == 'AGENCY') {
			if ($('#search_product_type').val() == '10') {
				$('#export_column').val('["Ad Campaign Name", "Sub Agency", "Advertiser", "Spent", "Impression", "CPM", "CTR[%]", "CPWC", "Website Click"]');
			} else if ($('#search_product_type').val() == '20') {
				$('#export_column').val('["Ad Campaign Name", "Sub Agency", "Advertiser", "Spent", "Impression", "CPM", "CTR[%]", "CPIC", "App Install"]');
			}
		} else if (SESSION.choosedRole == 'SUB_AGENCY') {
			if ($('#search_product_type').val() == '10') {
				$('#export_column').val('["Ad Campaign Name", "Advertiser", "Spent", "Impression", "CPM", "CTR[%]", "CPWC", "Website Click"]');
			} else if ($('#search_product_type').val() == '20') {
				$('#export_column').val('["Ad Campaign Name", "Advertiser", "Spent", "Impression", "CPM", "CTR[%]", "CPIC", "App Install"]');
			}
		}
	} else if ($('input[name=filterType]:checked').val() == 'subAg') {
		if ($('#search_product_type').val() == '10') {
			$('#export_column').val('["Sub Agency", "Spent", "Impression", "CPM", "CTR[%]", "CPWC", "Website Click"]');
		} else if ($('#search_product_type').val() == '20') {
			$('#export_column').val('["Sub Agency", "Spent", "Impression", "CPM", "CTR[%]", "CPIC", "App Install"]');
		}
	}
	$('#export_search_start_date').val($('#search_start_date').val());
	$('#export_search_end_date').val($('#search_end_date').val());
	$('#export_view_type').val($('input[name=viewType]:checked').val());
	$('#export_product_type').val($('#search_product_type').val());
	$('#export_filter_type').val($('input[name=filterType]:checked').val());
	$('#export_industry').val($('#select_industry_value').val());
}

function searchData() {
	exportDataSetting();
	getPerformanceReportData();
}

function selectIndustry($this) {
	var _text = $this.attr('data-text');
	var _value = $this.attr('data-value') + '@_';
	if (_text == 'All') {
		_value = 'All';
	}
	var $iv = $('#industry_view');
	$iv.attr('data-is-selected', true).removeClass('_open');
	$iv.find('._selected').removeClass('_selected');
	$iv.find('.selection').text(_text);
	$('#industry').val(_text);
	if ($this.parent().parent().hasClass('depth')) {
		_value = $this.parent().parent().parent().find('span').attr('data-value') + '_' + $this.attr('data-value');
	}
	$('#select_industry_value').val(_value);
	$this.addClass('_selected');
}

function drawIndustry($this, e) {
	var $target = $this.closest('[data-draw-industry]');
	if ($target.hasClass('_open')) {
		$target.removeClass('_open');
	} else {
		$target.addClass('_open');
	}
	e.stopPropagation();
}

function selectFilterType(ele) {
	if ($(ele).val() == 'adv') {
		$('#adv_industry_tr').show();
	} else {
		$('#adv_industry_tr').hide();
	}
}

function getPerformanceReportData() {
	wise.ajax({
		url : rootPath + "/sag/performance/report.ajax",
		type : 'POST',
		data : $('#search_form').serialize(),
		dataType : 'JSON',
		success : function(data) {
			switch ($('input[name=filterType]:checked').val()) {
			case 'agency':
				$('#table_title').text('Performance by Agency');
				break;
			case 'subAg':
				$('#table_title').text('Performance by Sub Agency');
				break;
			case 'adv':
				$('#table_title').text('Performance by Advertiser');
				break;
			case 'cam':
				$('#table_title').text('Performance by Campaign');
				break;
			}
			$('#chart_title').text('Average Performance of ' + $("#search_product_type option:selected").text() + ' Campaign Type');
			drawChart(data);
			drawTable(data);
		}
	});
}

function makeDataForChart(data) {
	var completeData = {};
	var totalTmpData = {};
	var totalData = {};
	var dailyData = {};
	var dateArray = [];
	if ($('#search_product_type').val() == '10') {
		COLUMN_NAME_MAP['clicks'] = 'Website Click';
	} else if ($('#search_product_type').val() == '20') {
		COLUMN_NAME_MAP['clicks'] = 'Install Click';
	}
	for ( var dateIndax in data.dateArray) {
		sumReportStatsData(totalTmpData, data.adInsightMap[data.dateArray[dateIndax]]);
	}
	for ( var i in STATS_ARRAY) {
		totalData[STATS_ARRAY[i]] = calcActionValue(STATS_ARRAY[i], totalTmpData);
	}
	dailyData = makeForChartData(data.adInsightMap, STATS_ARRAY, COLUMN_NAME_MAP);
	for ( var i in data.dateArray) {
		if ($('input[name=viewType]:checked').val() == 'month') {
			dateArray.push(moment(data.dateArray[i]).format('YYYY/MM'));
		} else {
			dateArray.push(moment(data.dateArray[i]).format('MM/DD'));
		}
	}
	completeData = {
		totalData : totalData,
		dailyData : dailyData,
		dateArray : dateArray
	}
	return completeData;
}

function drawChart(data) {
	var chartData = makeDataForChart(data);
	var chartOption = defaultChartOption();
	chartOption.totalSumMap = chartData.totalData;
	chartOption.colors = [ '#d7d7d7', '#1f7d3f', '#767676', '#90c7a2', '#c1c1c1' ];
	chartOption.chart = {
		height : '200',
		style : {
			'fontFamily' : '"Roboto Condensed","Nanum Barun Gothic", "맑은 고딕", sans-serif'
		}
	};
	chartOption.xAxis = {
		categories : chartData.dateArray,
		tickLength : 0,
		lineColor : '#d7d7d7'
	};
	chartOption.tooltip = {
		backgroundColor : "#404040",
		borderColor : "#404040",
		borderWidth : 1,
		shadow : false,
		useHTML : true,
		borderRadius : 10,
		style : {
			color : '#fff',
			padding : '0',
			margin : '0'
		},
		headerFormat : '',
		pointFormatter : function() {
			if ('CTR' == this.series.name) {
				return '<span>' + Highcharts.numberFormat(this.y, 2, '.') + ' %</span>';
			} else if ('CPC' == this.series.name || 'CPM' == this.series.name) {
				return '<span>' + Highcharts.numberFormat(this.y, 1, '.') + '</span>';
			} else {
				return '<span>' + Highcharts.numberFormat(this.y, 0, '.') + '</span>';
			}
		}
	};
	chartOption.legend = {
		layout : 'vertical',
		align : 'right',
		verticalAlign : 'top',
		itemMarginTop : 7,
		useHTML : true,
		symbolPadding : 5,
		margin : 50,
		padding : 0,
		y : 0,
		labelFormatter : function() {
			var count = 0;
			var rate_count = 0;
			for (var i = 0; i < this.yData.length; i++) {
				count += this.yData[i];
			}
			if ('CTR' == this.name) {
				return '<p class="legend"><span>' + this.name + '</span><span class="number">' + Highcharts.numberFormat(chartData.totalData.ctr, 2, '.') + '%</span></p>';
			} else if ('CPC' == this.name) {
				if ($('#search_product_type').val() == '10') {
					return '<p class="legend"><span>CPWC<span class="number">' + Highcharts.numberFormat(chartData.totalData.cpc, 1, '.') + '</span></p>';
				} else if ($('#search_product_type').val() == '20') {
					return '<p class="legend"><span>CPIC<span class="number">' + Highcharts.numberFormat(chartData.totalData.cpc, 1, '.') + '</span></p>';
				}
			} else if ('CPM' == this.name) {
				return '<p class="legend"><span>' + this.name + '<span class="number">' + Highcharts.numberFormat(chartData.totalData.cpm, 1, '.') + '</span></p>';
			} else if ('Spent' == this.name) {
				return '<p class="legend"><span>' + this.name + '<span class="number">' + Highcharts.numberFormat(count, 0, '.') + '</span></p>';
			} else {
				return '<p class="legend"><span>' + this.name + '<span class="number">' + Highcharts.numberFormat(count, 0, '.') + '</span></p>';
			}
		}
	};
	chartOption.plotOptions.line = {
		states : {
			hover : {
				enabled : false
			}
		}
	};
	for (var i = 0; i < chartData.dailyData.length; i++) {
		var reportStats = chartData.dailyData[i];
		var oppsTemp = { // 오른쪽 초기값들
			tickPixelInterval : 35,
			gridLineColor : '#F1F1F1',
			opposite : true,
			title : {
				text : ''
			},
			labels : {
				style : {
					color : '',
					display : 'none'
				}
			},
			min : 0
		};
		var column_temp = {
			type : 'column',
			name : '',
			yAxis : '',
			zIndex : 2,
			data : [],
			min : 0
		};
		var line_temp = {
			type : 'line',
			name : '',
			yAxis : '',
			zIndex : 3,
			data : [],
			min : 0
		};
		var area_temp = {
			type : 'area',
			name : '',
			yAxis : '',
			zIndex : 1,
			data : [],
			color : '',
			min : 0,
			fillOpacity : 0.2,
			marker : {
				enabled : false
			}
		};
		switch (reportStats.name) {
		case 'Spent':
			chartOption.series[i] = column_temp;
			chartOption.series[i].pointWidth = 6;
			break;
		case 'Impression':
			chartOption.series[i] = area_temp;
			break;
		default:
			chartOption.series[i] = line_temp;
		}
		chartOption.yAxis[i + 1] = oppsTemp;
		chartOption.series[i].name = reportStats.name;
		chartOption.series[i].yAxis = i + 1;
		chartOption.series[i].data = reportStats.data;
		chartOption.yAxis[i + 1].labels.style.color = chartOption.colors[i];
	}
	$('#average-performance_graph_div').highcharts(chartOption);
}

function makeDataForTable(data) {
	var tableData = {};
	var totalData = {};
	var calculationArr = [ 'cpm', 'ctr', 'cpc' ];
	for ( var i in data.adInsightTableArray) {
		sumReportStatsData(totalData, data.adInsightTableArray[i]);
	}
	tableData = {
		totalData : totalData,
		data : data.adInsightTableArray
	}
	for ( var i in calculationArr) {
		tableData.totalData[calculationArr[i]] = calcActionValue(calculationArr[i], tableData.totalData);
		for ( var k in data.adInsightTableArray) {
			data.adInsightTableArray[k][calculationArr[i]] = calcActionValue(calculationArr[i], data.adInsightTableArray[k]);
		}
	}
	return tableData;
}

function drawTable(data) {
	var drawData = makeDataForTable(data);
	var appendData = '';
	appendData += '<caption>' + $('#table_title').text() + '</caption>';
	appendData += '<colgroup>';
	if ($('input[name=filterType]:checked').val() == 'agency' || $('input[name=filterType]:checked').val() == 'subAg') {
		appendData += '<col/>';
		appendData += '<col span="2" width="110">';
		appendData += '<col span="3" width="85">';
		appendData += '<col width="105">';
		appendData += '<col width="205">';
	} else if ($('input[name=filterType]:checked').val() == 'adv') {
		if (SESSION.choosedRole == 'SUB_AGENCY') {
			appendData += '<col/>';
			appendData += '<col/>';
			appendData += '<col span="2" width="110"/>';
			appendData += '<col span="3" width="85">';
			appendData += '<col width="105">';
			appendData += '<col width="205">';
		} else {
			appendData += '<col width="95"/>';
			appendData += '<col width="95"/>';
			appendData += '<col width="95"/>';
			appendData += '<col span="2" width="75">';
			appendData += '<col span="3" width="70">';
			appendData += '<col width="105">';
			appendData += '<col width="190">';
		}
	} else if ($('input[name=filterType]:checked').val() == 'cam') {
		if (SESSION.choosedRole == 'SUB_AGENCY') {
			appendData += '<col/>';
			appendData += '<col/>';
			appendData += '<col span="2" width="110">';
			appendData += '<col span="3" width="85">';
			appendData += '<col width="105">';
			appendData += '<col width="110">';
		} else {
			appendData += '<col width="100"/>';
			appendData += '<col width="100"/>';
			appendData += '<col width="100"/>';
			appendData += '<col span="2" width="100">';
			appendData += '<col span="3" width="75">';
			appendData += '<col width="105">';
			appendData += '<col width="110">';
		}
	}
	appendData += '</colgroup>';
	appendData += '<thead>';
	switch ($('input[name=filterType]:checked').val()) {
	case 'agency':
	case 'subAg':
		appendData += '<tr>';
		appendData += '		<th class="agency" scope="col">';
		if (SESSION.choosedRole == 'AGENCY') {
			appendData += '			Sub Agency';
		} else if (SESSION.choosedRole == 'ADMIN') {
			appendData += '			Agency';
		}
		appendData += '		</th>';
		break;
	case 'adv':
		appendData += '<tr>';
		appendData += '		<th class="advertiser" scope="col">';
		appendData += '			Advertiser';
		appendData += '		</th>';
		appendData += '		<th class="industry" scope="col">';
		appendData += '			Industry';
		appendData += '		</th>';
		if (SESSION.choosedRole != 'SUB_AGENCY') {
			appendData += '		<th class="agency" scope="col">';
			if (SESSION.choosedRole == 'AGENCY') {
				appendData += '			Sub Agency';
			} else if (SESSION.choosedRole == 'ADMIN') {
				appendData += '			Agency';
			}
			appendData += '		</th>';
		}
		break;
	case 'cam':
		appendData += '<tr>';
		appendData += '		<th class="campaign" scope="col">';
		appendData += '			Campaign';
		appendData += '		</th>';
		if (SESSION.choosedRole != 'SUB_AGENCY') {
			appendData += '		<th class="agency" scope="col">';
			if (SESSION.choosedRole == 'AGENCY') {
				appendData += '			Sub Agency';
			} else if (SESSION.choosedRole == 'ADMIN') {
				appendData += '			Agency';
			}
			appendData += '		</th>';
		}
		appendData += '		<th class="advertiser" scope="col">';
		appendData += '			Advertiser';
		appendData += '		</th>';
		break;
	}
	appendData += '		<th class="spent" scope="col">';
	appendData += '			Spent';
	appendData += '		</th>';
	appendData += '		<th class="impression" scope="col">';
	appendData += '			Impression';
	appendData += '		</th>';
	appendData += '		<th class="cpm" scope="col">';
	appendData += '			CPM';
	appendData += '		</th>';
	appendData += '		<th class="ctr" scope="col">';
	appendData += '			CTR[%]';
	appendData += '		</th>';
	appendData += '		<th class="cpc" scope="col">';
	if ($('#search_product_type').val() == '10') {
		appendData += '			CPWC';
	} else if ($('#search_product_type').val() == '20') {
		appendData += '			CPIC';
	}
	appendData += '		</th>';
	appendData += '		<th class="click" scope="col">';
	if ($('#search_product_type').val() == '10') {
		appendData += '			Website Click';
	} else if ($('#search_product_type').val() == '20') {
		appendData += '			Install Click';
	}
	appendData += '		</th>';
	appendData += '		<th scope="col">';
	appendData += '			View';
	appendData += '		</th>';
	appendData += '</tr>';
	appendData += '</thead>';
	appendData += '<tfoot>';
	switch ($('input[name=filterType]:checked').val()) {
	case 'agency':
	case 'subAg':
		appendData += '<tr>';
		appendData += '		<td class="agency" scope="col"><span>';
		appendData += '			Total';
		appendData += '		</span></td>';
		break;
	case 'adv':
		appendData += '<tr>';
		appendData += '		<td class="advertiser" scope="col"><span>';
		appendData += '			Total';
		appendData += '		</span></td>';
		appendData += '		<td class="industry" scope="col"><span>';
		appendData += '		</span></td>';
		if (SESSION.choosedRole != 'SUB_AGENCY') {
			appendData += '		<td class="agency" scope="col"><span>';
			appendData += '		</span></td>';
		}
		break;
	case 'cam':
		appendData += '<tr>';
		appendData += '		<td class="campaign" scope="col"><span>';
		appendData += '			Total';
		appendData += '		</span></td>';
		if (SESSION.choosedRole != 'SUB_AGENCY') {
			appendData += '		<td class="agency" scope="col"><span>';
			appendData += '		</span></td>';
		}
		appendData += '		<td class="advertiser" scope="col"><span>';
		appendData += '		</span></td>';
		break;
	}
	appendData += '		<td class="spent" scope="col"><span>';
	appendData += Highcharts.numberFormat(drawData.totalData.spend, '0', '.');
	appendData += '		</span></td>';
	appendData += '		<td class="impression" scope="col"><span>';
	appendData += Highcharts.numberFormat(drawData.totalData.impressions, '0', '.');
	appendData += '		</span></td>';
	appendData += '		<td class="cpm" scope="col"><span>';
	appendData += drawData.totalData.cpm;
	appendData += '		</span></td>';
	appendData += '		<td class="ctr" scope="col"><span>';
	appendData += drawData.totalData.ctr;
	appendData += '		</span></td>';
	appendData += '		<td class="cpc" scope="col"><span>';
	appendData += drawData.totalData.cpc;
	appendData += '		</span></td>';
	appendData += '		<td class="click" scope="col"><span>';
	appendData += Highcharts.numberFormat(drawData.totalData.clicks, '0', '.');
	appendData += '		</span></td>';
	appendData += '		<td scope="col"><span>';
	appendData += '		</span></td>';
	appendData += '</tr>';
	appendData += '</tfoot>';
	appendData += '<tbody>';
	for ( var i in drawData.data) {
		switch ($('input[name=filterType]:checked').val()) {
		case 'agency':
		case 'subAg':
			appendData += '<tr>';
			appendData += '		<td class="agency" scope="col"><span class="dotdotdot_ele">';
			appendData += drawData.data[i].name;
			appendData += '		</span></td>';
			break;
		case 'adv':
			appendData += '<tr>';
			appendData += '		<td class="advertiser" scope="col"><span class="dotdotdot_ele">';
			appendData += drawData.data[i].name;
			appendData += '		</span></td>';
			appendData += '		<td class="industry" scope="col"><span class="dotdotdot_ele">';
			appendData += drawData.data[i].industry;
			appendData += '		</span></td>';
			if (SESSION.choosedRole != 'SUB_AGENCY') {
				appendData += '		<td class="agency" scope="col"><span class="dotdotdot_ele">';
				appendData += drawData.data[i].agencyName;
				appendData += '		</span></td>';
			}
			break;
		case 'cam':
			appendData += '<tr>';
			appendData += '		<td class="campaign" scope="col"><span class="dotdotdot_ele">';
			appendData += drawData.data[i].name;
			appendData += '		</span></td>';
			if (SESSION.choosedRole != 'SUB_AGENCY') {
				appendData += '		<td class="agency" scope="col"><span class="dotdotdot_ele">';
				appendData += drawData.data[i].agencyName;
				appendData += '		</span></td>';
			}
			appendData += '		<td class="advertiser" scope="col"><span class="dotdotdot_ele">';
			appendData += drawData.data[i].advName;
			appendData += '		</span></td>';
			break;
		}
		appendData += '		<td class="spent" scope="col"><span>';
		appendData += Highcharts.numberFormat(drawData.data[i].spend, '0', '.');
		appendData += '		</span></td>';
		appendData += '		<td class="impression" scope="col"><span>';
		appendData += Highcharts.numberFormat(drawData.data[i].impressions, '0', '.');
		appendData += '		</span></td>';
		appendData += '		<td class="cpm" scope="col"><span>';
		appendData += drawData.data[i].cpm;
		appendData += '		</span></td>';
		appendData += '		<td class="ctr" scope="col"><span>';
		appendData += drawData.data[i].ctr;
		appendData += '		</span></td>';
		appendData += '		<td class="cpc" scope="col"><span>';
		appendData += drawData.data[i].cpc;
		appendData += '		</span></td>';
		appendData += '		<td class="click" scope="col"><span>';
		appendData += Highcharts.numberFormat(drawData.data[i].clicks, '0', '.');
		appendData += '		</span></td>';
		switch ($('input[name=filterType]:checked').val()) {
		case 'agency':
			appendData += '		<td scope="col">';
			appendData += '			<button type="button" value="' + drawData.data[i].id + '" data-agency="' + drawData.data[i].agencyId + '" class="btn-sales" onclick="changeRoleAndMovePage(this, \'sales\', \'AGENCY\');" ';
			if (drawData.data[i].status == 1) {
				appendData += '			disabled';
			}
			appendData += '			>Sales</button>';
			appendData += '			<button type="button" value="' + drawData.data[i].id + '" data-agency="' + drawData.data[i].agencyId + '" class="btn-spender" onclick="changeRoleAndMovePage(this, \'spender\', \'AGENCY\');" ';
			if (drawData.data[i].status == 1) {
				appendData += '			disabled';
			}
			appendData += '			>Spender</button>';
			appendData += '		</td>';
			break;
		case 'subAg':
			appendData += '		<td scope="col">';
			appendData += '			<button type="button" value="' + drawData.data[i].id + '" data-agency="' + drawData.data[i].agencyId + '" class="btn-sales" onclick="changeRoleAndMovePage(this, \'sales\', \'SUB_AGENCY\');" ';
			if (drawData.data[i].status == 1) {
				appendData += '			disabled';
			}
			appendData += '			>Sales</button>';
			appendData += '			<button type="button" value="' + drawData.data[i].id + '" data-agency="' + drawData.data[i].agencyId + '" class="btn-spender" onclick="changeRoleAndMovePage(this, \'spender\', \'SUB_AGENCY\');" ';
			if (drawData.data[i].status == 1) {
				appendData += '			disabled';
			}
			appendData += '			>Spender</button>';
			appendData += '		</td>';
			break;
		case 'adv':
			appendData += '		<td scope="col">';
			appendData += '			<button type="button" value="' + drawData.data[i].id + '" data-agency="' + drawData.data[i].agencyId + '" data-sub_agency="' + drawData.data[i].subAgencyId + '" class="btn-sales" onclick="changeRoleAndMovePage(this, \'sales\', \'ADVERTISER\');" ';
			if (drawData.data[i].status == 1) {
				appendData += '			disabled';
			}
			appendData += '			>Sales</button>';
			appendData += '			<button type="button" value="' + drawData.data[i].id + '" data-agency="' + drawData.data[i].agencyId + '" data-sub_agency="' + drawData.data[i].subAgencyId + '" class="btn-spender" onclick="changeRoleAndMovePage(this, \'spender\', \'ADVERTISER\');" ';
			if (drawData.data[i].status == 1) {
				appendData += '			disabled';
			}
			appendData += '			>Manage</button>';
			appendData += '		</td>';
			break;
		case 'cam':
			appendData += '		<td scope="col">';
			appendData += '			<button type="button" value="' + drawData.data[i].advId + '" data-agency="' + drawData.data[i].agencyId + '" data-sub_agency="' + drawData.data[i].subAgencyId + '" data-campaign_id="' + drawData.data[i].id
					+ '" class="btn-manage" class="btn-spender" onclick="changeRoleAndMovePage(this, \'manage\', \'ADVERTISER\');" ';
			if (drawData.data[i].status == 1) {
				appendData += '			disabled';
			}
			appendData += '			>Manage</button>';
			appendData += '		</td>';
			break;
		}
		appendData += '</tr>';
	}
	appendData += '</tbody>';

	if ($('#performance_table').hasClass('dataTable')) {
		$('#performance_table').DataTable().destroy();
	}
	$('#performance_table').children().remove();
	$('#performance_table').append(appendData);
	var orderIndex = 2;
	if ($('input[name=filterType]:checked').val() == 'adv' || $('input[name=filterType]:checked').val() == 'cam') {
		orderIndex = 3;
	}
	var table = $('#performance_table').dataTable({
		'bFilter' : false,
		"order" : [ [ orderIndex, "desc" ] ],
		'info' : false,
		'paging' : false,
		'bLengthChange' : false,
		'retrieve' : true,
		'autoWidth' : false
	});
	$('#performance_table').find('.dotdotdot_ele').tooltip(ellipsisTooltip);

}
