function appendDataTable() {
	var campaignTable = $('#campaign_budget_status_table').dataTable({
		'bFilter' : false,
		'info' : false,
		"order" : [ [ 2, "asc" ] ],
		'paging' : false,
		'bLengthChange' : false,
		'retrieve' : true,
		'autoWidth' : false
	});
	var creativeTable = $('#creative_approval_status_table').dataTable({
		'bFilter' : false,
		'info' : false,
		"order" : [ [ 1, "desc" ] ],
		'paging' : false,
		'bLengthChange' : false,
		'retrieve' : true,
		'autoWidth' : false
	});
	$('#campaign_budget_status_table').find('.dotdotdot_ele').tooltip(ellipsisTooltip);
	$('#creative_approval_status_table').find('.dotdotdot_ele').tooltip(ellipsisTooltip);
}

function movePage(type, ele) {
	// alert('확인해보기123123123');
	// debugger;
	var $ele = $(ele);
	var url = '';
	switch (type) {
	case 'manage':
		// url = '/sag/change/role?chooseId=' + $ele.val() + '&chooseRole=ADVERTISER&page=' + type;
		url = SESSION.roleUrl + '/change/role?chooseId=' + $ele.val() + '&chooseRole=ADVERTISER&page=' + type;
		url += '&agencyId=' + $ele.attr('data-agency_id') + '&subAgencyId=' + $ele.attr('data-sub_agency_id') + '&adCampaignId=' + $ele.attr('data-cam_id');
		// tkdrb // 된듯
		url = '/adv/manage/campaign/info?adCampaignId=%s&moveSubAgencyId=%s&moveAdvId=%s'.format($ele.attr('data-cam_id'), $ele.attr('data-sub_agency_id'), $ele.val());
		break;
	case 'myAccount':
		url = '/ag/account/me';
		break;
	case 'sag':
		url = '/ag/account/subagency/list';
		break;
	case 'adv':
		url = '/ag/account/advertiser/list';
		break;
	}
	$(location).attr('href', url);
}

function confirmAdCreative(ele) {
	var $ele = $(ele);
	var adCreativeId = $ele.val();
	var adCampaignId = $ele.attr('data-cam_id');
	var popup = wisePopupComponent.popup('/sag/creative/confirm.popup', {
		addClass : 'popup-view-creative',
		popupTitle : 'View Creative',
		removeFooter : true,
		onFormOpenned : function() {
			$('#popup_go_to_campaign').attr('data-cam_id', adCampaignId);
			$('#popup_go_to_campaign').attr('data-cam_id', adCampaignId);
		}
	}, {
		ad_creative_id : adCreativeId
	});
}