var TABLE_STATS = [ 'totalSales', 'service', 'commission' ];

function changeRoleAndMovePage(ele, page, chooseRole) {
	var $ele = $(ele);
	// // 원래 내가 하고싶은 체인 롤이었어...
	// var url = /* (SESSION.choosedRole == 'ADMIN' ? '/sa' : '/ag') + */'/sa/change/role?chooseId=' + $ele.val() + '&chooseRole=' + chooseRole + '&page=' + page;
	// // 근대 현재 선택된 롤이 에이전시이면서 엘레먼트에 담긴 값이 현재 선택된 에이전시 아이디와 같으면 체인지 롤 안할래... 대신
	// if (SESSION.choosedRole == 'AGENCY' && $ele.val() == SESSION.choosedAgId) {
	// // 리포트로 이동하겠어
	// if (page == 'sales') {
	// url = '/ag/sales/report';
	// } else if (page == 'spender') {
	// url = '/ag/spender/report';
	// }
	// }
	// // 아 근대 내가 광고주면
	// if (chooseRole == 'ADVERTISER') {
	// url += '&agencyId=' + $ele.attr('data-agency') + '&subAgencyId=' + $ele.attr('data-sub_agency');
	// }

	// alert('으악3333333333333');
	// debugger;
	// page = sales, spender, manage
	// chooseRole = AGENCY, SUB_AGENCY, ADVERTISER
	// tkdrb
	switch (chooseRole) {
	case 'AGENCY':
		switch (page) {
		case 'sales':
			url = '/ag/sales/report?moveMainAgencyId=' + $ele.val();
			break;
		case 'spender':
			url = '/ag/spender/report?moveMainAgencyId=' + $ele.val();
			break;
		}
		break;
	case 'SUB_AGENCY':
		switch (page) {
		case 'sales':
			if (SESSION.choosedRole == 'AGENCY' && $ele.val() == SESSION.choosedAgId) {
				url = '/ag/sales/report';
			} else {
				url = '/sag/sales/report?moveSubAgencyId=' + $ele.val();
			}
			break;
		case 'spender':
			if (SESSION.choosedRole == 'AGENCY' && $ele.val() == SESSION.choosedAgId) {
				url = '/ag/spender/report';
			} else {
				url = '/sag/spender/report?moveSubAgencyId=' + $ele.val();
			}
			break;
		}
		break;
	case 'ADVERTISER':
		if (SESSION.choosedRole == 'AGENCY') {
			switch (page) {
			case 'sales':
				url = '/adv/sales/report?moveMainAgencyId=%s&moveSubAgencyId=%s&moveAdvId=%s'.format($ele.data('agency'), $ele.data('sub_agency'), $ele.val());
				break;
			case 'spender':
				url = '/adv/manage/campaign?moveMainAgencyId=%s&moveSubAgencyId=%s&moveAdvId=%s'.format($ele.data('agency'), $ele.data('sub_agency'), $ele.val());
				break;
			}
		} else if (SESSION.choosedRole == 'SUB_AGENCY') {
			switch (page) {
			case 'sales':
				url = '/adv/sales/report?moveMainAgencyId=%s&moveSubAgencyId=%s&moveAdvId=%s'.format($ele.data('agency'), $ele.data('sub_agency'), $ele.val());
				break;
			case 'spender':
				url = '/adv/manage/campaign?moveMainAgencyId=%s&moveSubAgencyId=%s&moveAdvId=%s'.format($ele.data('agency'), $ele.data('sub_agency'), $ele.val());
				break;
			case 'manage':
				url = '/adv/manage/campaign/info?adCampaignId=%s&moveMainAgencyId=%s&moveSubAgencyId=%s&moveAdvId=%s'.format($ele.data('campaign_id'), $ele.data('agency'), $ele.data('sub_agency'), $ele.val());
				break;
			}
		}
		break;
	}
	location.href = url;
}

function downloadXlsx() {
	$('#export_data').submit();
}

function exportDataSetting() {
	if ($('input[name=filterType]:checked').val() == 'agency') {
		$('#export_column').val('["Agency", "Total Sales", "Service", "Commission"]');
	} else if ($('input[name=filterType]:checked').val() == 'adv' || $('input[name=filterType]:checked').val() == '' || !$('input[name=filterType]:checked').val()) {
		if (SESSION.choosedRole == 'ADMIN') {
			$('#export_column').val('["Advertiser", "Agency", "Industry", "Total Sales", "Service", "Commission"]');
		} else if (SESSION.choosedRole == 'AGENCY') {
			$('#export_column').val('["Advertiser", "Sub Agency", "Industry", "Total Sales", "Service", "Commission"]');
		} else if (SESSION.choosedRole == 'SUB_AGENCY') {
			$('#export_column').val('["Advertiser", "Industry", "Total Sales", "Service", "Commission"]');
		}
	} else if ($('input[name=filterType]:checked').val() == 'subAg') {
		$('#export_column').val('["Sub Agency", "Total Sales", "Service", "Commission"]');
	}
	$('#export_search_start_date').val($('#search_start_date').val());
	$('#export_search_end_date').val($('#search_end_date').val());
	$('#export_view_type').val($('input[name=viewType]:checked').val());
	$('#export_product_type').val($('#search_product_type').val());
	$('#export_filter_type').val($('input[name=filterType]:checked').val());
	$('#export_industry').val($('#select_industry_value').val());
}

function selectFilterType(ele) {
	var $ele = $(ele);
	if ($ele.val() == 'agency' || $ele.val() == 'subAg') {
		$('#adv_industry_tr').hide();
	} else if ($ele.val() == 'adv') {
		$('#adv_industry_tr').show();
	}
}

function searchData() {
	exportDataSetting();
	getSpenderReportData();
}

function selectIndustry($this) {
	var _text = $this.attr('data-text');
	var _value = $this.attr('data-value') + '@_';
	if (_text == 'All') {
		_value = 'All';
	}
	var $iv = $('#industry_view');
	$iv.attr('data-is-selected', true).removeClass('_open');
	$iv.find('._selected').removeClass('_selected');
	$iv.find('.selection').text(_text);
	$('#industry').val(_text);
	if ($this.parent().parent().hasClass('depth')) {
		_value = $this.parent().parent().parent().find('span').attr('data-value') + '_' + $this.attr('data-value');
	}
	$('#select_industry_value').val(_value);
	$this.addClass('_selected');
}

function drawIndustry($this, e) {
	var $target = $this.closest('[data-draw-industry]');
	if ($target.hasClass('_open')) {
		$target.removeClass('_open');
	} else {
		$target.addClass('_open');
	}
	e.stopPropagation();
}

function getSpenderReportData() {
	var viewType = $('input[name=viewType]:checked').val();
	var filterType = $('input[name=filterType]:checked').val();
	wise.ajax({
		url : rootPath + '/sag/spender/report.ajax',
		type : 'POST',
		data : $('#search_form').serialize(),
		dataType : 'JSON',
		success : function(data) {
			if (filterType == 'agency') {
				$('#graph_title').text('5 Leading Sales Agencies');
				$('#total_graph_title').text('Total Sales Distribution by Agency');
				$('#daily_graph_title').text('Daily Sales by Agency');
				$('#table_title').text('Sales by Agency');
			} else if (filterType == 'subAg') {
				$('#graph_title').text('5 Leading Sales Sub Agencies');
				$('#total_graph_title').text('Total Sales Distribution by Sub Agency');
				$('#daily_graph_title').text('Daily Sales by Sub Agency');
				$('#table_title').text('Sales by Sub Agency');
			} else if (filterType == 'adv' || typeof filterType === 'undefined') {
				$('#graph_title').text('5 Leading Sales Advertiser');
				$('#total_graph_title').text('Total Sales Distribution by Advertiser');
				$('#daily_graph_title').text('Daily Sales by Advertiser');
				$('#table_title').text('Sales by Advertiser');
			}
			data = makeSumData(data);
			drawTable(data);
			drawChart(data);
		}
	});
}

function makeSumData(data) {
	var totalData = new Array();
	for ( var i in data.adInsightTableArray) {
		sumReportStatsData(totalData, data.adInsightTableArray[i]);
	}
	for ( var i in TABLE_STATS) {
		if (!totalData[TABLE_STATS[i]]) {
			totalData[TABLE_STATS[i]] = 0;
		}
	}
	data['totalData'] = totalData;
	return data;
}

function drawChart(data) {
	var dateArray = new Array();
	var agencyTopIdList = new Array();
	for ( var i in data.dateArray) {
		if ($('input[name=viewType]:checked').val() == 'month') {
			dateArray.push(moment(data.dateArray[i]).format('YYYY/MM'));
		} else {
			dateArray.push(moment(data.dateArray[i]).format('MM/DD'));
		}
	}
	for ( var i in data.adInsightTableArray) {
		if (i < 5) {
			agencyTopIdList.push(data.adInsightTableArray[i].id);
		}
	}
	if (data.adInsightTableArray.length > 5) {
		agencyTopIdList.push('etc');
	}
	data['agencyIdArray'] = agencyTopIdList;
	var dailyChartData = {
		chartData : makeDataForDailyChart(data),
		chartArray : dateArray,
		agencyIdArray : agencyTopIdList
	}
	drawTotalChart(makeDataForTotalChart(dailyChartData));
	drawDailyChart(dailyChartData);
}

function makeDataForTotalChart(data) {
	var chartData = new Array();
	var totalVal = 0;
	for ( var i in data.chartData) {
		var val = 0;
		for ( var dataI in data.chartData[i].data) {
			val += data.chartData[i].data[dataI];
			totalVal += data.chartData[i].data[dataI];
		}
		data.chartData[i].totalData = val;
	}
	for ( var idIndex in data.agencyIdArray) {
		var value = 0;
		if (data.chartData[data.agencyIdArray[idIndex]].totalData != 0 && totalVal != 0) {
			value = data.chartData[data.agencyIdArray[idIndex]].totalData / totalVal * 100;
		}
		chartData.push({
			name : value.toFixed(1) + '%',
			realValue : data.chartData[data.agencyIdArray[idIndex]].totalData,
			subName : data.chartData[data.agencyIdArray[idIndex]].name,
			y : Math.round(value)
		});
	}
	return chartData;
}

function makeDataForDailyChart(data) {
	var etcVal = 0;
	var chartData = {};
	for ( var dataIndex in data.dateArray) {
		for ( var i in data.adInsightTableArray) {
			if (i > 4) {
				break;
			}
			if (!chartData[data.adInsightTableArray[i].id]) {
				chartData[data.adInsightTableArray[i].id] = {
					name : data.adInsightTableArray[i].name,
					data : new Array()
				}
			}
			var val = 0;
			if (data.adInsightMap[data.dateArray[dataIndex]][data.adInsightTableArray[i].id]) {
				val = data.adInsightMap[data.dateArray[dataIndex]][data.adInsightTableArray[i].id].totalSales;
			}
			chartData[data.adInsightTableArray[i].id].data.push(val);
		}
		if (data.adInsightTableArray.length > 5) {
			etcVal = 0;
			for ( var i in data.adInsightMap[data.dateArray[dataIndex]]) {
				if (data.agencyIdArray.indexOf(Number(i)) == -1) {
					etcVal += data.adInsightMap[data.dateArray[dataIndex]][i].totalSales;
				}
			}
			if (!chartData['etc']) {
				chartData['etc'] = {
					name : 'etc',
					data : new Array()
				}
			}
			chartData['etc'].data.push(etcVal);
		}
	}
	return chartData;
}

function drawTotalChart(data) {
	var chartOption = defaultChartOption();
	chartOption.colors = [ '#26904a', '#39cf6c', '#165d9d', '#85d2dd', '#47a6b2', '#dedede' ];
	chartOption.chart = {
		backgroundColor : "transparent",
		plotBackgroundColor : null,
		plotBorderWidth : null,
		plotShadow : false,
		events : {
			load : function() {
				var bgX = this.plotLeft + 10;
				var bgY = 3;
				var _style = this.container.style;
				_style.backgroundImage = 'url(' + rootPath + '/images/bg_chart_ededed_188x188.png)', _style.backgroundRepeat = 'no-repeat', _style.backgroundPosition = bgX + 'px 4px'

				$('#total_sales_graph_div').find('.highcharts-legend-item span').tooltip(ellipsisTooltip);
			}
		},
		type : 'pie',
		width : '360',
		height : '200',
		style : {
			'fontFamily' : '"Roboto Condensed","Nanum Barun Gothic", "맑은 고딕", sans-serif'
		}
	};
	chartOption.credits.enabled = false;
	chartOption.exporting.enabled = false;
	chartOption.tooltip = {
		backgroundColor : "#404040",
		borderColor : "#404040",
		borderWidth : 1,
		shadow : false,
		useHTML : true,
		borderRadius : 10,
		style : {
			color : '#fff',
			padding : '0',
			margin : '0'
		},
		headerFormat : '',
		pointFormatter : function() {
			return '<span class="chart-title">' + this.subName + '</span><span class="number">' + Highcharts.numberFormat(this.realValue, '0', '.') + '</span><span class="percentage">' + this.name + '</span>';
		}
	};
	chartOption.legend = {
		layout : 'vertical',
		align : 'right',
		verticalAlign : 'middle',
		useHTML : true,
		symbolRadius : 6,
		symbolWidth : 12,
		symbolHeight : 12,
		itemMarginTop : 10,
		x : 15,
		labelFormatter : function() {
			return this.subName;
		}
	};
	chartOption.plotOptions.pie = {
		size : 190,
		allowPointSelect : false,
		cursor : 'pointer',
		dataLabels : {
			format : '{y}%',
			enabled : true,
			distance : -33,
			style : {
				fontSize : '13px',
				color : "#313131"
			}
		},
		showInLegend : true,
		states : {
			hover : {
				enabled : false
			}
		}
	};
	chartOption.series[0] = {
		innerSize : '86%',
		colorByPoint : true,
		data : data
	}
	$('#total_sales_graph_div').highcharts(chartOption);
}

function drawDailyChart(data) {
	var chartOption = defaultChartOption();
	chartOption.colors = [ '#26904a', '#39cf6c', '#165d9d', '#85d2dd', '#47a6b2', '#dedede' ];
	chartOption.chart = {
		height : '200',
		style : {
			'fontFamily' : '"Roboto Condensed","Nanum Barun Gothic", "맑은 고딕", sans-serif'
		}
	};
	chartOption.xAxis = {
		categories : data.chartArray,
		tickLength : 0,
		lineColor : '#d7d7d7'
	};
	chartOption.tooltip = {
		backgroundColor : '#404040',
		borderColor : '#404040',
		borderWidth : 1,
		shadow : false,
		useHTML : true,
		borderRadius : 10,
		style : {
			color : '#fff',
			padding : '0',
			margin : '0'
		},
		headerFormat : '',
		pointFormatter : function() {
			return Highcharts.numberFormat(this.y, '0', '.');
		}
	};
	chartOption.legend = {
		enabled : false
	};
	chartOption.plotOptions.line = {
		states : {
			hover : {
				enabled : false
			}
		}
	};

	var maxCheck = false;
	if (data.agencyIdArray.length == 1) {
		maxCheck = true;
		for ( var i in data.chartData[data.agencyIdArray[0]].data) {
			if (data.chartData[data.agencyIdArray[0]].data[i] > 0) {
				maxCheck = false;
			}
		}
	}
	var i = 0;
	for ( var id in data.chartData) {
		var reportStats = data.chartData[id];
		var oppsTemp = { // 오른쪽 초기값들
			tickPixelInterval : 35,
			gridLineColor : '#F1F1F1',
			opposite : true,
			title : {
				text : ''
			},
			labels : {
				enabled : false
			},
			min : 0
		};
		if (maxCheck) {
			oppsTemp.max = 1;
		}
		var line_temp = {
			type : 'line',
			name : '',
			yAxis : '',
			zIndex : 3,
			data : [],
			min : 0
		};
		chartOption.series[i] = line_temp;
		chartOption.yAxis[i + 1] = oppsTemp;
		chartOption.series[i].name = data.chartData[id].name;
		chartOption.series[i].data = data.chartData[id].data;
		chartOption.series[i].yAxis = i + 1;
		i++;
	}
	$('#daily_sales_graph_div').highcharts(chartOption);
}

function drawTable(data) {
	defaultChartOption();
	var tableAppendData = '';
	if ($('input[name=filterType]:checked').val() == 'agency') {
		tableAppendData += '<caption>Sales by Agency</caption>';
		tableAppendData += '<colgroup>';
		tableAppendData += '<col/>';
		tableAppendData += '<col/>';
		tableAppendData += '<col/>';
		tableAppendData += '<col/>';
		tableAppendData += '<col width="205px"/>';
		tableAppendData += '</colgroup>';
		tableAppendData += '<thead>';
		tableAppendData += '	<tr>';
		tableAppendData += '		<th class="agency" scope="col">';
		tableAppendData += '			Agency';
		tableAppendData += '		</th>';
		tableAppendData += '		<th class="total-sales" scope="col">';
		tableAppendData += '			Total Sales';
		tableAppendData += '		</th>';
		tableAppendData += '		<th class="service" scope="col">';
		tableAppendData += '			Service';
		tableAppendData += '		</th>';
		tableAppendData += '		<th class="commission" scope="col">';
		tableAppendData += '			Commission';
		tableAppendData += '		</th>';
		tableAppendData += '		<th scope="col">';
		tableAppendData += '			View';
		tableAppendData += '		</th>';
		tableAppendData += '	</tr>';
		tableAppendData += '</thead>';
		tableAppendData += '<tfoot>';
		tableAppendData += '	<tr>';
		tableAppendData += '		<td class="agency"><span>';
		tableAppendData += '			Total';
		tableAppendData += '		</span></td>';
		tableAppendData += '		<td class="total-sales"><span>';
		tableAppendData += Highcharts.numberFormat(data.totalData.totalSales, '0', '.');
		tableAppendData += '		</span></td>';
		tableAppendData += '		<td class="service"><span>';
		tableAppendData += Highcharts.numberFormat(data.totalData.service, '0', '.');
		tableAppendData += '		</span></td>';
		tableAppendData += '		<td class="commission"><span>';
		tableAppendData += Highcharts.numberFormat(data.totalData.commission, '0', '.');
		tableAppendData += '		</span></td>';
		tableAppendData += '		<td>';
		tableAppendData += '		</td>';
		tableAppendData += '	</tr>';
		tableAppendData += '</tfoot>';
		tableAppendData += '<tbody>';
		for ( var i in data.adInsightTableArray) {
			tableAppendData += '	<tr>';
			tableAppendData += '		<td class="agency"><span class="dotdotdot_ele">';
			tableAppendData += data.adInsightTableArray[i].name;
			tableAppendData += '		</span></td>';
			tableAppendData += '		<td class="total-sales"><span>';
			tableAppendData += Highcharts.numberFormat(data.adInsightTableArray[i].totalSales, '0', '.');
			tableAppendData += '		</span></td>';
			tableAppendData += '		<td class="service"><span>';
			tableAppendData += Highcharts.numberFormat(data.adInsightTableArray[i].service, '0', '.');
			tableAppendData += '		</span></td>';
			tableAppendData += '		<td class="commission"><span>';
			tableAppendData += Highcharts.numberFormat(data.adInsightTableArray[i].commission, '0', '.');
			tableAppendData += '		</span></td>';
			tableAppendData += '		<td>';
			tableAppendData += '			<button type="button" value="' + data.adInsightTableArray[i].id + '" class="btn-sales" onclick="changeRoleAndMovePage(this, \'sales\', \'AGENCY\');" ';
			if (data.adInsightTableArray[i].status == 1) {
				tableAppendData += 'disabled';
			}
			tableAppendData += ' >Sales</button>';
			tableAppendData += '			<button type="button" value="' + data.adInsightTableArray[i].id + '" class="btn-spender" onclick="changeRoleAndMovePage(this, \'spender\', \'AGENCY\');" ';
			if (data.adInsightTableArray[i].status == 1) {
				tableAppendData += 'disabled';
			}
			tableAppendData += ' >Spender</button>';
			tableAppendData += '		</td>';
			tableAppendData += '	</tr>';
		}
		tableAppendData += '</tbody>';
	} else if ($('input[name=filterType]:checked').val() == 'subAg') {
		tableAppendData += '<caption>Sales by Sub Agency</caption>';
		tableAppendData += '<colgroup>';
		tableAppendData += '<col/>';
		tableAppendData += '<col/>';
		tableAppendData += '<col/>';
		tableAppendData += '<col/>';
		tableAppendData += '<col width="205px"/>';
		tableAppendData += '</colgroup>';
		tableAppendData += '<thead>';
		tableAppendData += '	<tr>';
		tableAppendData += '		<th class="agency" scope="col">';
		tableAppendData += '			Sub Agency';
		tableAppendData += '		</th>';
		tableAppendData += '		<th class="total-sales" scope="col">';
		tableAppendData += '			Total Sales';
		tableAppendData += '		</th>';
		tableAppendData += '		<th class="service" scope="col">';
		tableAppendData += '			Service';
		tableAppendData += '		</th>';
		tableAppendData += '		<th class="commission" scope="col">';
		tableAppendData += '			Commission';
		tableAppendData += '		</th>';
		tableAppendData += '		<th scope="col">';
		tableAppendData += '			View';
		tableAppendData += '		</th>';
		tableAppendData += '	</tr>';
		tableAppendData += '</thead>';
		tableAppendData += '<tfoot>';
		tableAppendData += '	<tr>';
		tableAppendData += '		<td class="agency"><span>';
		tableAppendData += '			Total';
		tableAppendData += '		</span></td>';
		tableAppendData += '		<td class="total-sales"><span>';
		tableAppendData += Highcharts.numberFormat(data.totalData.totalSales, '0', '.');
		tableAppendData += '		</span></td>';
		tableAppendData += '		<td class="service"><span>';
		tableAppendData += Highcharts.numberFormat(data.totalData.service, '0', '.');
		tableAppendData += '		</span></td>';
		tableAppendData += '		<td class="commission"><span>';
		tableAppendData += Highcharts.numberFormat(data.totalData.commission, '0', '.');
		tableAppendData += '		</span></td>';
		tableAppendData += '		<td>';
		tableAppendData += '		</td>';
		tableAppendData += '	</tr>';
		tableAppendData += '</tfoot>';
		tableAppendData += '<tbody>';
		for ( var i in data.adInsightTableArray) {
			tableAppendData += '	<tr>';
			tableAppendData += '		<td class="agency"><span class="dotdotdot_ele">';
			tableAppendData += data.adInsightTableArray[i].name;
			tableAppendData += '		</span></td>';
			tableAppendData += '		<td class="total-sales"><span>';
			tableAppendData += Highcharts.numberFormat(data.adInsightTableArray[i].totalSales, '0', '.');
			tableAppendData += '		</span></td>';
			tableAppendData += '		<td class="service"><span>';
			tableAppendData += Highcharts.numberFormat(data.adInsightTableArray[i].service, '0', '.');
			tableAppendData += '		</span></td>';
			tableAppendData += '		<td class="commission"><span>';
			tableAppendData += Highcharts.numberFormat(data.adInsightTableArray[i].commission, '0', '.');
			tableAppendData += '		</span></td>';
			tableAppendData += '		<td>';
			tableAppendData += '			<button type="button" value="' + data.adInsightTableArray[i].id + '" class="btn-sales" onclick="changeRoleAndMovePage(this, \'sales\', \'SUB_AGENCY\');" ';
			if (data.adInsightTableArray[i].status == 1) {
				tableAppendData += 'disabled';
			}
			tableAppendData += '			>Sales</button>';
			tableAppendData += '			<button type="button" value="' + data.adInsightTableArray[i].id + '" class="btn-spender" onclick="changeRoleAndMovePage(this, \'spender\', \'SUB_AGENCY\');" ';
			if (data.adInsightTableArray[i].status == 1) {
				tableAppendData += 'disabled';
			}
			tableAppendData += '			>Spender</button>';
			tableAppendData += '		</td>';
			tableAppendData += '	</tr>';
		}
		tableAppendData += '</tbody>';
	} else if ($('input[name=filterType]:checked').val() == 'adv' || !$('input[name=filterType]:checked').val()) {
		tableAppendData += '<caption>Aales by Advertiser</caption>';
		tableAppendData += '<colgroup>';
		if (SESSION.choosedRole == 'SUB_AGENCY') {
			tableAppendData += '<col/>';
			tableAppendData += '<col/>';
			tableAppendData += '<col/>';
			tableAppendData += '<col/>';
			tableAppendData += '<col/>';
			tableAppendData += '<col width="210px"/>';
		} else {
			tableAppendData += '<col/>';
			tableAppendData += '<col/>';
			tableAppendData += '<col/>';
			tableAppendData += '<col/>';
			tableAppendData += '<col/>';
			tableAppendData += '<col/>';
			tableAppendData += '<col width="205px"/>';
		}
		tableAppendData += '</colgroup>';
		tableAppendData += '<thead>';
		tableAppendData += '	<tr>';
		tableAppendData += '		<th class="advertiser" scope="col">';
		tableAppendData += '			Advertiser';
		tableAppendData += '		</th>';
		if (SESSION.choosedRole != 'SUB_AGENCY') {
			tableAppendData += '		<th class="agency" scope="col">';
			if (SESSION.choosedRole == 'AGENCY') {
				tableAppendData += '			Sub Agency';
			} else if (SESSION.choosedRole == 'ADMIN') {
				tableAppendData += '			Agency';
			}
		}
		tableAppendData += '		</th>';
		tableAppendData += '		<th class="industry" scope="col">';
		tableAppendData += '			Industry';
		tableAppendData += '		</th>';
		tableAppendData += '		<th class="total-sales" scope="col">';
		tableAppendData += '			Total Sales';
		tableAppendData += '		</th>';
		tableAppendData += '		<th class="service" scope="col">';
		tableAppendData += '			Service';
		tableAppendData += '		</th>';
		tableAppendData += '		<th class="commission" scope="col">';
		tableAppendData += '			Commission';
		tableAppendData += '		</th>';
		tableAppendData += '		<th scope="col">';
		tableAppendData += '			View';
		tableAppendData += '		</th>';
		tableAppendData += '	</tr>';
		tableAppendData += '</thead>';
		tableAppendData += '<tfoot>';
		tableAppendData += '	<tr>';
		tableAppendData += '		<td class="advertiser"><span>';
		tableAppendData += '			Total';
		tableAppendData += '		</span></td>';
		if (SESSION.choosedRole != 'SUB_AGENCY') {
			tableAppendData += '		<td class="agency"><span>';
			tableAppendData += '		</span></td>';
		}
		tableAppendData += '		<td class="industry"><span>';
		tableAppendData += '		</span></td>';
		tableAppendData += '		<td class="total-sales"><span>';
		tableAppendData += Highcharts.numberFormat(data.totalData.totalSales, '0', '.');
		tableAppendData += '		</span></td>';
		tableAppendData += '		<td class="service"><span>';
		tableAppendData += Highcharts.numberFormat(data.totalData.service, '0', '.');
		tableAppendData += '		</span></td>';
		tableAppendData += '		<td class="commission"><span>';
		tableAppendData += Highcharts.numberFormat(data.totalData.commission, '0', '.');
		tableAppendData += '		</span></td>';
		tableAppendData += '		<td>';
		tableAppendData += '		</td>';
		tableAppendData += '	</tr>';
		tableAppendData += '</tfoot>';
		tableAppendData += '<tbody>';
		for ( var i in data.adInsightTableArray) {
			tableAppendData += '	<tr>';
			tableAppendData += '		<td class="advertiser"><span class="dotdotdot_ele">';
			tableAppendData += data.adInsightTableArray[i].name;
			tableAppendData += '		</span></td>';
			if (SESSION.choosedRole != 'SUB_AGENCY') {
				tableAppendData += '		<td class="agency"><span class="dotdotdot_ele">';
				tableAppendData += data.adInsightTableArray[i].agencyName;
				tableAppendData += '		</span></td>';
			}
			tableAppendData += '		<td class="industry"><span class="dotdotdot_ele">';
			tableAppendData += data.adInsightTableArray[i].industry;
			tableAppendData += '		</span></td>';
			tableAppendData += '		<td class="total-sales"><span>';
			tableAppendData += Highcharts.numberFormat(data.adInsightTableArray[i].totalSales, '0', '.');
			tableAppendData += '		</span></td>';
			tableAppendData += '		<td class="service"><span>';
			tableAppendData += Highcharts.numberFormat(data.adInsightTableArray[i].service, '0', '.');
			tableAppendData += '		</span></td>';
			tableAppendData += '		<td class="commission"><span>';
			tableAppendData += Highcharts.numberFormat(data.adInsightTableArray[i].commission, '0', '.');
			tableAppendData += '		</span></td>';
			tableAppendData += '		<td>';
			tableAppendData += '			<button type="button" value="' + data.adInsightTableArray[i].id + '" data-agency = "' + data.adInsightTableArray[i].agencyId + '" data-sub_agency = "' + data.adInsightTableArray[i].subAgencyId + '" class="btn-sales" onclick="changeRoleAndMovePage(this, \'sales\', \'ADVERTISER\');" ';
			if (data.adInsightTableArray[i].status == 1) {
				tableAppendData += 'disabled';
			}
			tableAppendData += ' >Sales</button>';
			tableAppendData += '			<button type="button" value="' + data.adInsightTableArray[i].id + '" data-agency = "' + data.adInsightTableArray[i].agencyId + '" data-sub_agency = "' + data.adInsightTableArray[i].subAgencyId
					+ '" class="btn-spender" onclick="changeRoleAndMovePage(this, \'spender\', \'ADVERTISER\');" ';
			if (data.adInsightTableArray[i].status == 1) {
				tableAppendData += 'disabled';
			}
			tableAppendData += ' >Spender</button>';
			tableAppendData += '		</td>';
			tableAppendData += '	</tr>';
		}
		tableAppendData += '</tbody>';
	}
	if ($('#spender_table').hasClass('dataTable')) {
		$('#spender_table').DataTable().destroy();
	}
	$('#spender_table').children().remove();
	$('#spender_table').append(tableAppendData);
	var orderIndex = 1;
	if ($('input[name=filterType]:checked').val() == 'adv' || !$('input[name=filterType]:checked').val()) {
		orderIndex = 3;
	}
	var table = $('#spender_table').dataTable({
		'bFilter' : false,
		"order" : [ [ orderIndex, "desc" ] ],
		'info' : false,
		'paging' : false,
		'bLengthChange' : false,
		'retrieve' : true,
		'autoWidth' : false
	});
	$('#spender_table').find('.dotdotdot_ele').tooltip(ellipsisTooltip);
}
