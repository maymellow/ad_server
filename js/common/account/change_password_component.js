var changePasswordComponent = new ChangePasswordComponent();

function ChangePasswordComponent() {
	// init
	{
		window.passwordPopup = $.extend({}, wisePopupComponent);
	}

	var _formId = '#change_password_form';

	this.popup = function() {
		passwordPopup.popup('/account/me/modify/password.popup', {
			popupComponentName : 'passwordPopup',
			addClass : 'popup-change-password',
			popupTitle : 'Change Password',
			okBtnCallBack : function() {
				change();
				return false;
			}
		});
	}

	this.popupForReviewer = function(reviewerId) {
		passwordPopup.popup('/account/me/modify/password.popup', {
			popupComponentName : 'passwordPopup',
			addClass : "popup-change-password",
			popupTitle : 'Change Password',
			onFormOpenned : function() {
				$('.popup-change-password').find('input[name=password]').closest('tr').remove();
			},
			okBtnCallBack : function() {
				chageForReviewer(reviewerId);
				return false;
			}
		});
	}

	var change = function() {
		var $form = $(_formId);
		if (wise.isValidate($form)) {
			wise.ajax({
				url : rootPath + '/account/me/modify/password.ajax',
				data : $form.serialize(),
				success : function(data) {
					if (data.success) {
						alert('Your password has been successfully changed.');
						passwordPopup.x();
					} else {
						if (data.message == '비밀번호 틀림.') {
							$form.find('input[name=password]').addClass('parsley-error');
							$form.find('input[name=password]').tooltip({
								animation : false,
								container : 'body',
								placement : 'top-left',
								title : 'Please re-enter your password'
							});
						} else {
							alert(data.message);
						}
					}
				}
			});
		}
	}

	var chageForReviewer = function(reviewerId) {
		var $form = $(_formId);
		if (wise.isValidate($form)) {
			wise.ajax({
				url : rootPath + '/sa/account/reviewer/chaneg_password.ajax',
				data : $form.serialize() + '&targetId=' + reviewerId,
				dataType : 'text',
				success : function() {
					alert('Account password has been successfully changed.');
					passwordPopup.x();
				}
			});

		}
	}

}
