/*
 * 2015-11-12
 * 파일 업로드 할때 리스트 보여야 한다고함.
 * 전체적으로 같이 사용하기 위한 함수들.
 * 
 * 주의사항
 * html코드 같아야한다.
 * input태그 name 속성을 꼭 비워둬야함.
 * 
 * 적용된 파일 목록.
 * create_agency.jsp
 * pending.jsp
 * popup_add_advertiser.jsp
 * popup_edit_advertiser.jsp
 * popup_add_subagency.jsp
 * popup_edit_subagency_info.jsp
 * my_account.jsp (ag, sag)
 * 
 * */

var fileComponent = (function() {

	var _viewId = '#file_component_view';

	var change = function($this) {
		var _html = '<li data-add-file>';
		_html += '		<a download>%s</a>'; // %s >> fileName
		_html += '		<button type="button" class="btn-delete" value="%s" onclick="fileComponent.removeForUpload($(this));">delete</button>'; // value="%s" >> fileName
		_html += '	</li>';

		var $ul = $this.closest(_viewId).find('[data-view-name=edit_file_list]');
		$ul.find('[data-add-file]').remove();
		$.each($this.get(0).files, function(idx, file) {
			$ul.append(_html.format(file.name, file.name));
		});

		$this.attr('data-remove-files', '');
		if ($ul.find('[data-add-file]').length == 0) {
			$this.attr('data-is-change', false);
		} else {
			$this.attr('data-is-change', true);
		}

		wise.isValidate($(_viewId).find('input[type=file]'));

		$(_viewId + ' .file__list').find('li a').tooltip(ellipsisTooltip);
	}

	var remove = function($this) {
		var $ul = $this.closest('ul');
		insertRemoveList($this, $ul);
		$this.closest('li').remove();
	}

	var removeForUpload = function($this) {
		var $file = $this.closest(_viewId).find('input[type=file]'); // $('#business_file'); // XXX 두가지 있을 경우 생각하기.
		insertRemoveList($this, $file);
		var _cnt = $this.closest('ul').find('[data-add-file]').length;
		if (_cnt == 1) {
			$file.attr('data-is-change', false);
			$file.val('').attr('data-remove-files', '');
		}

		var $li = $this.closest('li');
		var _tooltipId = $li.attr('aria-describedby');
		if (typeof _tooltipId !== 'undefined') {
			$('#' + _tooltipId).remove();
		}

		$li.remove();
		wise.isValidate($file);
	}

	var insertRemoveList = function($this, $target) {
		var _r = $target.attr('data-remove-files');
		if (_r == '' || typeof _r == "undefined") {
			$target.attr('data-remove-files', $this.val());
		} else {
			$target.attr('data-remove-files', '%s,%s'.format(_r, $this.val()));
		}
	}

	var makeFormData = function($form) {
		this.make = function($form_temp, _fieldId, _fieldName) {
			var $file = $form_temp.find('#' + _fieldId);
			if ($file.length == 1) {
				var _cancelArr = $file.attr('data-remove-files').split(',');
				if ($file.attr('data-is-change') == 'true') {
					$.each($file.get(0).files, function(idx, file) {
						if (_cancelArr.indexOf(file.name) == -1) {
							_formData.append(_fieldName, file);
						}
					});
				}
			}
		}

		var _formData = new FormData($form[0]);
		this.make($form, 'business_file', 'businessFile');
		this.make($form, 'employment_certificate_file', 'employmentCertificateFile');

		// 삭제할 파일 리스트 XXX 한번에 사업증,재직증명서 둘다 수정할경우가 생기면 변경해야함.
		var _removeFiles = $form.find('[data-view-name=edit_file_list]').attr('data-remove-files');
		if (_removeFiles != '') {
			_formData.append('removeFiles', _removeFiles);
		}

		return _formData;
	}

	var init = function(_type) {
		var _id1, _id2;
		if (_type == 'c') {
			_id1 = '#corporate_licens_list_edit';
			_id2 = '#corporate_licens_list_view';
		} else if (_type == 'e') {
			_id1 = '#';
			_id2 = '#';
			alert('이건 아직 안햇지~');
		} else {
			alert('누구냐 너!?');
			return false;
		}

		$(_id1).html($(_id2).html()) //
		.attr('data-remove-files', '') //
		.find('._hide').each(function() {
			$(this).removeClass('_hide');
		});
		$(_id1).find('li a').each(function() {
			$(this).text($(this).data('title'));
		});
		$(_viewId + ' .file__list').find('li a').tooltip(ellipsisTooltip);
	}

	var valid = function() {
		var $view = $(_viewId);
		var $ul = $view.find('ul[data-view-name="edit_file_list"]');
		var $file = $view.find('input[type=file]');

		var isImgOrPdfFile = function() {
			var _check = true;
			var allowedFiles = [ 'JPG' ]; // , 'PDF'
			$ul.find('li button').each(function() {
				var _fileName = $(this).val();
				if (_fileName.indexOf('.') >= 0) {
					var _extension = _fileName.substring(_fileName.lastIndexOf('.') + 1, _fileName.length).toUpperCase();
					if (allowedFiles.indexOf(_extension) == -1) {
						_check = false;
						// return wise.validationUi($(), $ul.prevAll('button'), 'JPG, PDF 파일만 업로드 가능.');
						wise.validationUi($(), $(this).closest('li'), 'You can only upload images in jpg format.'); // .closest('li') // You can only upload images in jpg or pdf format.
					}
				}
			});

			if (_check) {
				if (!$file.prop('required')) {
					$file.addClass('parsley-success').removeClass('parsley-error');
				}
			} else {
				if (!$file.prop('required')) {
					$file.addClass('parsley-error').removeClass('parsley-success');
				}
			}
			return _check;
		}

		wise.isValidate($file);

		if ($ul.find('li:not("._hide")').length == 0) {
			if (!$file.prop('required')) {
				$file.addClass('parsley-error').removeClass('parsley-success');
			}
			return false;
		} else {
			return isImgOrPdfFile();
		}
	}

	return {
		change : change,
		remove : remove,
		removeForUpload : removeForUpload,
		makeFormData : makeFormData,
		init : init,
		valid : valid
	}

}());