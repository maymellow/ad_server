// TODO 승인 비승인 처리하는 로직 공통으로 묶으려다 망했음. 

function changeStatusOfAccount(options) {
	// 이 함수 수정되면 사용하는 곳 agency, advertiser 확인하기.
	if (confirm(options.confirmText)) {
		wise.ajax({
			url : rootPath + options.url,
			data : {
				targetId : options.$this.attr('data-account-id'),
				status : options.$this.attr('data-status')
			},
			success : function(data) {
				options.callback();
				var $statusView = options.$this.closest('#status_view');
				$statusView.find('button').addClass('_hide');
				switch (data.type) {
				// 승인
				case '20':
				case '30':
					$statusView.find('span').attr('class', '_approved').text('Approved by %s on %s'.format(data.name, data.date));
					$statusView.find('#disapprove_btn').removeClass('_hide');
					대행사상태에따른버튼컨트롤('approve');
					alert(data.object_name + ' has been approved.');
					break;
				// 비승인
				case '21':
				case '31':
					$statusView.find('span').attr('class', '_disapproved').text('Disapproved by %s on %s'.format(data.name, data.date));
					$statusView.find('#approve_btn').removeClass('_hide');
					대행사상태에따른버튼컨트롤('disapprove');
					break;
				}
			}
		});
	}
}

function 대행사상태에따른버튼컨트롤(_status) {
	if (_status == 'disapprove') {
		$('#commission_modify_btn, #add_service_btn').addClass('_hide');
	} else {
		$('#commission_modify_btn, #add_service_btn').removeClass('_hide');
	}
}

function changeStatusOfAgency($this) {
	var text = 'When you disapprove agency, it won’t be able to create campaigns, and any ongoing campaigns will be paused. Would you like to proceed?';
	if ($this.attr('data-status') == 'approve') {
		text = 'Are you sure you want to approve this agency?';
		changeStatusOfAccount({
			$this : $this,
			url : '/sa/account/change_status_of_agency.ajax',
			confirmText : text,
			callback : function() {
				printApproveHistoryList($this.attr('data-account-id'));
				printAgencyList({
					search : _search,
					status : $('#status').val()
				});
			}
		});
	} else {
		if (confirm(text)) {
			editAgencyDisapprovePopup($this.attr('data-account-id'), 'agency');
		}
	}
}

function changeStatusOfSubAgency($this) {
	var text = 'When you disapprove sub agency, it won’t be able to create campaigns, and any ongoing campaigns will be paused. Would you like to proceed?';
	if ($this.attr('data-status') == 'approve') {
		text = 'Are you sure you want to approve this sub agency?';
		changeStatusOfAccount({
			$this : $this,
			url : '/sa/account/change_status_of_agency.ajax',
			confirmText : text,
			callback : function() {
				if (window.location.pathname.split('/')[1] == 'ag') {
					editSubAgencyInfoPopup($this.attr('data-account-id'));
					printSubAgencyList({
						search : _search,
						status : $('#status').val()
					});
				} else {
					printApproveHistoryList($this.attr('data-account-id'));
					printAgencyList({
						search : _search,
						status : $('#status').val()
					});
				}
			}
		});
	} else {
		if (confirm(text)) {
			editAgencyDisapprovePopup($this.attr('data-account-id'), 'sub agency');
		}
	}
}

function changeStatusOfAdvertiser($this) {
	if ($this.attr('data-status') == 'approve') {
		changeStatusOfAccount({
			$this : $this,
			url : '/rev/account/change_status_of_advertiser.ajax',
			confirmText : 'Approve the advertiser.', // $this.attr('data-문구')
			callback : function() {
				printApproveHistoryList($this.attr('data-account-id'));
				printAdvertiserList({
					status : $('#status').val()
				});
			}
		});
	} else {
		if (confirm("Disapprove the advertiser.")) {
			editDisapproveAdvertiserPopup($this.attr('data-account-id'));
		}
	}
}

function editAgencyDisapprovePopup(accountId, _type) {
	wisePopupComponent.popup('/sa/account/popup.popup?p=disapproveAgency', {
		addClass : 'popup-disapprove-agency-account',
		popupTitle : _type == 'agency' ? 'Disapprove Agency Account' : 'Disapprove Sub Agency Account',
		okBtnText : 'Back',
		okBtnStyle : 'fail',
		cancelBtnText : 'OK',
		cancelBtnStyle : 'success',
		beforeOpend : function() {
			wisePopupComponent.x();
		},
		okBtnCallBack : function() {
			if (_type == 'agency') {
				approveAgnecyShowPopup_edit(accountId);
			} else if (_type == 'sub agency') {
				editSubAgencyInfoPopup(accountId);
			}
		},
		cancelBtnCallBack : function() {
			disapproveAgency({
				accountId : accountId,
				comment : $('#comment').val()
			});
			return false;
		}
	});
}

function editDisapproveAdvertiserPopup(accountId) {
	wisePopupComponent.popup('/rev/account/popup.popup?p=disapproveAdvertiser', {
		addClass : 'popup-disapprove-advertiser-account',
		popupTitle : 'Disapprove Advertiser Account',
		okBtnText : 'Back',
		okBtnStyle : 'fail',
		cancelBtnText : 'OK',
		cancelBtnStyle : 'success',
		beforeOpend : function() {
			wisePopupComponent.x();
		},
		okBtnCallBack : function() {
			approveAdvertiserPopup_edit(accountId);
		},
		cancelBtnCallBack : function() {
			approveAdvertiserProcess({
				accountId : accountId,
				comment : $('#comment').val(),
				isApprove : false
			});
			return false;
		}
	}, {
		accountId : accountId
	});
}
