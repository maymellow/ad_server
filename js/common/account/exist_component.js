var existComponent = new ExistComponent();

function ExistComponent() { // check_id_or_name exist_component

	var _loginIdElementId = '#login_id';
	var _businessNameElementId = '#business_name';

	this.id = function() {
		var $loginId = $(_loginIdElementId);
		var _minlenth = $loginId.data('parsley-length')[0];
		wise.isValidate($loginId);
		if (_minlenth > $loginId.val().length) {
			$('#id_validation').val($loginId.val());
			wise.isValidate($loginId);
			$loginId.focus();
		} else if (_minlenth <= $loginId.val().length) {
			wise.ajax({
				url : rootPath + '/check/account/exist_login_id.ajax',
				data : {
					loginId : $loginId.val()
				},
				success : function(data) {
					if (data.success) {
						$('#id_validation').val($loginId.val());
						wise.isValidate($loginId);
						if ($loginId.val().length >= 5 && $loginId.val().length <= 15) {
						}
					} else {
						alert('사용중인 아이디입니다.');
					}
				}
			});
		}
	}

	this.name = function(role) {
		var $businessName = $(_businessNameElementId);
		wise.isValidate($businessName);
		if ($businessName.val() != null && $businessName.val() != '') {
			wise.ajax({
				url : rootPath + '/check/account/exist_business_name.ajax',
				data : {
					businessName : $businessName.val(),
					role : role
				},
				dataType : 'json',
				success : function(data) {
					if (data.success) {
						$('#name_validation').val($businessName.val());
						wise.isValidate($businessName);
					} else {
						alert('사용중인 이름입니다.');
					}
				}
			});
		}
	}

	this.changeName = function() {
		var $businessName = $(_businessNameElementId);
		if ($businessName.closest('td').data('name') == $businessName.val()) {
			$businessName.next('button').prop('disabled', true);
			$('#name_validation').val($businessName.val());
		} else {
			$businessName.next('button').prop('disabled', false);
		}
	}

}
