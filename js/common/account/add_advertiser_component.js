var addAdvertiserComponent = new AddAdvertiserComponent();

function AddAdvertiserComponent() {
	var _viewId = 'add_advertiser_div';
	var _selectBoxId = '#selectAdv';

	this.initSetting = function() {
		wise.ajax({
			url : rootPath + '/ag/account/subagency/search_advertiser_list.ajax',
			success : function(data) {
				var _selectBoxOption = '';
				for ( var i in data) {
					_selectBoxOption += '<option value="%s" data-img="%s" data-advname="%s">%s</option>'.format(data[i].id, data[i].img, data[i].name, data[i].name);
				}
				$(_selectBoxId).html(_selectBoxOption).select2({
					placeholder : "Search approved advertiser",
					templateResult : addAdvertiserComponent.template,
					wise_custom_ui : true,
					theme: 'advertiser'
					
				});
			}
		});
	}

	this.init = function() {
		var _advList = $('#adv_list_view').html();
		var $addAdvertiserView = $('#add_advertiser_view');
		$addAdvertiserView.html(_advList).find('button').removeClass('_hide');
		$addAdvertiserView.find('li span').each(function() {
			$(this).text($(this).data('business_name'));
		});
		$addAdvertiserView.find('li span').tooltip(ellipsisTooltip);
		$addAdvertiserView.attr('data-adv_remove_list', '');
	}

	this.template = function(state) {
		if (!state.id) {
			return state.text;
		}
		var $state = $('<span><img src="%s" class="img-flag" style="width: 30px;" />%s</span>'.format($(state.element).data('img'), $(state.element).data('advname')));
		return $state;
	}

	this.onchange = function() {
		var $selectBox = $(_selectBoxId);
		var _selectedArr = $selectBox.val();
		var _li = '';
		for ( var idx in _selectedArr) {
			var res = $selectBox.find('option[value=%s]'.format(_selectedArr[idx]));
			_li += '<li class="addAdvLi">';
			_li += '	<input type="hidden" name="addAdvertiserList" value="%s" />'.format(res.val());
			_li += '	<span>%s</span>'.format(res.data('advname'));
			_li += '	<button type="button" class="btn-close" onclick="addAdvertiserComponent.remove(%s);">close</button>'.format(res.val());
			_li += '</li>';
		}
		$('#add_advertiser_view').find('.addAdvLi').remove();
		$('#add_advertiser_view').append(_li).find('li span').tooltip(ellipsisTooltip);
	}

	this.remove = function(advId) {
		$(_selectBoxId).find('option[value=%s]'.format(advId)).prop('selected', false).trigger('change');
	}

	this.removeForAdded = function(advId) {
		var $ul = $('#add_advertiser_view');
		var _r = $ul.attr('data-adv_remove_list');
		if (_r == '' || typeof _r == "undefined") {
			$ul.attr('data-adv_remove_list', advId);
		} else {
			$ul.attr('data-adv_remove_list', '%s,%s'.format(_r, advId));
		}
		$ul.find('#added_adv_' + advId).remove();
	}

}
