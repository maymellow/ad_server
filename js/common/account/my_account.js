$(function() {
	$('.my-account-info__table > table').find('td span').tooltip(ellipsisTooltip);
});

function modifyEmailInfo() {
	if(wise.isValidate($('#email_form'))){
		wise.ajax({
			url : rootPath + "/account/me/modify/email.ajax",
			data : {
				email : $('#email_modify_text').val()
			},
			dataType : 'text',
			success : function() {
				location.reload();
			}
		});
	}
}

function editEmailToggle() {
	var email = $('[data-view=email]').text();
	$('#email_modify_text').val(email);

	$('tr [data-view-trigger').each(function() {
		var $this = $(this);
		if ($this.hasClass('modify-show')) {
			$this.removeClass('modify-show');
			$this.addClass('modify-hide');
		} else {
			$this.removeClass('modify-hide');
			$this.addClass('modify-show');
		}
	});
}

// ///////////////////////////// TODO 아래쪽은 아직 안함.

function modifyEmailNotificationShow($this) {
	$this.hide();
	$('#email_notification_save').show();
	$('.span_info').hide();
	$('.input_info').removeClass('_hide');
	$('.email_check_ele').removeAttr('disabled');
}

function cancelEmailNotificationOption(){
	$('#email_notification_modify').show();
	$('#email_notification_save').hide();
	$('.span_info').show();
	$('.input_info').addClass('_hide');
	$('.email_check_ele').attr('disabled', 'disabled');
}

function modifyEmailNotificationSave() {
	if(wise.isValidate($('#notification_form'))){
		wise.ajax({
			type : 'GET',
			url : rootPath + SESSION.roleUrl + '/account/edit/notification.ajax',
			dataType : 'JSON',
			data : $('#notification_form').serialize(),
			success : function(data) {
				if(data){
					$('#limit_per').text($('#limit_per_text').val());
					$('#agency_count').text($('#agency_count_text').val());
					$('#advertiser_count').text($('#advertiser_count_text').val());
					$('#creative_count').text($('#creative_count_text').val());
					cancelEmailNotificationOption();
				}else{
					alert('실패 ㅠㅠ');
				}
			}
		});
	}
}
