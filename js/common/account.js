function initMenu() {
	var pathname = rootPath + window.location.pathname;
	if (pathname.indexOf(rootPath + '/adv/create/campaign/fillinfo') >= 0 || pathname.indexOf(rootPath + '/adv/create/campaign/adform') >= 0) {
		pathname = rootPath + '/adv/create/campaign/choosetype';
	} else if (pathname.indexOf(rootPath + '/adv/manage/campaign/info') >= 0) {
		pathname = rootPath + '/adv/manage/campaign';
	}
	var $a = $("#gnb a[href='" + pathname + "']");
	var $li = $a.closest("li");
	$a.addClass("_active");
	if ($li.attr("data-menu-index") == 1) {
		$li.closest("ul").css({
			"display" : "block"
		}).siblings("a").addClass("_active");
	}
}

function initAccountSelectBox() {
	switch (SESSION.role) {
	case "ADMIN":
		getAgencyListForLeftMenu();
		break;
	case "AGENCY":
		getSubAgencyListForLeftMenu(SESSION.choosedAgId);
		getAdvertiserListForLeftMenu(SESSION.choosedAgId);
		break;
	case "SUB_AGENCY":
		getAdvertiserListForLeftMenu(SESSION.choosedSubAgId);
		break;
	case "ADVERTISER":
	case "REVIEWER":
		// $("#choosed_agency_id").hide();
		// $("#choosed_advertiser_id").hide();
		break;
	}
}

function onAgencyChanged(agencyId) {
	getSubAgencyListForLeftMenu(agencyId);
	getAdvertiserListForLeftMenu(agencyId);
}

function getAgencyListForLeftMenu() {
	wise.ajax({
		url : rootPath + '/sa/account/agency_all_list.ajax',
		success : function(data) {
			var _html = '<option></option>';
			// var _html = '<option value="0">Choose Agency</option>';
			$.each(data, function(idx, ag) {
				var act = ag.account;
				if (SESSION.choosedAgId == act.id) {
					_html += '<option value="%s" selected="selected">%s</option>'.format(act.id, act.businessName);
					getSubAgencyListForLeftMenu(act.id);
					getAdvertiserListForLeftMenu(act.id);
				} else {
					_html += '<option value="%s">%s</option>'.format(act.id, act.businessName);
				}
			});
			$('#choosed_agency_id').html(_html);
			select2Box();
		}
	});
}

function getSubAgencyListForLeftMenu(agencyId) {
	wise.ajax({
		url : rootPath + '/ag/account/subagency_list.ajax',
		data : {
			agencyId : agencyId
		},
		success : function(data) {
			var _html = '<option></option>';
			// var _html = '<option value="0">Choose Sub Agency</option>';
			$.each(data, function(idx, sag) {
				var act = sag.account;
				if (SESSION.choosedSubAgId == act.id) {
					_html += '<option value="%s" selected="selected">%s</option>'.format(act.id, act.businessName);
					getAdvertiserListForLeftMenu(act.id);
				} else {
					_html += '<option value="%s">%s</option>'.format(act.id, act.businessName);
				}
			});
			$('#choosed_sub_agency_id').html(_html);
			select2Box();
		}
	});
}

function getAdvertiserListForLeftMenu(agencyId) {
	wise.ajax({
		url : rootPath + '/adv/account/advertiser_list.ajax',
		data : {
			agencyId : agencyId
		},
		success : function(data) {
			var _html = '<option></option>';
			// var _html = '<option value="0">Choose Advertiser</option>';
			$.each(data, function(idx, adv) {
				var act = adv.account;
				if (SESSION.choosedAdvId == act.id) {
					_html += '<option value="%s" selected="selected">%s</option>'.format(act.id, act.businessName);
				} else {
					_html += '<option value="%s">%s</option>'.format(act.id, act.businessName);
				}
			});
			$('#choosed_advertiser_id').html(_html);
			select2Box();
		}
	});
}

function select2Box() {
	$('#change_account_form .band-nav').select2({
		minimumResultsForSearch : Infinity,
		theme : 'band'
	});
}

function onSubAgencyChanged(subAgId) {
	if (subAgId == 0) {
		getSubAgencyListForLeftMenu(SESSION.choosedAgId);
		getAdvertiserListForLeftMenu(SESSION.choosedAgId);
	} else {
		getAdvertiserListForLeftMenu(subAgId);
	}
};

function onChangeSelectBoxAdvertiserListForSubagency() {
	if (SESSION.role == 'SUB_AGENCY') {
		$('#change_account_form').submit();
	}
}