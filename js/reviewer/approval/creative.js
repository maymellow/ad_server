var adCreativeList;
function getCreativeList() {
	$('#ad_creative_search_box').val('');
	wise.ajax({
		url : rootPath + '/rev/creative/list.list',
		type : 'POST',
		data : $('#search_form').serialize(),
		dataType : 'HTML',
		success : function(html) {
			$('#creative_list_div').html(html);
			var table = $('#ad_creative_data_table').dataTable({
				'info' : false,
				'paging' : false,
				'bLengthChange' : false,
				"order": [[ 0, "desc" ]],
				'aoColumnDefs' : [ {
					'bSearchable' : false,
					'aTargets' : [ 0, 3, 4, 5, 7 ]    //  검색에서 제외할 컬럼 지정
				} ],
				'retrieve' : true,
				'autoWidth':false
			});
			 $('#ad_creative_data_table_filter').hide();
			var ex = $('#ad_creative_data_table').DataTable();
			$('#ad_creative_search_box').on('keyup', function() {
				ex.search(this.value).draw();
			});
			$('#creative_list_div').find('.dotdotdot_ele').tooltip(ellipsisTooltip);
		}
	});
}

function approveAdCreative(adCreativeId) {
	var popup = wisePopupComponent.popup('/rev/creative/confirm.popup', {
		addClass : 'popup-approve-creative',
		popupTitle : 'Approve Creative',
		okBtnText : 'Approve',
		okBtnCallBack : function() {
			if (confirm('Are you sure you want to approve this creative?')) {
				return true;
			}
			return false;
		},
		cancelBtnText : 'Disapprove',
		cancelBtnCallBack : function() { // 거절 처리
			disapprovalPopup(adCreativeId);
		},
		beforeOpend : function() { // Back 버튼을 이용해 돌아왔을 경우에 대한 처리
			wisePopupComponent.x();
		},
		onFormOpenned : function() {
			$('#go_to_campaign').hide();
			$('#ad_creative_id').val(adCreativeId);
			$('.main-sub').find('p span').tooltip(ellipsisTooltip);
		},
		onTaskComplete : function() {
			wise.ajax({
				type : 'POST',
				url : rootPath + '/rev/creative/approve',
				dataType : 'json',
				data : {
					ad_creative_id : adCreativeId,
				},
				success : function(data) {
					if (data.msg == 'ok') {
						alert('You have approved this creative.');
						getCreativeList();
					}
				}
			});
		}
	}, {
		ad_creative_id : adCreativeId
	});
}

function disapprovalPopup(adCreativeId) {
	wisePopupComponent.popup('/rev/creative/disapproval.popup', {
		addClass : "popup-disapprove-advertiser-account",
		popupTitle : 'Disapprove Creative',
		okBtnText : 'Back',
		okBtnStyle : 'fail',
		okBtnCallBack : function() {
			approveAdCreative(adCreativeId); // 뒤로가기
			return false;
		},
		cancelBtnText : 'OK',
		cancelBtnStyle : 'success',
		cancelBtnCallBack : function() { // 거절 처리
			if(wise.isValidate($('#disapprove_form'))){
				wise.ajax({
					type : 'POST',
					url : rootPath + '/rev/creative/disapprove.ajax',
					dataType : 'json',
					data : {
						ad_creative_id : adCreativeId,
						message : $('#comment').val()
					},
					success : function(data) {
						getCreativeList();
						wisePopupComponent.x();
					}
				});
				// false를 리턴하는 이유는 팝업이 바로 꺼지지 않고 ajax 처리가 완료되는 시점에 팝업이 꺼지게 하기 위함이다.
			}
			return false;
		},
		beforeOpend : function() {
			wisePopupComponent.x(); // 열려있던 승인창을 닫는다.
		},
		onTaskComplete : function() {
		}
	}, {
		ad_creative_id : adCreativeId
	});
}

function viewAdCreative(adCreativeId) {
	wisePopupComponent.popup('/rev/view/creative.popup', {
		addClass : 'popup-view-creative-admin',
		popupTitle : 'View Creative',
		removeFooter : true,
		onFormOpenned : function() {
			$('.approved-status').find('span').tooltip(ellipsisTooltip);
			$('.popup-view-creative-admin').each(function() {
				historyResize($(this));
			});
			$('#popup_ad_creative_history').animate({
				scrollTop : $('#popup_ad_creative_history > ul').height()
			}, 'slow');
		},
		
		beforeOpend : function() {
			wisePopupComponent.x(); // 열려있던 승인창을 닫는다.
		},
	}, {
		ad_creative_id : adCreativeId
	});
}