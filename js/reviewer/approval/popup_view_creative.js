function getAdCreativePreview(adCreativeId) {
	wise.ajax({
		type : 'POST',
		// url : rootPath + SESSION.roleUrl + '/get/creative/preview.ajax', // admin에서 에러남
		url : rootPath + '/rev/get/creative/preview.ajax',
		dataType : 'HTML',
		data : {
			spec : '[{"id":' + adCreativeId + '}]'
		},
		success : function(data) {
			$('#ad_creative_preview').html(data);
		}
	});
}

function popupApprovalAdCreativeStatus() {
	wise.ajax({
		type : 'POST',
		url : rootPath + '/rev/creative/approve.ajax',
		dataType : 'json',
		data : $('#popup_ad_creative_form').serialize(),
		success : function(data) {
			if (data.msg == 'ok') {
				getCreativeList();
				viewAdCreative($('#popup_ad_creative_id').val());
			} else {
				alert('fail');
			}
		}
	});
}

function popupDisapprovalAdCreative(adCreativeId) {
	wisePopupComponent.popup('/rev/creative/disapproval.popup', {
		addClass : "popup-disapprove-advertiser-account",
		popupTitle : 'Disapprove Creative',
		okBtnText : 'Back',
		okBtnStyle : 'fail',
		okBtnCallBack : function() {
			viewAdCreative(adCreativeId); // 뒤로가기
			return false;
		},
		cancelBtnText : 'OK',
		cancelBtnStyle : 'success',
		cancelBtnCallBack : function() { // 거절 처리
			wise.ajax({
				type : 'POST',
				url : rootPath + '/rev/creative/disapprove.ajax',
				dataType : 'json',
				data : {
					ad_creative_id : adCreativeId,
					message : $('#comment').val()
				},
				success : function(data) {
					getCreativeList();
					wisePopupComponent.x();
				}
			});
			// false를 리턴하는 이유는 팝업이 바로 꺼지지 않고 ajax 처리가 완료되는 시점에 팝업이 꺼지게 하기 위함이다.
			return false;
		},
		beforeOpend : function() {
			wisePopupComponent.x(); // 열려있던 승인창을 닫는다.
		},
		onTaskComplete : function() {
		}
	}, {
		ad_creative_id : adCreativeId
	});
}

function popupSaveAdCreativeComment(adCreativeId) {
	if ($('#popup_ad_creative_comment').val() == '') {
		$('#popup_ad_creative_comment').addClass('parsley-error');
		$('#popup_ad_creative_comment').tooltip({
			animation : false,
			container : 'body',
			placement : 'top-left',
			title : 'This value is required.'
		});
		$('#popup_ad_creative_comment').focus();
		return;
	}
	wise.ajax({
		type : 'POST',
		url : rootPath + '/rev/creative/save/comment.ajax',
		dataType : 'json',
		data : {
			adCreativeId : adCreativeId,
			comment : $('#popup_ad_creative_comment').val()
		},
		success : function(data) {
			if (data) {
				viewAdCreative($('#popup_ad_creative_id').val());
			} else {
				alert('fail');
			}
		}
	});
}