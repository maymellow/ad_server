function popupToggleActiveStatus(ele) {
	var $ele = $(ele);
	if ($ele.hasClass('_active')) {
		$ele.removeClass('_active');
	} else {
		$ele.addClass('_active');
	}
}

function popupChooseMultiCreative(ele){
	var _val = $(ele).val();
	$('.image-wrap > img').hide();
	$('#popup_ad_creative_choose_image' + _val).show();
	$('.main-only').hide();
	$('.main-sub').hide();
	$('#popup_ad_creative_title' + _val).show();
}