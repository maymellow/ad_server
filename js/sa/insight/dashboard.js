function pageInit() {
	getInventoryStatus($('input[name="viewType"]:checked'));
	getAgencyCreditStatus();
	getApprovalRequestCount();
}

function getInventoryStatus(ele) {
	wise.ajax({
		url : rootPath + "/sa/dashboard/inventory/status.list",
		type : 'POST',
		data : {
			view_type : $(ele).val()
		},
		dataType : 'HTML',
		success : function(html) {
			$('#inventory_status_table').html(html);
		}
	});
}

function getApprovalRequestCount() {
	wise.ajax({
		url : rootPath + "/sa/dashboard/approval/count.ajax",
		type : 'GET',
		dataType : 'JSON',
		success : function(data) {
			$('#agency_request_count').text(data.agency);
			$('#advertiser_request_count').text(data.advertiser);
			$('#creative_request_count').text(data.creative);
		}
	});
}

function getAgencyCreditStatus() {
	wise.ajax({
		url : rootPath + "/sa/dashboard/agency/credit/status.list",
		type : 'POST',
		dataType : 'HTML',
		success : function(html) {
			if ($('#agency_credit_status_table').hasClass('dataTable')) {
				$('#agency_credit_status_table').DataTable().destroy();
			}
			$('#agency_credit_status_div').html(html);
			$('#agency_credit_status_table .progress').each(function() {
				wise.drawProgressChart(this);
			});
			var table = $('#agency_credit_status_table').dataTable({
				'bFilter' : false,
				"order" : [ [ 1, "desc" ] ],
				'info' : false,
				'paging' : false,
				'bLengthChange' : false,
				'retrieve' : true,
				'autoWidth':false
			});
			$('#agency_credit_status_table').find('.dotdotdot_ele').tooltip(ellipsisTooltip);
		}
	});
}

function movePage(type) {
	var url = '';
	switch (type) {
	case 'pending_agency':
		url = '/sa/account/agency/list?status=3';
		break;
	case 'agency':
		url = '/sa/account/agency/list';
		break;
	case 'advertiser':
		url = '/sa/account/advertiser/list?status=3';
		break;
	case 'creative':
		url = '/sa/creative?status=3';
		break;
	}
	$(location).attr('href', url);
}

