var accessRulesComponent = new AccessRulesComponent();
function AccessRulesComponent() {
	var _viewId = '#access_rules_div';

	this.edit = function() {
		$(_viewId).addClass('_correct');
		countrySelectBoxComponent.removeBtnShow(true);
		blackIpComponent.removeBtnShow(true);
	}

	this.cancel = function() {
		$(_viewId).removeClass('_correct');
		countrySelectBoxComponent.init();
		blackIpComponent.init();
	}

	this.submit = function() {
		wise.ajax({
			url : rootPath + '/sa/adsetting/ip_black.ajax',
			data : {
				countries : countrySelectBoxComponent.toString(),
				blackIps : blackIpComponent.toString()
			},
			success : function(data) {
				console.log(data);
			}
		});
	}
}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var countrySelectBoxComponent = new CountrySelectBoxComponent();
function CountrySelectBoxComponent() {
	var _selectBoxId = '#country_select_box';
	var _addCountryUlId = '#add_country_ul';
	this._ADD_COUNTRY_LIST = [];
	this.selectedValue;

	this.removeBtnShow = function(isShow) {
		if (isShow) {
			$(_addCountryUlId).find('button').removeClass('_hide');
		} else {
			$(_addCountryUlId).find('button').addClass('_hide');
		}
	}

	this.eventBind = function($selectBox) {
		$selectBox.off('select2:unselect').on('select2:unselect', function(stats) {
			var $option = $(stats.params.data.element);
			if (/^[0-9]+$/.test($option.val())) {
				countrySelectBoxComponent._ADD_COUNTRY_LIST.push({
					'id' : $option.val()
				});
			} else {
				countrySelectBoxComponent.deleteCountryArr($option.val());
			}
		});

		$selectBox.off('select2:select').on('select2:select', function(stats) {
			var $option = $(stats.params.data.element);
			if (/^[0-9]+$/.test($option.val())) {
				countrySelectBoxComponent.deleteCountryArr($option.val());
			} else {
				countrySelectBoxComponent._ADD_COUNTRY_LIST.push({
					'countryCode' : $option.val()
				});
			}
		});
	}

	this.init = function() {
		this._ADD_COUNTRY_LIST = [];
		var $selectBox = $(_selectBoxId);
		this.selectedValue = $selectBox.data('selected_value');

		// 셀렉트박스 초기화
		var countryList = [];
		$.each(this.selectedValue, function(idx, country) {
			countryList.push(country.id);
		});
		$selectBox.val(countryList).trigger('change');

		// 버튼 안보이게
		this.removeBtnShow(false);

		// select2 ui에 이벤트 바인딩
		this.eventBind($selectBox);
	}

	this.onchange = function() {
		var $selectBox = $(_selectBoxId);
		var $addCountryUl = $(_addCountryUlId);
		var _selectValues = $selectBox.val();
		var _selectedValue = $selectBox.data('selected_value');
		var _li = '';

		for ( var idx in _selectValues) {
			var countryCode = _selectValues[idx];
			_li += '<li class="addAdvLi" id="addCountry_%s">'.format(countryCode);
			if (/^[0-9]+$/.test(countryCode)) {
				_li += '<span>%s</span>'.format($selectBox.find('option[value=%s]'.format(countryCode)).attr('id'));
			} else {
				_li += '<span>%s</span>'.format(countryCode);
			}
			_li += '	<button type="button" class="btn-close" onclick="countrySelectBoxComponent.remove2(%s);">close</button>'.format("'addCountry_%s'".format(countryCode));
			_li += '</li>';
		}

		$addCountryUl.find('.addAdvLi').remove();
		$addCountryUl.append(_li);

		if ($selectBox.val().length == 1) {
			$addCountryUl.find('button').prop('disabled', true);
		} else {
			$addCountryUl.find('button').prop('disabled', false);
		}
	}

	this.remove2 = function(countryId) {
		var id = countryId.split('_')[1];
		if (/^[0-9]+$/.test(id)) {
			this._ADD_COUNTRY_LIST.push({
				'id' : id
			});
		} else {
			this.deleteCountryArr(id);
		}
		$(_selectBoxId).find('option[value=%s]'.format(id)).prop('selected', false).trigger('change');
	}

	this.toString = function() {
		return JSON.stringify(this._ADD_COUNTRY_LIST);
	}

	this.deleteCountryArr = function(id) {
		var tmpIdx;
		var isNum = /^[0-9]+$/.test(id);
		$.each(this._ADD_COUNTRY_LIST, function(idx, obj) {
			if (isNum) {
				if (obj.id == id) {
					tmpIdx = idx;
					return;
				}
			} else {
				if (obj.countryCode == id) {
					tmpIdx = idx;
					return;
				}
			}
		});
		if (typeof tmpIdx !== 'undefined') {
			this._ADD_COUNTRY_LIST.splice(tmpIdx, 1);
		}
	}
}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var blackIpComponent = new BlackIpComponent();
function BlackIpComponent() {
	var _blackIpTdId = '#black_ip_td';
	var _formId = '#black_ip_input_form';
	var _blackIpUlId = '#black_ip_ul';
	var _blackIpExcelUlId = '#black_ip_excel_ul';
	var _blackIpInputId = '#black_ip_input';
	var _excelInput = '#excel_input';
	this._ADDED_IP_LIST = [];
	this._IP_LIST = [];
	this._EXCEL_IP_LIST = [];

	// 배열에 추가.
	this.pushIp = function(_ipObject) {
		// 고유 아이디 생성. 삭제할때 인덱스로 사용.
		var id = '%s-%s'.format(wise.guid(), wise.guid());
		_ipObject.guid = id;
		this._IP_LIST.push(_ipObject);
		return id;
	}

	// 초기화
	this.init = function() {
		var that = this;
		var addedValue = $(black_ip_input).data('added_value');
		$(_blackIpUlId).empty();
		$.each(addedValue, function(idx, addedIp) {
			that.draw(that.toIpStirng(addedIp), addedIp.id);
		});
		this._IP_LIST = [];
		this._ADDED_IP_LIST = addedValue;
		this.removeBtnShow(false);
	}

	this.isIp = function(str) {
		var ipExp = /^(?:(?:(?:(?:25[0-5]|2[0-4][0-9]|1?[0-9][0-9]?)(?:-(?:25[0-5]|2[0-4][0-9]|1?[0-9][0-9]?))?)|\*)\.){3}(?:(?:(?:25[0-5]|2[0-4][0-9]|1?[0-9][0-9]?)(?:-(?:25[0-5]|2[0-4][0-9]|1?[0-9][0-9]?))?)|\*)$/;
		return ipExp.test(str);
	}

	// 추가 버튼.
	this.add = function() {
		var $td = $(_blackIpTdId);
		var $form = $td.find(_formId);
		var _inputValue = $form.find(_blackIpInputId).val();

		// 파슬리밸리데이션, 정규식 검사.
		if (wise.isValidate($form) && this.isIp(_inputValue)) {
			// ip 범위 검사.
			if (this.rangeCheck(_inputValue)) {
				var _ipObject = this.parse(_inputValue);
				// 중복값인지 검사.
				if (!this.isExist(_ipObject)) {
					// 화면 그리기.
					this.draw(_inputValue, this.pushIp(_ipObject));
				}
			}
		}
	}

	// 화면 그리기.
	this.draw = function(_ipText, _id) {
		var _li = '<li id="%s">'.format('ip_%s'.format(_id));
		_li += '		<span>%s</span>'.format(_ipText);
		_li += '		<button type="button" class="btn-close" onclick="blackIpComponent.remove(%s);">close</button>'.format("'ip_%s'".format(_id));
		_li += '	<li>';
		$(_blackIpUlId).append(_li);
	}

	// ip 범위 검사.
	this.rangeCheck = function(_ipRange) {
		var ipArr = _ipRange.split('.');
		var _check = true;
		for ( var i in ipArr) {
			if (ipArr[i].indexOf('-')) {
				var tmpArr = ipArr[i].split('-');
				if (parseInt(tmpArr[0]) > parseInt(tmpArr[1])) {
					_check = false;
				}
			}
		}
		if (!_check) {
			// TODO false일때 오류메시지 뿌리기.
			alert('ip 범위설정 오류');
		}
		return _check;
	}

	// string으로 되어있는 ip를 object로 변환.
	this.parse = function(_ipRange) {
		// 디폴트 오브젝트. 어떤식으로 저장될지 보기위해 초기화.
		var ip = {
			// 'deleteId' : 0,
			'guid' : '',
			'ipBlock1Min' : 0,
			'ipBlock1Max' : 0,
			'ipBlock2Min' : 0,
			'ipBlock2Max' : 0,
			'ipBlock3Min' : 0,
			'ipBlock3Max' : 0,
			'ipBlock4Min' : 0,
			'ipBlock4Max' : 0
		};
		var ipArr = _ipRange.split('.');

		for (var i = 0; i < ipArr.length; i++) {
			if (ipArr[i] == '*') {
				ip['ipBlock%sMin'.format(i + 1)] = 0;
				ip['ipBlock%sMax'.format(i + 1)] = 255;
			} else if (/^[0-9]+$/.test(ipArr[i])) {
				ip['ipBlock%sMin'.format(i + 1)] = parseInt(ipArr[i]);
				ip['ipBlock%sMax'.format(i + 1)] = parseInt(ipArr[i]);
			} else if (ipArr[i].indexOf('-')) {
				var tmpIp = ipArr[i].split('-');
				ip['ipBlock%sMin'.format(i + 1)] = parseInt(tmpIp[0]);
				ip['ipBlock%sMax'.format(i + 1)] = parseInt(tmpIp[1]);
			}
		}
		return ip;
	}

	// 중복값인지 검사. 중복값이 있으면 true
	this.isExist = function(_ipObject) {
		var totalIpList = this._IP_LIST.concat(this._ADDED_IP_LIST).concat(this._EXCEL_IP_LIST);
		for (var idx = 0; idx < totalIpList.length; idx++) {
			var _is_exist = true;
			var _ipObj = totalIpList[idx];
			for (var i = 0; i < 4; i++) {
				var aMin = _ipObj['ipBlock%sMin'.format(i + 1)];
				var aMax = _ipObj['ipBlock%sMax'.format(i + 1)];
				var tMin = _ipObject['ipBlock%sMin'.format(i + 1)];
				var tMax = _ipObject['ipBlock%sMax'.format(i + 1)];
				_is_exist = _is_exist && this.isExistForBlock(aMin, aMax, tMin, tMax);
				if (i == 3 && _is_exist) {
					// TODO 에러 문구 띄우기
					alert('중복값입니다.');
					return true;
				}
			}
		}
		return false;
	}

	// 비교해서 같은 값이 있으면 true
	this.isExistForBlock = function(addedMin, addedMax, targetMin, targetMax) {
		for (var a = addedMin; a <= addedMax; a++) {
			for (var t = targetMin; t <= targetMax; t++) {
				if (a == t) {
					return true;
				}
			}
		}
		return false;
	}

	// 화면상의 ip list를 지운다.
	this.remove = function(id) {
		var _id = id.split('_')[1];
		if (/^[0-9]+$/.test(_id)) {
			this._IP_LIST.push({
				'id' : _id
			});
			// this._IP_LIST.push(this.getIpObjectForAddedIp(_id));
			this.deleteIp(this._ADDED_IP_LIST, _id);
		} else {
			this.deleteIp(this._IP_LIST, _id);
		}
		$(_blackIpUlId).find('#ip_' + _id).remove();
	}

	// _IP_LIST 에 담겨있는 특정 ip를 지운다.
	this.deleteIp = function(arr, id) {
		var _isNum = /^[0-9]+$/.test(id);
		for ( var i in arr) {
			var _ip = arr[i];
			if (_isNum) {
				if (_ip.id == id) {
					arr.splice(i, 1);
					break;
				}
			} else {
				if (_ip.guid == id) {
					arr.splice(i, 1);
					break;
				}
			}
		}
	}

	this.getAddedValue = function() {
		return this._IP_LIST;
	}

	this.toString = function() {
		return JSON.stringify(this._IP_LIST.concat(this._EXCEL_IP_LIST));
	}

	this.toIpStirng = function(_obj) {
		var strIp = this.toIpStirngForBlock(_obj.ipBlock1Min, _obj.ipBlock1Max);
		strIp += '.';
		strIp += this.toIpStirngForBlock(_obj.ipBlock2Min, _obj.ipBlock2Max);
		strIp += '.';
		strIp += this.toIpStirngForBlock(_obj.ipBlock3Min, _obj.ipBlock3Max);
		strIp += '.';
		strIp += this.toIpStirngForBlock(_obj.ipBlock4Min, _obj.ipBlock4Max);
		return strIp;
	}

	this.toIpStirngForBlock = function(min, max) {
		var block = '';
		if (min == max) {
			block = min;
		} else {
			if (min > max) {
				console.log('이거 이상한데!!!');
			} else if (min == 0 && max == 255) {
				block = '*';
			} else {
				block = '%s-%s'.format(min, max);
			}
		}
		return block;
	}

	this.removeBtnShow = function(isShow) {
		if (isShow) {
			$(_blackIpUlId).find('button').removeClass('_hide');
		} else {
			$(_blackIpUlId).find('button').addClass('_hide');
		}
	}

	this.getIpObjectForAddedIp = function(id) {
		var tmpIpObj;
		$.each(this._ADDED_IP_LIST, function(idx, ip) {
			if (ip.id == id) {
				tmpIpObj = ip;
				return;
			}
		});
		return tmpIpObj;
	}

	// 엑셀파일 선택 됐을때.
	this.handleFile = function(e) {
		// https://github.com/SheetJS/js-xlsx
		var files = e.target.files;
		var i, f;
		for (i = 0, f = files[i]; i != files.length; ++i) {
			var reader = new FileReader();
			var name = f.name;
			reader.onload = function(e) {
				var data = e.target.result;
				var workbook = XLSX.read(data, {
					type : 'binary'
				});
				blackIpComponent.addForExcel(workbook, name);
			};
			reader.readAsBinaryString(f);
		}
	}

	this.addForExcel = function(workbook, fileName) {
		var ipStrArr = workbook.Strings;
		var availableIpArr = []; // 사용가능한 ip들
		var unavailableIpArr = []; // 사용불가능한 ip들

		for (var i = 0; i < ipStrArr.Count; i++) {
			var ipStr = ipStrArr[i].h;
			// ip 형태의 text인지 검사.
			if (this.isIp(ipStr)) {
				// ip 범위 검사.
				if (this.rangeCheck(ipStr)) {
					var _ipObject = this.parse(ipStr);
					// 중복값인지 검사.
					if (!this.isExist(_ipObject)) {
						availableIpArr.push(ipStr);
						this._EXCEL_IP_LIST.push(_ipObject);
					} else {
						unavailableIpArr.push(ipStr);
					}
				} else {
					unavailableIpArr.push(ipStr);
				}
			} else {
				unavailableIpArr.push(ipStr);
			}
		}
		this.drwaForExcel(availableIpArr, unavailableIpArr, fileName);
	}

	this.drwaForExcel = function(ipArr, unavailableIpArr, fileName) {
		var _li = '<li><span>%s &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><button type="button" class="btn-close" onclick="blackIpComponent.removeExcel();">close</button><li>'.format(fileName);
		for ( var i in ipArr) {
			_li += '<li><span>%s</span><li>'.format(ipArr[i]);
		}
		if (unavailableIpArr.length != 0) {
			_li += '<li><span>%s</span><ul>'.format('등록불가.');
			for ( var i in unavailableIpArr) {
				_li += '<li><span>%s</span><li>'.format(unavailableIpArr[i]);
			}
			_li += '</ul><li>';
		}
		$(_blackIpExcelUlId).empty().append(_li);
	}

	this.removeExcel = function() {
		this._EXCEL_IP_LIST = [];
		$(_blackIpExcelUlId).empty();
		$(_excelInput).val('');
	}

}
