/*
 * 
 * 팝업창 내 에서 발생하는 모든 기능들.
 * 1. 팝업 열기
 * 2. 팝업 내의 부가 기능.
 * 
 * */
function addReviewerPopup() {
	wisePopupComponent.popup('/sa/account/popup.popup?p=addReviewer', {
		addClass : 'popup-add-sub-agency', // 스타일 재활용
		popupTitle : 'Add Approval Manager',
		okBtnCallBack : function() {
			createReviewer();
			return false;
		}
	});
}

function createReviewer() {
	var $form = $('#rev_form');
	if (wise.isValidate($form)) {
		wise.ajax({
			url : rootPath + '/sa/account/reviewer/create.ajax',
			data : $form.serialize(),
			dataType : 'text',
			success : function() {
				printReviewerList({
					search : _search
				});
				wisePopupComponent.x();
			}
		});
	}
}

function reviewerInfoPopup(reviewerId) {
	wisePopupComponent.popup('/sa/account/popup.popup?p=reviewerInfo', {
		addClass : 'popup-view-approval-manager',
		popupTitle : 'View Approval Manager',
		removeFooter : true
	}, {
		reviewerId : reviewerId
	});
}

function editStartForReviewerInfo(_boolean) {
	if (_boolean) {
		$('.popup [data-field-disable=true]').addClass('_uncorrectable');
		$('.popup [data-field-disable=false]').addClass('_correct');
	} else {
		$('.popup [data-field-disable=true]').removeClass('_uncorrectable');
		$('.popup [data-field-disable=false]').removeClass('_correct');
	}
	$('.popup ._uncorrectable button').attr('disabled', _boolean);
	$('.btn-change-password').attr('disabled', _boolean);
	$('#popup_reviewer_status > button').attr('disabled', _boolean);
}

function editReviewerNameAndEmail(reviewerId) {
	if (wise.isValidate($('#info_edit_form'))) {
		var _name = $('.popup #rev_name').val();
		var _email = $('.popup #rev_email').val();
		wise.ajax({
			url : rootPath + '/sa/account/reviewer/edit_name_and_email.ajax',
			data : {
				targetId : reviewerId,
				name : _name,
				email : _email
			},
			dataType : 'text',
			success : function() {
				printReviewerList({
					search : _search
				});
				$('.popup #rev_name_view').text(_name);
				$('.popup #rev_email_view').text(_email);
				editStartForReviewerInfo(false);
			}
		});
	}
}

function editReviewStatus(reviewerId, status) {
	wise.ajax({
		url : rootPath + '/sa/account/reviewer/edit_status.ajax',
		data : {
			targetId : reviewerId,
			status : status
		},
		dataType : 'text',
		success : function() {
			printReviewerList({
				search : _search
			});
			var appendData = '';
			if (status == 1) {
				appendData = '<p>Inactive</p><button type="button" class="btn-status" onclick="editReviewStatus(' + reviewerId + ', 0);">Activate</button>';
			} else if (status == 0) {
				appendData = '<p>Active</p><button type="button" class="btn-status" onclick="editReviewStatus(' + reviewerId + ', 1);">Deactivate</button>';
			}
			$('#popup_reviewer_status').children().remove();
			$('#popup_reviewer_status').append(appendData);
		}
	});
}
