$(function() {
	$("input[name=search]").on("keyup", function(e) {
		_search = $(this).val();
		printAgencyList({
			search : _search,
			status : $("#status").val()
		});
	});

	// $("#status").on("change", function() {
	// printAgencyList({
	// search : _search,
	// status : $(this).val()
	// });
	// });

}); // end ready func

function printAgencyList(data) {
	if (data === undefined) {
		data = {};
	} else if (data == 'search') {
		data = {
			search : _search,
			status : $('#status').val()
		};
	}
	wise.ajax({
		url : rootPath + "/sa/account/popup.popup?p=agencyList",
		data : data,
		dataType : 'HTML',
		success : function(html) {
			$('#print_agency_list').html(html);
			tableDataSort();
			initProgress();
			$('#print_agency_list > table').find('td span.dotdotdot_ele').tooltip(ellipsisTooltip);
		}
	});
}

function tableDataSort() {
	var $subAgArr = $("tr[data-role=SUB_AGENCY]");
	$.each($subAgArr, function() {
		var $this = $(this);
		var _agencyGroupId = $this.attr("data-agency-group-id");
		$("tbody[data-agency-group=" + _agencyGroupId + "]").append($this);
	});
}

function initProgress() {
	$('#print_agency_list .credit-status .progress').each(function() {
		wise.drawProgressChart(this);
	});
}
