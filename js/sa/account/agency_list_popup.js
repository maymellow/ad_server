/*
 * 
 * 팝업창 내 에서 발생하는 모든 기능들.
 * 1. 팝업 열기
 * 2. 팝업 내의 부가 기능.
 * 
 * */

$(function() {

	// 결제 가능 금액 라디오버튼 컨트롤
	$(document).on('change', 'input[name=credit_amount_limit]', function() {
		if ($(this).val() == 'Limitless') {
			$('#creditAmount').val('').prop('disabled', true);
		} else {
			$('#creditAmount').prop('disabled', false).focus();
		}
	});

});

function approveAgnecyShowPopup(accountId, status) {
	if (status == 3) {
		approveAgnecyShowPopup_approve(accountId);
	} else {
		approveAgnecyShowPopup_edit(accountId);
	}
}

function approveAgnecyShowPopup_approve(accountId) {
	wisePopupComponent.popup('/sa/account/popup.popup?p=approveAgency', {
		addClass : 'popup-approve-agency-account',
		popupTitle : 'Approve Agency Account',
		okBtnText : 'Approve',
		cancelBtnText : 'Disapprove',
		beforeOpend : function() {
			wisePopupComponent.x();
		},
		okBtnCallBack : function() {
			var name = $('.popup .popup__container .agency-info__table .name__tr td').text();
			commissionAndCreditAmountPopup(accountId, name);
			return false;
		},
		cancelBtnCallBack : function() {
			disapprovePopup(accountId, 'agency');
		},
		onFormOpenned : function() {
			$('.popup .file__list').find('li a').tooltip(ellipsisTooltip);
		}
	}, {
		accountId : accountId
	});
}

function approveAgnecyShowPopup_edit(accountId) {
	wisePopupComponent.popup('/sa/account/popup.popup?p=editAgency', {
		addClass : 'popup-view-agency',
		popupTitle : 'View Agency',
		removeFooter : true,
		beforeOpend : function() {
			wisePopupComponent.x();
		},
		onFormOpenned : function() {
			if ($('#creditAmount').val() != 0) {
				$('input[name=credit_amount_limit][value=Limit]').prop('checked', true).trigger('change');
			} else {
				$('input[name=credit_amount_limit][value=Limitless]').prop('checked', true).trigger('change');
			}
			printApproveHistoryList(accountId);
			$('.wise-number').number(true);

			if ($('#status_view').attr('data-status') == 1) {
				대행사상태에따른버튼컨트롤('disapprove');
			}

			// 시은님 테스트용 start
			$('.popup-view-agency, .popup-view-advertiser').each(function() {
				historyResize($(this));
			});
			// 시은님 테스트용 end
			$(function() {
				$('.popup #status_view').find('.approved-status span').tooltip(ellipsisTooltip);
				$('.popup .file__list').find('li a').tooltip(ellipsisTooltip);
				// $('.popup #service_amount').tooltip(ellipsisTooltip);
			});
			
			// 달력
			var jqDatepicker = {
				weekHeader : 'Wk',
				dateFormat : 'yy.mm.dd',
				autoSize : true,
				showMonthAfterYear : true,
				onSelect : function() {
					var time = moment($('#contract_period_end').val(), 'YYYY.MM.DD') - moment($('#contract_period_start').val(), 'YYYY.MM.DD');
					if (time < 0) {
						$('#contract_period_end').val($('#contract_period_start').val());
					}
				}
			}
			$('#contract_period_start, #contract_period_end').datepicker(jqDatepicker);
		}
	}, {
		accountId : accountId
	});
}

function commissionAndCreditAmountPopup(accountId, name) {
	wisePopupComponent.popup('/sa/account/popup.popup?p=commission', {
		addClass : 'popup-set-commission',
		popupTitle : 'Set Commission & Credit Amount',
		okBtnText : 'Back',
		okBtnStyle : 'fail',
		cancelBtnText : 'OK',
		cancelBtnStyle : 'success',
		beforeOpend : function() {
			wisePopupComponent.x();
		},
		onFormOpenned : function() {
			$('.wise-number').number(true);
		},
		okBtnCallBack : function() {
			approveAgnecyShowPopup_approve(accountId);
		},
		cancelBtnCallBack : function() {
			approveAgency(accountId, name);
			return false;
		}
	});
}

function approveAgency(accountId, name) {
	if (wise.isValidate($('#cac_form'))) {
		wise.ajax({
			url : rootPath + '/sa/account/agency/approve.ajax',
			data : {
				accountId : accountId,
				commission : $('#commission').val(),
				creditAmount : $('#creditAmount').val(),
				service : $('#service').val()
			},
			dataType : 'text',
			success : function() {
				printAgencyList({
					search : _search,
					status : $('#status').val()
				});
				getAgencyListForLeftMenu();
				alert(name + ' has been approved.');
				wisePopupComponent.x();
			}
		});
	}
}

function disapprovePopup(accountId, _type) {
	wisePopupComponent.popup('/sa/account/popup.popup?p=disapproveAgency', {
		addClass : 'popup-disapprove-agency-account',
		popupTitle : _type == 'agency' ? 'Disapprove Agency Account' : 'Disapprove Sub Agency Account',
		okBtnText : 'Back',
		okBtnStyle : 'fail',
		cancelBtnText : 'OK',
		cancelBtnStyle : 'success',
		beforeOpend : function() {
			wisePopupComponent.x();
		},
		okBtnCallBack : function() {
			if (_type == 'agency') {
				approveAgnecyShowPopup_approve(accountId);
			} else if (_type == 'sub agency') {
				subAgencyInfoPopupForAdmin(accountId);
			}
		},
		cancelBtnCallBack : function() {
			disapproveAgency({
				accountId : accountId,
				comment : $('#comment').val()
			});
			return false;
		}
	});
}

function disapproveAgency(data) {
	if (wise.isValidate($('#disapprove_form'))) {
		wise.ajax({
			url : rootPath + '/sa/account/agency/disapprove.ajax',
			data : data,
			dataType : 'text',
			success : function() {
				if (window.location.pathname.split('/')[1] == 'ag') {
					printSubAgencyList({
						search : _search,
						status : $('#status').val()
					});
				} else {
					printAgencyList({
						search : _search,
						status : $('#status').val()
					});
				}
				wisePopupComponent.x();
			}
		});
	}
}

function printApproveHistoryList(accountId) {
	wise.ajax({
		url : rootPath + '/sa/account/get_account_history_list.ajax',
		data : {
			accountId : accountId,
			position : 'ag'
		},
		dataType : 'HTML',
		success : function(html) {
			// $('#approve_history').perfectScrollbar();
			$('#approve_history').html(html).animate({
				scrollTop : $('#approve_history > ul').height()
			}, 'slow');
		}
	});
}

function insertComment(accountId) {
	var comment = $('#comment_text').val();
	if (comment != '') {
		wise.ajax({
			url : rootPath + '/sa/account/agency/insertcomment.ajax',
			data : {
				accountId : accountId,
				comment : comment,
				position : 0
			},
			dataType : 'text',
			success : function() {
				$('#comment_text').val('');
				printApproveHistoryList(accountId);
			}
		});
	} else {
		$('#comment_text').addClass('parsley-error');
		$('#comment_text').tooltip({
			animation : false,
			container : 'body',
			placement : 'top-left',
			title : 'This value is required.'
		});
		$('#comment_text').focus();
		return;
	}
}

function viewToggle(isDisabled) {
	$('.popup [data-show]').each(function() {
		var $this = $(this);
		if ($this.attr('data-show') == 'true') {
			$this.attr('data-show', 'false');
		} else {
			$this.attr('data-show', 'true');
		}
	});
	$('input[name=credit_amount_limit]').attr('disabled', isDisabled);
}

var commisionAndCreaditData = {};
function modifyStartForCommissionAndCreditAmount(_boolean, $this) {
	viewToggleStart(_boolean, $this);
	$('input[name=credit_amount_limit]').attr('disabled', !_boolean);
	if (_boolean) {
		commisionAndCreaditData = {
			commission : $('#commission').val(),
			limit : $('#creditAmount').val(),
			limitType : $('#r5').is(':checked')
		};
	} else {
		$('#commission').val(commisionAndCreaditData.commission);
		if (commisionAndCreaditData.limitType) {
			$('#r5').prop('checked', true);
			$('#creditAmount').prop('disabled', false).val(commisionAndCreaditData.limit);
		} else {
			$('#r6').prop('checked', true);
			$('#creditAmount').prop('disabled', true).val('');
		}
	}

	// if (_boolean) {
	// $('.popup [data-field-disable=true]').addClass('_uncorrectable');
	// $('.popup [data-field-disable=false]').addClass('_correct');
	// } else {
	// $('.popup [data-field-disable=true]').removeClass('_uncorrectable');
	// $('.popup [data-field-disable=false]').removeClass('_correct');
	// }
	// $('.popup ._uncorrectable button').attr('disabled', _boolean);
	// $('input[name=credit_amount_limit]').attr('disabled', !_boolean);
}

function editCommissionAndCreditAmount(accountId, $this) {
	if (wise.isValidate($('#cAc_form'))) {
		var $popup = $this.closest('.popup');
		var _commission = $popup.find('#commission').val();
		var _creditAmount = $popup.find('#creditAmount').val();
		wise.ajax({
			url : rootPath + '/sa/account/agency/edit_commission_and_creditamount.ajax',
			data : {
				accountId : accountId,
				commission : _commission,
				creditAmount : _creditAmount
			},
			dataType : 'JSON',
			success : function(data) {
				if (data.success) {
					printApproveHistoryList(accountId);
					printAgencyList({
						search : _search,
						status : $('#status').val()
					});
					commisionAndCreaditData = {
						commission : $('#commission').val(),
						limit : $('#creditAmount').val(),
						limitType : $('#r5').is(':checked')
					};
					// 팝업뷰 컨트롤
					$popup.find('#commission_view').text(wise.number_format(_commission));
					$popup.find('#creditAmount_view').text(wise.number_format(_creditAmount));
					$popup.find('#creditAmount').attr('placeholder', wise.number_format(_creditAmount));
					modifyStartForCommissionAndCreditAmount(false, $this);

					var $view = $popup.find('#cAc_form .credit-amount__tr .modify-show');
					$view.find('.limit, .limitless').removeClass('wise_display_none');
					if (_creditAmount == 0) {
						$view.find('.limit').addClass('wise_display_none');
					} else {
						$view.find('.limitless').addClass('wise_display_none');
					}
				} else {
					alert(data.message);
				}
			}
		});
	}
}

function viewDetailServicePopup(accountId) {
	wisePopupComponent.popup('/ag/account/popup.popup?p=detailService', {
		addClass : 'popup-service-remaining',
		popupTitle : 'Service Remaining',
		okBtnText : '',
		cancelBtnText : '',
		onFormOpenned : function() {
			$('#service_history').animate({
				scrollTop : $('#service_history ul').height()
			}, 'slow');
		}
	}, {
		accountId : accountId,
		service : $('.popup #service_amount').text()
	});
}

function addServicePopup(accountId) {
	wisePopupComponent.popup('/sa/account/popup.popup?p=addService', {
		addClass : 'popup-add-service',
		popupTitle : 'Add Service',
		onFormOpenned : function() {
			$('.wise-number').number(true);
		},
		okBtnCallBack : function() {
			addService(accountId);
			return false;
		}
	}, {
		service : $('.popup #service_amount').text()
	});
}

function addService(accountId) {
	var _service = $('#service').val();
	if (confirm('Are you sure you want to add service of (%s) amount to this agency?'.format(_service))) {
		if (wise.isValidate($('#add_service_form'))) {
			wise.ajax({
				url : rootPath + '/sa/account/agency/addservice.ajax',
				data : {
					accountId : accountId,
					service : _service
				},
				dataType : 'JSON',
				success : function(data) {
					$('.popup #service_amount').text(wise.number_format(data.service));
					printApproveHistoryList(accountId);
					wisePopupComponent.x();
				}
			});
		}
	}
}

function modifyElePasswordShow() {
	$('#ag_form').attr('data-parsley-excluded', '');
	$('._wise_popup_add_element').show();
	$('._wise_popup_modify_password_element').hide();
}

function viewToggleStart(_boolean, $that) {
	var _targetName = $that.closest('[data-field-disable]').attr('data-field-disable');
	if (_boolean) {
		$('[data-field-disable]').each(function() {
			var $this = $(this);
			var _fieldName = $this.attr('data-field-disable');
			switch (_fieldName) {
			case 'true':
				$this.addClass('_uncorrectable');
				$this.find('div button').attr('disabled', true);
				break;
			default:
				if (_fieldName == _targetName) {
					$this.addClass('_correct');
					$this.find('div button').attr('disabled', false);
				} else {
					$this.addClass('_uncorrectable');
					$this.find('div button').attr('disabled', true);
				}
				break;
			}
		});
	} else {
		$('[data-field-disable]').each(function() {
			var $this = $(this);
			var _fieldName = $this.attr('data-field-disable');
			switch (_fieldName) {
			case 'true':
				$this.removeClass('_uncorrectable');
				$this.find('div button').attr('disabled', false);
				break;
			default:
				if (_fieldName == _targetName) {
					$this.removeClass('_correct');
					$this.find('div button').attr('disabled', true);
				} else {
					$this.removeClass('_uncorrectable');
					$this.find('div button').attr('disabled', false);
				}
				break;
			}
		});
	}
}

function editAgencyInfoStart($this) {
	viewToggleStart(true, $this);
	$('#contract_period_start').val($('#contract_period_start_view').text());
	$('#contract_period_end').val($('#contract_period_end_view').text());
}

function SubmitAgencyInfo(accountId, $this) {
	var $form = $('#agency_info_form');
	if (wise.isValidate($form)) {
		wise.ajax({
			url : rootPath + '/sa/account/agency/edit_agency_contract_period.ajax',
			data : $form.serialize(),
			dataType : 'text',
			success : function() {
				viewToggleStart(false, $this);
				$('#contract_period_start_view').text($('#contract_period_start').val());
				$('#contract_period_end_view').text($('#contract_period_end').val());
				printApproveHistoryList(accountId);
			}
		});
	}

}
