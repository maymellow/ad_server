$(function() {
	$("input[name=search]").on("keyup", function(e) {
		_search = $(this).val();
		printReviewerList({
			search : _search
		});
	});

	$("#status").on("change", function() {
		printReviewerList({
			search : _search
		});
	});

}); // end ready func

function printReviewerList(data) {
	if (data === undefined) {
		data = {};
	}
	wise.ajax({
		url : rootPath + "/sa/account/popup.popup?p=reviewerList",
		data : data,
		dataType : 'HTML',
		success : function(html) {
			if ($('#reviewer_table_list').hasClass('dataTable')) {
				$('#reviewer_table_list').DataTable().destroy();
			}
			$('#print_reviewer_list').html(html);
			var table = $('#reviewer_table_list').dataTable({
				'info' : false,
				'paging' : false,
				'bLengthChange' : false,
				"order" : [ [ 1, "desc" ] ],
				'aoColumnDefs' : [ {
					'bSearchable' : false,
					'aTargets' : [ 1, 2, 3, 4 ]
				} ],
				'retrieve' : true,
				'autoWidth' : false
			});
			$('#reviewer_table_list_filter').hide();
			var ex = $('#reviewer_table_list').DataTable();
			$('#reviewer_search_input').on('keyup', function() {
				ex.search(this.value).draw();
			});

			$('#reviewer_table_list').find('td span.dotdotdot_ele').tooltip(ellipsisTooltip);
		}
	});
}
