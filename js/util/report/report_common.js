var changeSearchDate = function(ele) {
	var _val = $(ele).val();
	if(_val >= 0){
		if(_val == 1 || _val == 0){
			$('#search_end_date').val(moment(dt).add(-_val, 'day').format('YYYY.MM.DD'));
		}else{
			$('#search_end_date').val(moment(dt).format('YYYY.MM.DD'));
		}
		$('#search_start_date').val(moment(dt).add(-_val, 'day').format('YYYY.MM.DD'));
	}else{
		if(_val == -1){
			var setDate = new Date(dt.getFullYear(), dt.getMonth() - 1, 1);
			var nowDate2 = new Date(dt.getFullYear(), dt.getMonth() , 0);
			$('#search_end_date').val(moment(nowDate2).format('YYYY.MM.DD'));
			$('#search_start_date').val(moment(setDate).format('YYYY.MM.DD'));
		}else if(_val == -2){
			var setDate = new Date(dt.getFullYear(), dt.getMonth(), 1);
			$('#search_end_date').val(moment(dt).format('YYYY.MM.DD'));
			$('#search_start_date').val(moment(setDate).format('YYYY.MM.DD'));
		}
	}
	viewTypeCheck();
}

var selectDateTime = function(){
	var time = moment($('#search_end_date').val(), 'YYYY.MM.DD') - moment($('#search_start_date').val(), 'YYYY.MM.DD');
	if(time < 0){
		$('#search_end_date').val($('#search_start_date').val());
	}
	if($('#search_data_select').val() != '-3'){
		$('#search_data_select').select2('destroy');
		$('#search_data_select').val('-3').select2({
			minimumResultsForSearch: Infinity,
			theme : 'band'
		});
	}
}

var viewTypeCheck = function() {
	console.log('vsefsdfs');
	var time = moment($('#search_end_date').val(), 'YYYY.MM.DD') - moment($('#search_start_date').val(), 'YYYY.MM.DD');
	time = time / 1000 / 60 / 60 / 24;
	$('input[name="viewType"]').removeAttr('disabled');
	$('input[name="viewType"]').removeAttr('checked');
	if (time == 0 || time < 15) {
		$('#view_type_day').prop('checked', true);
		$('#view_type_month').prop('disabled', true);
		if (time != 14) {
			$('#view_type_week').prop('disabled', true);
		}
	} else {
		$('#view_type_day').prop('disabled', true);
		$('#view_type_week').prop('checked', true);
		if (time < 57) {
			$('#view_type_month').prop('disabled', true);
		} else if (time > 98) {
			$('#view_type_month').prop('checked', true);
			$('#view_type_week').prop('disabled', true);
		}
	}
}