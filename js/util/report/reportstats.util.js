function sumReportStatsData(source, targets) {
	for ( var key in targets) {
		try {
			if(key == 'dateTime' || key == 'id' || key == 'name'){
				continue;
			}
			if ($.isNumeric(targets[key])) {
				var value = source[key];
				if (!value) {
					value = 0;
				}
				source[key] = value + targets[key];
			}
		} catch (exception) { // DO NOTHING
		}
	}
}
function makeForChartData(data, stats_map, column_name_map){
	var completeSource = new Array();
	var source = new Array();
	for ( var i in data) {
		for ( var k in stats_map) {
			if (!source[stats_map[k]] || source[stats_map[k]] == null) {
				source[stats_map[k]] = new Array();
			}
			var value = data[i][stats_map[k]];
			if(stats_map[k] == 'cpc' || stats_map[k] == 'ctr'|| stats_map[k] == 'vtr'|| stats_map[k] == 'cpv' || stats_map[k] == 'cpm'){
				value = calcActionValue(stats_map[k], data[i]);
			}
			source[stats_map[k]].push(value);				
		}
	}
	for ( var keyIndex in stats_map) {
		completeSource.push({
			data : source[stats_map[keyIndex]],
			name : column_name_map[stats_map[keyIndex]],
			realName : stats_map[keyIndex]
		});
	}
	return completeSource;
}

function isInArray(array, search) {
	return array.indexOf(search) >= 0;
}

function calcActionValue(key, data) {
	if ($.isEmptyObject(data)) {
		return 0;
	}
	switch (key) {
	case 'cpm':
		if (data['spend'] != 0 && data['impressions'] != 0 && data['spend'] !== undefined && data['impressions'] !== undefined) {
			return parseFloat((1000 * (data['spend'] / data['impressions'])).toFixed(1)); // 1000 * (spend / (double) impressions);
		} else {
			return 0;
		}
		break;
	case 'vtr':
	case 'ctr':
		if (data['clicks'] != 0 && data['impressions'] != 0 && data['clicks'] !== undefined && data['impressions'] !== undefined) {
			return parseFloat(((data['clicks'] / data['impressions']) * 100.0).toFixed(2)); // (click / imp) * 100
		} else {
			return 0;
		}
		break;
	case 'cpv':
	case 'cpc':
		if (data['spend'] != 0 && data['clicks'] != 0 && data['clicks'] !== undefined && data['spend'] !== undefined) {
			return parseFloat((data['spend'] / data['clicks']).toFixed(1));
		} else {
			return 0;
		}
		break;
	case 'impressions' : 
		return data['impressions'];
		break;
	case 'clicks' : 
		return data['clicks'];
		break;
	case 'spend' : 
		return data['spend'];
		break;
	}
}
