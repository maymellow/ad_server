var STACK_TEMP = { // 초기화 겸 셋팅
	type : 'column',
	name : '',
	data : [],
	min : 0
};
var COLUMN_TEMP = {
	type : 'column',
	name : '',
	pointWidth: 6,
	zIndex : 2,
	data : [],
	min : 0
};
var LINE_TEMP = {
	type : 'line',
	name : '',
	zIndex : 3,
	data : [],
	min : 0
};
var AREA_TEMP = {
	type : 'area',
	name : '',
	zIndex : 1,
	data : [],
	min : 0,
	fillOpacity : 0.2
};
var OPPS_TEMP = { // 오른쪽 초기값들
	opposite : true,
	title : {
		text : ''
	},
	labels : {
		style : {
			color : '',
			display : 'none'
		}
	},
	min : 0
};

function defaultChartOption() {
	Highcharts.setOptions({
		lang : {
	        thousandsSep: ','
		}
	});
	return {
		summary : {
			ctr : 0,
			cpc : 0,
			cost_per : 0
		},
		title : { // 제목
			text : null
		},
		subtitle : { // 부 제목
			text : null
		},
		legend : { // 밑에 상태바
			layout : 'vertical',
			align : 'right',
			borderColor : '#ffffff',
			verticalAlign : 'top',
			x : 0,
			itemStyle : {
				paddingBottom : '10px',
				fontSize : '11px',
				fontWeight : 'bold'
			},
			labelFormatter : function() {
			}
		},
		xAxis : {
			type : 'datetime',
			minTickInterval : 24 * 3600 * 1000
		},
		yAxis : [ {
			title : {
				text : ''
			}
		} ],
		tooltip : { // 마우스가 올라갔을때
		},
		navigation : { // 오른쪽 상담 네비게이션 삭제
			buttonOptions : {
				enabled : false
			}
		},
		credits : { // 오른쪽 하단 로고 삭제
			enabled : false
		},
		exporting : { // 오른쪽 하단 로고 삭제
			enabled : false
		},
		plotOptions : { // 차트에 선 점 값};
			column : {
				stacking : 'normal'
			}
		},
		series : [],
		totalSumMap : {}
	};
}