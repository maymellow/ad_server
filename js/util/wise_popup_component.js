//// 다른위치에서 절대로 변경하시면 안됩니다.

var popupStack2 = []; // TODO 이거 이제 제거해도 될듯. 추후작업

var wisePopupComponent = (function() {
	var noop = function() {
		return true;
	};

	var defaultOptions = {
		popupTitle : 'Popup title',
		beforeOpend : noop,
		onFormOpenned : noop,
		onTaskComplete : noop,
		okBtnText : 'OK',
		okBtnCallBack : noop,
		okBtnStyle : 'success',
		cancelBtnText : 'Cancel',
		cancelBtnCallBack : noop,
		cancelBtnStyle : 'fail',
		optionBtnText : '',
		optionBtnCallBack : noop,
		optionBtnStyle : 'fail',
		addClass : '',
		showXBtn : true,
		createDim : true,
		popupComponentName : 'wisePopupComponent',
		removeFooter : false
	}

	var guid = function() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	}

	return {

		// 팝업 옵션 초기화
		init : function() {
			var that = this;

			// 팝업 사이즈 조정을 위한 class 추가
			if (that.options.addClass != '') {
				that.$popup.addClass(that.options.addClass);
				var $popupContainer = that.$popup.find('.popup__container');
				switch (that.options.addClass) {
				case 'popup-view-agency':
				case 'popup-view-sub-agency':
				case 'popup-view-advertiser':
					$popupContainer.attr('data-layout', 'double').addClass('correct__wrap');
					break;
				case 'popup-view-approval-manager':
					$popupContainer.addClass('correct__wrap');
					break;
				case 'popup-edit-creative':
					$popupContainer.attr('data-layout', 'double')
					break;
				case 'popup-view-creative-admin':
					$popupContainer.attr('data-layout', 'double')
					break;
				}
			}

			// 팝업 가운데 정렬
			that.$popup.centerLayer();

			// 타이틀
			var options = that.options;
			if (options.popupTitle.length > 0) {
				// that.$popup.find('[data-field-id=_wise_popup_title]').text(options.popupTitle).show();
				that.$popup.find('._wise_popup_title').text(options.popupTitle).show();
			} else {
				that.$popup.find('.popup__header').hide();
			}

			// 버튼
			if (that.options.removeFooter) {
				that.$popup.find('.popup__footer').remove();
			} else {
				that.$popup.find('.btn_positive').text(that.options.okBtnText).attr('data-type', options.okBtnStyle);
				if (that.options.okBtnText == '') {
					that.$popup.find('.btn_positive').hide();
				}

				that.$popup.find('.btn_negative').text(that.options.cancelBtnText).attr('data-type', options.cancelBtnStyle);
				if (that.options.cancelBtnText == '') {
					that.$popup.find('.btn_negative').hide();
				}

				if (that.options.optionBtnText != '') {
					that.$popup.find('.btn_option').text(that.options.optionBtnText).attr('data-type', options.optionBtnStyle).show();
				}
			}

			if (!that.options.showXBtn) {
				that.$popup('.btn-popup-close').hide();
			}

			// if (that.options.createDim) {
			// that.dim();
			// }
		},

		// 팝업 끄기
		x : function() {
			var that = this;

			if (that.$popup === undefined) {
				return false;
			}

			$(window).unbind('scroll');
			that.$popup.find('.popup__container').unbind('scroll');
			$('body').css({
				'overflow-y' : 'auto',
				'overflow-x' : 'auto'
			});

			var _popupId = popupStack2.pop();
			$('#' + _popupId).remove();

			if (this.options.createDim) {
				$('#_wise_popup_dim_' + _popupId).remove();
			}
		},

		// 팝업 끄기(옵션 실행후)
		close : function() {
			var that = this;
			if (that.options.cancelBtnCallBack) {
				if (!that.options.cancelBtnCallBack()) {
					return;
				}
			}
			that.x();
		},

		// 완료버튼
		taskComplete : function() {
			var that = this;
			if (that.options.okBtnCallBack) {
				if (that.options.okBtnCallBack()) {
					if (that.options.onTaskComplete) {
						that.options.onTaskComplete();
						that.x();
					}
				}
			} else if (that.options.onTaskComplete) {
				that.options.onTaskComplete();
				that.x();
			}
		},

		// 팝업 열기
		popup : function(bodyUrl, options, args) {
			var that = this;
			that.options = $.extend({}, defaultOptions, options);

			wise.ajax({
				url : rootPath + bodyUrl,
				data : $.extend({}, args),
				dataType : 'HTML',
				success : function(html) {
					that.options.beforeOpend();

					that.popupId = guid();
					popupStack2.push(that.popupId);

					if (that.options.createDim) {
						that.dim();
					}

					var componentHtml = '<div class="popup" id="' + that.popupId + '">';
					componentHtml += '		<div class="popup__header">';
					componentHtml += '			<p class="title _wise_popup_title">Title</p>';
					componentHtml += '		</div>';
					componentHtml += '		<div class="popup__container"></div>';
					componentHtml += '		<div class="popup__footer">';
					componentHtml += '			<button type="button" class="btn_positive" onclick="' + that.options.popupComponentName + '.taskComplete();">Ok</button>';
					componentHtml += '			<button type="button" class="btn_option" onclick="' + that.options.popupComponentName + '.options.optionBtnCallBack();" style="display: none;">Option</button>';
					componentHtml += '			<button type="button" class="btn_negative" onclick="' + that.options.popupComponentName + '.close();">Close</button>';
					componentHtml += '		</div>';
					componentHtml += '		<button class="btn-popup-close" onclick="' + that.options.popupComponentName + '.x();">Popup Close</button>';
					componentHtml += '	</div>';

					$('body').append(componentHtml);/*.css({
						'overflow-y' : 'hidden',
						'overflow-x' : 'hidden'
					});*/
					that.$popup = $('#' + that.popupId);
					that.$popup.find('.popup__container').html(html);

					that.init();
				},
				complete : function() {
					if (that.options.onFormOpenned) {
						that.options.onFormOpenned();
					}
				}
			});
		},

		// 팝업 딤넣기
		dim : function() {
			$('body').append('<div class="popup-dim" id="_wise_popup_dim_' + this.popupId + '" onclick="' + this.options.popupComponentName + '.x();" style="cursor:pointer; z-index:100;"></div>');
		}

	}; // end return

}());
