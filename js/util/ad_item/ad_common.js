var previewFunction = function() {
	WebfontConfig = {
		google : {
			families : [ 'Roboto Condensed' ]
		},
		custom : {
			families : [ 'Nanum Barun Gothic' ],
			urls : [ 'css/nanumbarungothic.css' ]
		},
		fontactive : function() {
			$(".feed-text").dotdotdot({
				wrap : 'letter',
				height : 30
			});
			$(".feed-title").dotdotdot({
				height : 35,
				wrap : 'letter'
			});
		}
	}
	WebFont.load(WebfontConfig);

	$('.screen__content').imagesLoaded(function() {
		if (!$('.screen__content').find('li:last-child').hasClass('feed-dummy')) {
			var myScroll = new IScroll('.screen__content', { 
				mouseWheel : true 
				});
			myScroll.on('scrollEnd', function () {
			    if ( this.y<-100 ) {
			    	myScroll.scroller.clientHeight=600
			    	console.log(myScroll.scroller.clientHeight)
			    /*	console.log(myScroll.y)
			    	console.dir(myScroll)*/
/*			    	console.log(myScroll.options.invertWheelDirection)*/
		
			    }
			});
			
		}
		else{
			if($('.screen__content').find('.feed')[0].offsetHeight>=375){
				var myScroll = new IScroll('.screen__content', {
					mouseWheel : true
				});
			}
			
		}
	}).progress(function() {
		$('.screen__content').addClass('_load');
	}).done(function() {
		$('.screen__content').removeClass('_load');
	});
}
