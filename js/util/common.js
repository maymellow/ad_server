(function() {
	$.fn.extend({
		// 사용하려다 말앗네
		wise_readOnly : function(bool) {
			if (bool == undefined || bool == true) {
				return this.prop('readOnly', true), this.css("background-color", "#eee")
			} else {
				return this.prop('readOnly', false), this.css("background-color", "")
			}
		},

		// 팝업 위치 정렬
		centerLayer : function() {
			var $popup = $(this);
			var headerWidth = $('.container .nav').width();
			var popupWidth = $popup.width() + 42; // padding + border = 42
			this.css({
				'position' : 'absolute',
				'left' : '50%',
				'margin-left' : ('-' + ((popupWidth / 2) - (headerWidth / 2)) + 'px'),
				'top' : ((152 + $(parent).scrollTop()) + 'px')
			// 'top' : ((($(parent).height() - $popup.outerHeight()) / 2) + $(parent).scrollTop() + 'px')
			});
			return this;
		}
	});

	// 문잘열 삽입
	// '%s_%s_%s'.format('1', 2, 'a'); -> '1_2_a'
	String.prototype.format = function() {
		var args = [].slice.call(arguments, 0), i = 0;
		return this.replace(/%s/g, function() {
			return args[i++];
		});
	};

})();

// IE에서 console.log에러 방지
var console = window.console || {
	log : function() {
	}
};

// 말줄임표가 있을때만 툴팁 띄우기
/*
 * var DOTDOTDOT = { // watch : 'window', wrap : 'letter', callback : function(a, b) { if (a && b) { $(this).addClass('_ellipsis'); $(this).attr('data-title', b[0].textContent); $(this).tooltip({ placement : 'top-left', trigger : 'hover' }); } else { $(this).removeClass('_ellipsis'); $(this).tooltip('destroy'); } } }
 */
var ellipsisTooltip = {
	placement : 'top-left',
	trigger : 'hover',
	title : function() {
		var that = this;
		var _element = $(that).context;
		if ($(that).hasClass('dotdotdot_ele')) {
			// offsetWidth => scrollWidth로 바꿈
			if ($(that).parents('td').width() <= _element.scrollWidth) {
				$(that).addClass('_ellipsis');
				return _element.textContent;
			} else {
				$(that).removeClass('_ellipsis');
				return "";
			}
		} else {
			if (_element.offsetWidth < _element.scrollWidth) {
				$(that).addClass('_ellipsis');
				return _element.textContent;
			} else {
				$(that).removeClass('_ellipsis');
				return "";
			}
		}
	}
}

function showErrorPage(html, error) {
	console.log(error);
	var appendData = '<div class="popup-dim" id="popup_error_page_dim"></div>';
	appendData += '<div class="popup popup-error" id="popup_error_page" style="position: absolute; left: 50%; top: 50%;">';
	appendData += '		<div class="popup__header">';
	appendData += '			<p class="title">Oops! An Error has occurred.</p>';
	appendData += '		</div>';
	appendData += '		 <div class="popup__container">';
	appendData += '			<p>Perhaps you are here because:</p>';
	appendData += '				<ul>';
	appendData += '					<li>The page you are looking for cannot be found.</li>';
	appendData += '					<li>You do not have access to this page.</li>';
	appendData += '					<li>Sorry, we made a mistake.</li>';
	appendData += '				</ul>';
	appendData += '		 </div>';
	appendData += '		<div class="popup__footer">';
	appendData += '			<button type="button" data-type="fail" onclick="wise.x();">Close</button>';
	appendData += '		 </div>';
	appendData += '		<button class="btn-popup-close" type="button" onclick="wise.x();">close</button>';
	appendData += '</div>';
	$('body').append(appendData);
	$('#popup_error_page').centerLayer();
}

// 팝업 높이 히스토리값 이용 자동 설정
function historyResize(ele) {
	var _this = ele, _information = _this.find('.information'), _record = _this.find('.record');
	_record.find('.history-wrap').height((_information.outerHeight() - _record.outerHeight()) + _record.find('.history-wrap').outerHeight());
	_information.resize(function() {
		if (_information.outerHeight() !== _record.outerHeight()) {
			_record.find('.history-wrap').height((_information.outerHeight() - _record.outerHeight()) + _record.find('.history-wrap').outerHeight());
		}
	});
};

var wise = (function() {
	var ajax = function(option) {
		// wise.ajax전용 디폴트 옵션
		var AJAX_DEFAULT_OPTIONS = {
			type : 'POST',
			dataType : 'JSON',
			beforeSend : function() {
				if (this.createDim) {
					dimComponent.createForAjax();
				}
			},
			complete : function() {
				if (this.createDim) {
					dimComponent.remove();
				}
			},
			error : function(request, status, error) {
				showErrorPage(request.responseText, error);
			},
			createDim : true
		};

		var option = $.extend({}, AJAX_DEFAULT_OPTIONS, option);

		// complete 함수가 정의 되어있다면 디폴트 함수와 병합
		if (typeof option.complete === 'function') {
			var _originComplete = option.complete;
			option.complete = function() {
				_originComplete();
				AJAX_DEFAULT_OPTIONS.complete.call(option);
			}
		}

		// beforeSend 함수가 정의 되어있다면 디폴트 함수와 병합
		if (typeof option.beforeSend === 'function') {
			var _originBeforeSend = option.beforeSend;
			option.beforeSend = function() {
				AJAX_DEFAULT_OPTIONS.beforeSend.call(option);
				_originBeforeSend();
			}
		}

		// error는 재정의 하는것으로 사용하면 될듯.

		// ajax 구동
		$.ajax(option);
	}

	var number_format = function(number, decimals, dec_point, thousands_sep) {
		// http://kevin.vanzonneveld.net
		number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
		var n = !isFinite(+number) ? 0 : +number, prec = !isFinite(+decimals) ? 0 : Math.abs(decimals), sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep, dec = (typeof dec_point === 'undefined') ? '.' : dec_point, s = '', toFixedFix = function(n, prec) {
			var k = Math.pow(10, prec);
			return '' + Math.round(n * k) / k;
		};
		// Fix for IE parseFloat(0.55).toFixed(0) = 0;
		s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
		if (s[0].length > 3) {
			s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
		}
		if ((s[1] || '').length < prec) {
			s[1] = s[1] || '';
			s[1] += new Array(prec - s[1].length + 1).join('0');
		}
		return s.join(dec);
	}

	var x = function() {
		$('#popup_error_page').remove();
		$('#popup_error_page_dim').remove();
	}

	var isValidate = function($form) {
		return $form.parsley({
			errorsWrapper : false
		}).validate();
	}

	var escapeHtml = function(string) {
		var entityMap = {
			"&" : "&amp;",
			"<" : "&lt;",
			">" : "&gt;",
			'"' : '&quot;',
			"'" : '&#39;',
			"/" : '&#x2F;'
		};
		return String(string).replace(/[&<>"'\/]/g, function(s) {
			return entityMap[s];
		});
	}

	var unescapeHtml = function(string) {
		var e = document.createElement('div');
		e.innerHTML = string;
		return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
	}

	var textCount = function($this) {
		var _maxLength = $this.data('parsley-maxlength');
		if (typeof _maxLength === 'undefined') {
			_maxLength = $this.data('max_length');
		}
		var _resultLength = $this.val().length;
		var _cnt = _maxLength - _resultLength;
		var $viewNumber = $this.next('.max-length');
		if (0 > _cnt) {
			$viewNumber.text(_cnt * -1).addClass('_over');
		} else {
			$viewNumber.text(_cnt).removeClass('_over');
		}
		$this.tooltip('destroy');
		$this.removeClass('parsley-error');
	}

	var validationUi = function($input, $view, message, isBorderLine) {
		isBorderLine = typeof isBorderLine === 'undefined' ? true : isBorderLine;
		if ($input.val() == '' || typeof $input.val() == 'undefined') {
			if (isBorderLine) {
				$view.css({
					'outline' : '0 none',
					'border' : '1px solid #ff8888'
				});
			}
			$view.tooltip({
				animation : false,
				container : 'body',
				placement : 'top-left',
				title : message
			});
			return false;
		} else {
			if (isBorderLine) {
				$view.attr('style', '');
			}
			$view.tooltip('destroy');
			return true;
		}
	}

	var dateOption = function() {
		return {
			weekHeader : 'Wk',
			dateFormat : 'yy.mm.dd', // 형식(2012-03-03)
			autoSize : true, // 오토리사이즈(body등 상위태그의 설정에 따른다)
			showMonthAfterYear : true
		// 년 뒤에 월 표시
		};
	}

	var activeTooltipForIcon = function() {
		$('.popup .activeTooltipForIcon').tooltip({
			animation : false,
			container : 'body',
			placement : 'top-left',
			html : "true"
		});
	}

	var drawProgressChart = function(ele) {
		var _this = $(ele);
		var value = parseInt(_this.attr('data-credit_status'));
		var _parent = _this.parent();
		_parentPadding = parseInt(_parent.css('padding-left'));
		_this.width(_parent.width() - ($('.zero').width() + $('.finish').width()) - 6 + "px");

		_this.progressbar({
			value : value,
			create : function(e, ui) {
				if (value > 0) {
					_this.after($('<strong/>', {
						'class' : 'currentNumber',
						'style' : 'left:' + ((_this.width() / 100) * $(e.target).attr('aria-valuenow') + $('.zero').width() - 3 + _parentPadding) + 'px;',
						'text' : $(e.target).attr('aria-valuenow')
					}));
				}
				_this.find('.ui-progressbar-value')
			}
		})
	}

	var guid = function() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	}

	// 목록
	return {
		ajax : ajax,

		// 숫자에 콤마, 소수점 표시
		number_format : number_format,

		// 파슬리 밸리데이션
		isValidate : isValidate,

		// 에러팝업 닫기버튼
		x : x,

		// Escaping HTML strings
		escapeHtml : escapeHtml,
		unescapeHtml : unescapeHtml,

		// 입력하는 text 길이 계산기
		textCount : textCount,

		// 커스텀 밸리데이션 UI
		validationUi : validationUi,

		// 달력 옵션
		dateOption : dateOption,

		// 툴팁 활성화
		activeTooltipForIcon : activeTooltipForIcon,

		// progress 바 그리기
		drawProgressChart : drawProgressChart,

		// 고유 아이디 만들기
		guid : guid
	}
})();

// number_format 사용법
// + original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
// + improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
// + bugfix by: Michael White (http://getsprink.com)
// + bugfix by: Benjamin Lupton
// + bugfix by: Allan Jensen (http://www.winternet.no)
// + revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
// + bugfix by: Howard Yeend
// + revised by: Luke Smith (http://lucassmith.name)
// + bugfix by: Diogo Resende
// + bugfix by: Rival
// + input by: Kheang Hok Chin (http://www.distantia.ca/)
// + improved by: davook
// + improved by: Brett Zamir (http://brett-zamir.me)
// + input by: Jay Klehr
// + improved by: Brett Zamir (http://brett-zamir.me)
// + input by: Amir Habibi (http://www.residence-mixte.com/)
// + bugfix by: Brett Zamir (http://brett-zamir.me)
// + improved by: Theriault
// + input by: Amirouche
// + improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
// * example 1: number_format(1234.56);
// * returns 1: '1,235'
// * example 2: number_format(1234.56, 2, ',', ' ');
// * returns 2: '1 234,56'
// * example 3: number_format(1234.5678, 2, '.', '');
// * returns 3: '1234.57'
// * example 4: number_format(67, 2, ',', '.');
// * returns 4: '67,00'
// * example 5: number_format(1000);
// * returns 5: '1,000'
// * example 6: number_format(67.311, 2);
// * returns 6: '67.31'
// * example 7: number_format(1000.55, 1);
// * returns 7: '1,000.6'
// * example 8: number_format(67000, 5, ',', '.');
// * returns 8: '67.000,00000'
// * example 9: number_format(0.9, 0);
// * returns 9: '1'
// * example 10: number_format('1.20', 2);
// * returns 10: '1.20'
// * example 11: number_format('1.20', 4);
// * returns 11: '1.2000'
// * example 12: number_format('1.2000', 3);
// * returns 12: '1.200'
// * example 13: number_format('1 000,50', 2, '.', ' ');
// * returns 13: '100 050.00'
// Strip all characters but numerical ones.
