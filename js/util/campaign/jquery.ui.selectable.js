/*!
 * jQuery UI Selectable 1.11.1
 * http://jqueryui.com
 *
 * Copyright 2014 jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/selectable/
 */
var flag = 0;
var mode = false;
var doSelect;
var selectable = $.widget("ui.selectable", $.ui.mouse, {
	version: "1.11.1",
	options: {
		appendTo: "body",
		autoRefresh: true,
		distance: 0,
		filter: "span",
		tolerance: "touch",
		multiSelect: true,

		// callbacks
		selected: null,
		selecting: null,
		start: null,
		stop: null,
		unselected: null,
		unselecting: null
	},
	_create: function() {
		var selectees,
			that = this;

		this.element.addClass("ui-selectable");

		this.dragged = false;

		// cache selectee children based on filter
		this.refresh = function() {
			selectees = $(that.options.filter, that.element[0]);
			selectees.addClass("ui-selectee");
			selectees.each(function() {
				var $this = $(this),
					pos = $this.offset();
				$.data(this, "selectable-item", {
					element: this,
					$element: $this,
					left: pos.left,
					top: pos.top,
					right: pos.left + $this.outerWidth(),
					bottom: pos.top + $this.outerHeight(),
					startselected: true,
					selected: $this.hasClass("ui-selected"),
					selecting: $this.hasClass("ui-selecting"),
					unselecting: $this.hasClass("ui-unselecting"),
					prevselected: $this.hasClass("ui-unselecting")
				});
			});
		};
		this.refresh();

		this.selectees = selectees.addClass("ui-selectee");

		this._mouseInit();

		this.helper = $("<div class='ui-selectable-helper'></div>");
	},

	_destroy: function() {
		this.selectees
			.removeClass("ui-selectee")
			.removeData("selectable-item");
		this.element
			.removeClass("ui-selectable ui-selectable-disabled");
		this._mouseDestroy();
	},

	_mouseStart: function(event) {

		var that = this,
			options = this.options;

		this.opos = [ event.pageX, event.pageY ];
	
		if (this.options.disabled) {
			return;
		}

		this.selectees = $(options.filter, this.element[0]);

		this._trigger("start", event);

		$(options.appendTo).append(this.helper);
		// position helper (lasso)
		this.helper.css({
			"left": event.pageX,
			"top": event.pageY,
			"width": 0,
			"height": 0
		});

		if (options.autoRefresh) {
			this.refresh();
		}


		this.selectees.filter(".ui-selected").each(function() {
			var selectee = $.data(this, "selectable-item");
			selectee.startselected = true;
			if (!event.metaKey && !options.multiSelect) {
				selectee.$element.removeClass("ui-selected");
				selectee.selected = false;
				selectee.$element.addClass("ui-unselecting");
				selectee.unselecting = true;
				// selectable UNSELECTING callback
				that._trigger("unselecting", event, {
					unselecting: selectee.element
				});
			}
		});

		$(event.target).parents().addBack().each(function() {
			
			var	selectee = $.data(this, "selectable-item");
			if (selectee) {
				doSelect = (!event.metaKey && !options.multiSelect) || !selectee.$element.hasClass("ui-selected");
				if(doSelect){
					mode = "on";
				}else{
					mode = "off";
				}
				selectee.$element
					.removeClass(doSelect ? "ui-unselecting" : "ui-selected")
					.addClass(doSelect ? "ui-selecting" : "ui-unselecting");
				selectee.unselecting = !doSelect;
				selectee.selecting = doSelect;
				selectee.selected = doSelect;
				selectee.prevselected = doSelect;
				// selectable (UN)SELECTING callbackd
				if (doSelect) {
					if(selectee.$element.hasClass("all-select-time")){
						selectee.$element.siblings('.times').find('span').addClass("ui-selected");
					}
					if(selectee.$element.hasClass("every-hour")){
						$("."+selectee.$element.attr('id')).addClass("ui-selected");
					}
					that._trigger("selecting", event, {
						selecting: selectee.element
					});
				} else {
					if(selectee.$element.hasClass("all-select-time")){
						selectee.$element.siblings('.times').find('span').removeClass("ui-selected");
					}
					if(selectee.$element.hasClass("every-hour")){
						$("."+selectee.$element.attr('id')).removeClass("ui-selected");
					}
					that._trigger("unselecting", event, {
						unselecting: selectee.element
					});
				}
				return false;
			}
		});

	},

	_mouseDrag: function(event) {

		this.dragged = true;

		if (this.options.disabled) {
			return;
		}

		var tmp,
			that = this,
			options = this.options,
			x1 = this.opos[0],
			y1 = this.opos[1],
			x2 = event.pageX,
			y2 = event.pageY;

		if (x1 > x2) { tmp = x2; x2 = x1; x1 = tmp; }
		if (y1 > y2) { tmp = y2; y2 = y1; y1 = tmp; }
		this.helper.css({ left: x1, top: y1, width: x2 - x1, height: y2 - y1 });
		this.selectees.each(function() {
			var selectee = $.data(this, "selectable-item"),
				hit = false;

			//prevent helper from being selected if appendTo: selectable
			if (!selectee || selectee.element === that.element[0]) {
				return;
			}

			if (options.tolerance === "touch") {
				hit = ( !(selectee.left > x2 || selectee.right < x1 || selectee.top > y2 || selectee.bottom < y1) );
			} else if (options.tolerance === "fit") {
				hit = (selectee.left > x1 && selectee.right < x2 && selectee.top > y1 && selectee.bottom < y2);
			}

			if (hit) {
				// SELECT
				/*
				if (!selectee.selecting) {
					selectee.$element.addClass("ui-selected");
					selectee.selecting = true;
					// selectable SELECTING callback
					that._trigger("selecting", event, {
						selecting: selectee.element
					});
				}

				if (selectee.selecting) {
					if(selectee.$element.hasClass("all-select-time")){
						selectee.$element.siblings('.times').find('span').addClass("ui-selected");
					}
					if(selectee.$element.hasClass("every-hour")){
						$("."+selectee.$element.attr('id')).addClass("ui-selected");
					}
					if(flag == 0){
						selectee.$element.addClass("ui-selected");
						selectee.selected = false;
						selectee.unselecting = false;
						flag = 1;
					}

					if (selectee.selected) {
						selectee.$element.removeClass("ui-selected");
						selectee.selected = false;
						selectee.unselecting = true;
					}else if (selectee.unselecting) {
						selectee.$element.removeClass("ui-selected");
						selectee.selected = true;
						selectee.unselecting = false;
						
					}

				}
				*/



				if(mode == "on"){
					selectee.$element.addClass("ui-selected");
					if(selectee.$element.hasClass("all-select-time")){
						selectee.$element.siblings('.times').find('span').addClass("ui-selected");
					}
					if(selectee.$element.hasClass("every-hour")){
						$("."+selectee.$element.attr('id')).addClass("ui-selected");
					}
				}else{
					selectee.$element.removeClass("ui-selected");	
					if(selectee.$element.hasClass("all-select-time")){
						selectee.$element.siblings('.times').find('span').removeClass("ui-selected");
					}
					if(selectee.$element.hasClass("every-hour")){
						$("."+selectee.$element.attr('id')).removeClass("ui-selected");
					}
				}


			} else {

				if(selectee.$element.attr('prev')=="on"){
					selectee.$element.addClass("ui-selected");
				}else{
					selectee.$element.removeClass("ui-selected");
				}


			}

		});

		return false;
	},

	_mouseStop: function(event) {
		var that = this;
		this.dragged = false;

		if(!doSelect){
			$(".ui-unselecting", this.element[0]).each(function() {
				var selectee = $.data(this, "selectable-item");
				selectee.$element.removeClass("ui-unselecting");
				selectee.unselecting = false;
				selectee.startselected = false;
				that._trigger("unselected", event, {
					unselected: selectee.element
				});
			});
		}else{
			$(".ui-selecting", this.element[0]).each(function() {
				var selectee = $.data(this, "selectable-item");
				selectee.$element.removeClass("ui-selecting").addClass("ui-selected");
				selectee.selecting = false;
				selectee.selected = true;
				selectee.startselected = true;
				that._trigger("selected", event, {
					selected: selectee.element
				});
			});
		}
		
		this.selectees.each(function() {
			var selectee = $.data(this, "selectable-item");
			if(selectee.$element.hasClass("ui-selected")){
				selectee.$element.attr('prev', 'on');
			}else{
				selectee.$element.attr('prev', 'off');
			}
		});


		this._trigger("stop", event);

		this.helper.remove();

		return false;
	}

});