var dimComponent = new DimComponent();

function DimComponent() {
	var _requestAjaxCount = 0; // ajax 호출 갯수.
	var _requestTime = 0; // 요청된 시간 (빠르게 처리될경우 최대 1초 딜레이를 주기위함)
	var _delay; // 딜레이
	var _innerWidth; // 화면 사이즈 (화면크기 변하는지 감지)
	var _innerHeight; // 화면 사이즈 (화면크기 변하는지 감지)
	var $overlay = $('<div class="popup-dim" style="z-index:110;"></div>'); // 딤 돔객체 생성
	var $loading = $('<div class="loading"><img src="' + rootPath + '/images/loading_ffffff_145x50.png" alt=""></div>'); // 로딩 돔객체 생성

	var appendBody = function($dim) {
		$('body').append($dim);
	}

	// 딤만 생성 (무한 생성해도 딤은 하나만 생성됨 )
	this.create = function() {
		var $tmpOverlay = $overlay.removeClass('_hide')
		appendBody($tmpOverlay);
		$loading.addClass('_hide');
		return $tmpOverlay;
	}

	// 딤 + 로딩 생성
	this.createForAjax = function() {
		// ajax 호출 갯수
		_requestAjaxCount++;
		if (_requestAjaxCount <= 0) {
			_requestAjaxCount = 1;
		} else if (_requestAjaxCount == 1) {
			// 요청시간 저장
			_requestTime = new Date().getTime();
		}

		// 화면 사이즈 변경 됐을때
		if (_innerWidth != innerWidth || _innerHeight != innerHeight) {
			init();
		}

		// 로딩바 추가
		this.create().append($loading.removeClass('_hide'));
	}

	// 딤을 지운다. (숨김처리)
	this.remove = function() {
		_requestAjaxCount--;
		if (_requestAjaxCount <= 0) {
			if (_requestTime == 0) {
				hide();
			} else {
				var _now = new Date().getTime();
				var _interval = _now - _requestTime;
				if (_interval < _delay) {
					// 처리시간이 너무 빠를경우 1초 후 딤 제거
					setTimeout(hide, _delay - _interval);
				} else {
					hide();
				}
			}
		}
	}

	// 딤을 숨긴다.
	var hide = function() {
		$overlay.addClass('_hide');
	}

	// 초기화 함수
	var init = function() {
		_delay = 500;
		_innerWidth = innerWidth;
		_innerHeight = innerHeight;
		$loading = $loading.css({
			'position' : 'absolute',
			'left' : '50%',
			'margin-left' : ('-' + ($loading.width() / 2) + 'px'),
			'top' : (_innerHeight / 2) + 'px'
		});
		// 한번만 넣고 컨트롤 하려했는데 body를 못찾음.
		// var body = document.getElementsByTagName('body')[0];
		// $overlay = document.createElement('div');
		// $overlay.className = 'popup-dim';
		// $overlay.style.zIndex = 110;
		// var img = document.createElement('img');
		// img.src = rootPath + '/images/loading_ffffff_145x50.png';
		// $loading = document.createElement('div');
		// $loading.appendChild(img);
		// $loading.className = 'loading';
		// $loading.style.position = 'absolute';
		// $loading.style.left = '50%';
		// $loading.style.marginLeft = ('-' + ($loading.clientWidth / 2) + 'px');
		// $loading.style.top = (_innerHeight / 2) + 'px';
		// $overlay.appendChild($loading);
		// body.appendChild($overlay);
	}

	// 초기화
	{
		init();
	}
}