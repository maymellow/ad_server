function getAdCreativePreview(adCreativeId) {
	wise.ajax({
		type : 'POST',
		url : rootPath + '/adv/get/creative/preview.ajax',
		dataType : 'HTML',
		data : {
			spec : '[{"id":' + adCreativeId + '}]'
		},
		success : function(data) {
			$('#ad_creative_preview').html(data);
		}
	});
}

function popupGotoCampaign(type, ele) {
	// alert('hihi');
	// debugger;
	var $ele = $(ele);
	var url = '';
	if (SESSION.choosedRole == 'ADVERTISER') {
		url = '/adv/manage/campaign/info?adCampaignId=' + $ele.attr('data-cam_id');
	} else {
		// url = '/sag/change/role?chooseId=' + $ele.val() + '&chooseRole=ADVERTISER&page=' + type + '&subAgencyId='+$ele.attr('data-sub_agency_id')+'&adCampaignId='+$ele.attr('data-cam_id');
		// url = SESSION.roleUrl + '/adv/change/role?chooseId=' + $ele.val() + '&chooseRole=ADVERTISER&page=' + type + '&subAgencyId='+$ele.attr('data-sub_agency_id')+'&adCampaignId='+$ele.attr('data-cam_id');
		// url = '/adv/change/role?chooseId=' + $ele.val() + '&chooseRole=ADVERTISER&page=' + type + '&subAgencyId=' + $ele.attr('data-sub_agency_id') + '&adCampaignId=' + $ele.attr('data-cam_id');
		// tkdrb
		url = '/adv/manage/campaign/info?adCampaignId=%s&moveSubAgencyId=%s&moveAdvId=%s'.format($ele.attr('data-cam_id'), $ele.attr('data-sub_agency_id'), $ele.val());
	}
	$(location).attr('href', url);
}
