function getCreativePreview(adCreativeId){
//	wise.ajax({
//		type : 'POST',
//		url : rootPath + '/adv/get/creative/preview.ajax',
//		dataType : 'HTML',
//		data : {
//			spec : '[' + creativeSpecList.toString() + ']'
//		},
//		success : function(data) {
//			$('#ad_creative_preview_empty').hide();
//			$('#ad_creative_preview').show();
//			$('#ad_creative_preview').html(data);
//		}
//	});
}
function getCreativeList(){
	wise.ajax({
		url : rootPath + '/adv/manage/creative/list.list',
		type : 'POST',
		data : $('#search_form').serialize(),
		dataType : 'HTML',
		success : function(html) {
			$('#creative_list_div').html(html);
			var noSearchColumn = new Array();
			if (SESSION.choosedRole == 'AGENCY') {
				noSearchColumn = [ 0, 1, 4, 5, 7 ];
			}else if(SESSION.choosedRole == 'SUB_AGENCY'){
				noSearchColumn = [ 0, 1, 3, 4, 5, 6 ];
			}
			var table = $('#ad_creative_data_table').dataTable({
				'info' : false,
				'paging' : false,
				'bLengthChange' : false,
				"order": [[ 1, "desc" ]],
				'aoColumnDefs' : [ {
					'bSearchable' : false,
					'aTargets' : noSearchColumn    //  검색에서 제외할 컬럼 지정
				} ],
				'retrieve' : true,
				'autoWidth':false
			});
			 $('#ad_creative_data_table_filter').hide();
			var ex = $('#ad_creative_data_table').DataTable();
			$('#ad_creative_search_box').on('keyup', function() {
				ex.search(this.value).draw();
			});
			
			$('#ad_creative_data_table').find('.dotdotdot_ele').tooltip(ellipsisTooltip);
		}
	});	
}

function confirmAdCreative(ele) {
	var $ele = $(ele);
	var adCreativeId = $ele.val();
	var popup = wisePopupComponent.popup('/adv/creative/confirm.popup', {
		addClass : 'popup-view-creative',
		popupTitle : 'View Creative',
		removeFooter : true,
		onFormOpenned : function() {
			$('#popup_go_to_campaign').attr('data-cam_id', $ele.attr('data-cam_id'));
			$('.approved-status').find('span').tooltip(ellipsisTooltip);
		}
	}, {
		ad_creative_id : adCreativeId,
		ad_campaign_id : $ele.attr('data-cam_id')
	});
}