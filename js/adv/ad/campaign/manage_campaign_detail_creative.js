function showNewCreativePopup(){
	var adAudienceIds = new Array();
	var popup = wisePopupComponent.popup('/adv/new/creative.popup', {
		addClass : 'popup-add-new-creative',
		popupTitle : 'Add New Creative',
		okBtnText : 'OK',
		onFormOpenned : function() {
			var getText = function($ele){
				if(typeof $ele.data('title') === 'undefined'){
					return wise.escapeHtml($ele.text());
				}else{
					return wise.escapeHtml($ele.data('title'));
				}
			}
			var appendData = '';
		
			$('#campaign_detail_list > tbody').each(function(){
					appendData += '<li data-id="' + $(this).attr('data-id') + '">';
					appendData += '<input type="checkbox" id="ad_audience_id_' + $(this).attr('data-id') + '" /><label for="ad_audience_id_' + $(this).attr('data-id') + '"><span>' + getText($('#ad_audience_name_'+ $(this).attr('data-id'))) + '</span></label>';
					appendData += '</li>';
			});
			$('#popup_create_ad_creative_id').val(-1);
			$('#popup_audience_list').append(appendData);
			$('#popup_audience_list').find('label span').tooltip(ellipsisTooltip);
		},
		okBtnCallBack : function() {
			if(popupNewAdCreativeValidationCheck()){
				$('#popup_creative_spec_list').val('[' + popupMakeAdCreativeSpec() + ']');
				$('#popup_audience_id_list').val('[' + popupMakeAdAudienceId() + ']');
				$('#popup_ad_campaign_id').val($('#ad_campaign_id').val());
				return true;
			}else{
				return false;
			}
		},
		onTaskComplete : function() {
			wise.ajax({
				type : 'POST',
				url : rootPath + '/adv/add/creative.ajax',
				dataType : 'JSON',
				data : $('#popup_new_creative_form').serialize(),
				success : function(data) {
					if(data){
						location.reload();
					}else{
						alert('실패 ㅠㅠ');
					}
				}
			});
		}
	},{
		product_type : $('#ad_campaign_product_type').val(),
		app_id : $('#ad_campaign_app_id').val()
	});
}

function showEditCreativePopup(ele){
	var $ele = $(ele);
	var popup = wisePopupComponent.popup('/adv/create/new/creative.popup', {
		addClass : 'popup-upload-new-creative',
		popupTitle : 'Edit Creative',
		okBtnText : 'OK',
		okBtnCallBack : function() {
			return popupAdCreativeValidationCheck();
		},
		onFormOpenned : function() {
			$('#popup_ad_creative_new_count').val(1);
		},
		onTaskComplete : function() {
			var returnArray = createNewAdCreativeMakeSpec(parseInt($('#ad_campaign_product_type').val()), -1);
			wise.ajax({
				type : 'POST',
				url : rootPath + '/adv/edit/creative.ajax',
				dataType : 'JSON',
				data : {
					spec : JSON.stringify(returnArray),
					ad_creative_id : $ele.attr('data-creative_id'),
					ad_item_id : $ele.val()
				},
				success : function(data) {
					if(data){
						location.reload();
					}else{
						alert('실패 ㅠㅠ');
					}
				}
			});
		}
	}, {
		ad_creative_id : $ele.attr('data-creative_id'),
		product_type : parseInt($('#ad_campaign_product_type').val()/10)
	});
}