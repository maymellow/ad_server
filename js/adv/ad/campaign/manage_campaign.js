function downloadXlsx(){
	$('#export_data').submit();
}

function exportDataSetting(){
	$('#export_search_start_date').val($('#search_start_date').val());
	$('#export_search_end_date').val($('#search_end_date').val());
	$('#export_view_type').val($('input[name=viewType]:checked').val());
	$('#export_campaign_status').val($('#select_campaign_status').val());
}

function dataSearch(){
	getTableData();
	exportDataSetting();
	WC_CHART_DATA = null;
	AI_CHART_DATA = null;
	DATA_ARRAY = null;
	getGraphData();
}

function openAndCloseCampaignListTable(ele){
	var $ele = $(ele);
	if($ele.hasClass('_fold')){
		$ele.removeClass('_fold');
	}else{
		$ele.addClass('_fold');
	}
}

function selectDateTime(){
	var time = moment($('#search_end_date').val(), 'YYYY.MM.DD') - moment($('#search_start_date').val(), 'YYYY.MM.DD');
	if(time < 0){
		$('#search_end_date').val($('#search_start_date').val());
	}
	if($('#search_data_select').val() != '-3'){
		$('#search_data_select').select2('destroy');
		$('#search_data_select').val('-3').select2({
			minimumResultsForSearch: Infinity,
			theme : 'band'
		});
	}
}

function showCampaignDetailInfo(adCampaignId) {
	location.href = '/adv/manage/campaign/info?adCampaignId=' + adCampaignId;
}

function changeAdCampaignStatus(ele) {
	var $ele = $(ele);
	var status = 0;
	if($ele.is(":checked")){
		$ele.attr('checked', 'checked');
		status = 0;
	}else{
		$ele.removeAttr('checked');
		status = 1;
	}
	wise.ajax({
		url : rootPath + '/adv/change/campaign/status.ajax',
		data : {
			ad_campaign_id : $ele.attr('data-id'),
			ad_campaign_status : status
		},
		success : function(data) {
			if (!data) {
				if (status == 1) {
					$ele.attr('checked', 'checked');
				} else if (status == 0) {
					$ele.removeAttr('checked');
				}
			} else {
				$ele.attr('disabled', 'disabled');
				setTimeout(function() {
					removeCheckBoxDisable(ele);
				}, 2000);
			}
	
		}
	});
}

function removeCheckBoxDisable(ele) {
	$(ele).removeAttr('disabled');
}

function getTableData() {
	wise.ajax({
		url : rootPath + "/adv/campaign/list.list",
		data : $('#search_form').serialize(),
		dataType : 'HTML',
		success : function(html) {
			$('#website_click_table_div').remove();
			$('#app_install_table_div').remove();
			$('#campaign_report').append(html);
			var wcTable = $('#website_click_campaign_table').dataTable({
				'bFilter' : false,
				"order": [[ 2, "desc" ]],
				'info' : false,
				'paging' : false,
				'bLengthChange' : false,
				'retrieve' : true,
				'autoWidth':false
			});
			var aiTable = $('#app_install_campaign_table').dataTable({
				'bFilter' : false,
				"order": [[ 2, "desc" ]],
				'info' : false,
				'paging' : false,
				'bLengthChange' : false,
				'retrieve' : true,
				'autoWidth':false
			});
			$('#website_click_campaign_table').find('td span').tooltip(ellipsisTooltip);
			$('#app_install_campaign_table').find('td span').tooltip(ellipsisTooltip);
		}
	});
}
