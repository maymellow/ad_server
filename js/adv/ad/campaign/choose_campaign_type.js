$(document).on('click', '#create_ad_campiang_form', function(){
	$('.contents-btn-wrap > button').tooltip('destroy');
});

function selectAdCampaignType(ele){
	var $ele = $(ele);
	if($ele.val() == '20' && $('#check_app_url').val() == 'false'){
		$ele.tooltip({
			animation : false,
			container : 'body',
			placement : 'top',
			title : 'You must fill in app URL in advertiser info in order to create App Install campaigns' ,
			trigger : 'focus'
		});
		$ele.focus();
		return;
	}else if($ele.val() == '20'){
		$('#app_list').select2('destroy');
		$('#app_list > option').remove();
		wise.ajax({
			type : 'GET',
			url : rootPath + '/adv/get/app/url.ajax',
			dataType : 'JSON',
			success : function(data) {
				var appendData = '';
				for(var i in data){
					appendData += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
				}
				$('.app__tr').show();
				$('#app_list').append(appendData);
				$('#app_list').select2({
				    minimumResultsForSearch: Infinity,
				    theme : 'band'
				});
			}
		});
	}else{
		$('.app__tr').hide();
	}
	$('#ad_campaign_type').val($ele.val());
	$('.product_type').removeClass('_active');
	$ele.addClass('_active');
}

function chooseAdCampaignCreateType(ele){
	var $ele = $(ele);
	$('#app_list_div').parent().parent().hide();
	if($ele.val()=='new'){
		$('#use_existing_ad_campaign_id').val('0');
		$('#wc_use_existing_campaign > li').remove();
		$('#ai_use_existing_campaign > li').remove();
		$('.product_type').removeClass('_active');
		$('#ad_use_existing_campaign').hide();
		$('#ad_campaign_type_li').show();
	}else if($ele.val()=='using'){
		wise.ajax({
			type : 'GET',
			url : rootPath + '/adv/create/using/campaign/list.ajax',
			dataType : 'JSON',
			success : function(data) {
				var wcAppendData = '';
				var aiAppendData = '';
				for(var i in data.ai){
					aiAppendData += '<li data-id="' + data.ai[i].id +'" data-type="20"> <a href="#" onclick="chooseUseAdCampaign($(this).parent());">' + data.ai[i].name + '</a></li>';
				}
				for(var i in data.wc){
					wcAppendData += '<li data-id="' + data.wc[i].id +'" data-type="10"><a href="#" onclick="chooseUseAdCampaign($(this).parent());">' + data.wc[i].name + '</a></li>';
				}
				$('#wc_use_existing_campaign').append(wcAppendData);
				$('#ai_use_existing_campaign').append(aiAppendData);
				$('#ad_use_existing_campaign').show();
				$('#ad_campaign_type_li').hide();
				$('#ad_use_existing_campaign').find('ul li a').tooltip(ellipsisTooltip);
			}
		});	
	}
}

function chooseUseAdCampaign(ele){
	var $this = $(ele);
	
	if ($this.hasClass('_active')){
		return false;
	}
	else {
		$('#wc_use_existing_campaign, #ai_use_existing_campaign').find('li').removeClass('_active');
		$this.addClass('_active');
	}
	 
	$('#ad_campaign_type').val($this.attr('data-type'));
	$('#use_existing_ad_campaign_id').val($this.attr('data-id'));
}

function createAdCampaignOk(){
	if(productType != $('#ad_campaign_type').val()){
		$('#change_check').val('true');
	}else{
		$('#change_check').val('false');	
	}
	var validationCheck = checkFormValidation();
	if(validationCheck){
		$('#create_ad_campiang_form').submit();
	}
}

function checkFormValidation(){
	var validationCheck = true;
	var campaignTypeSize = 0;
	if($('input[name="adCampaignCreateType"]:checked').val() == 'new'){
		campaignTypeSize = $('#ad_campaign_type_li > li > button._active').length;
	}else if($('input[name="adCampaignCreateType"]:checked').val() == 'using'){
		campaignTypeSize = $('#wc_use_existing_campaign, #ai_use_existing_campaign').find('li._active').length;
	}
	if(campaignTypeSize < 1){
		validationCheck = false;
		$('.contents-btn-wrap > button').tooltip({
			animation : false,
			container : 'body',
			placement : 'top',
			title : 'Please select campaign type.' ,
			trigger : 'focus'
		});
		$('.contents-btn-wrap > button').focus();
	}
	return validationCheck;
}