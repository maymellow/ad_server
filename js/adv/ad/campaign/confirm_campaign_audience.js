function showAdAudiencePopup(ele, type){
	var $ele = $(ele);
	var title = 'Create New Audience';
	if(type == 'edit'){
		title = 'Edit Audience';
	}
	var popup = wisePopupComponent.popup('/adv/audience.popup', {
		addClass : 'popup-add-new-audience',
		popupTitle : title,
		okBtnText : 'OK',
		onFormOpenned : function() {
			if(type == 'new'){
				$('#popup_ad_audience_name_tr').hide();
				$('#campaign_detail_table > tbody').each(function(){
					$(this).find(' > tr').each(function(){
						if($(this).attr('data-row') == 'sub'){
							if(parseInt($(this).attr('data-id')) < 0){
								if(ADCREATIVE_ID > parseInt($(this).attr('data-id'))){
									ADCREATIVE_ID = parseInt($(this).attr('data-id'));
								}
							}
						}
					});
					$('#popup_audience_name').attr('required', false);
					$('#popup_audience_age_max').val('65').trigger('change');
				});
				ADCREATIVE_ID--;
				$('#popup_create_ad_creative_id').val(ADCREATIVE_ID);
			}
			
			$('#popup_audience_bid_type').select2('destroy');
			if($('#ad_campaign_product_type').val() == 10){
				$('#popup_augience_campaign_type').val('10');
				$('#popup_audience_bid_type > option[value="1"]').text('CPWC'); 
			}else if($('#ad_campaign_product_type').val() == 20){
				$('#popup_augience_campaign_type').val('20');
				$('#popup_audience_platform').select2('destroy');
				$('#popup_audience_platform > option[value="0"]').remove();
				$('#popup_audience_platform').select2({
				    minimumResultsForSearch: Infinity,
				    theme : 'band'
				});
				$('#popup_audience_bid_type > option[value="1"]').text('CPIC');
			}
			$('#popup_audience_bid_type').select2({
				minimumResultsForSearch: Infinity,
			    theme : 'band'
			});
		},
		okBtnCallBack : function() {
			var validationCheck = wise.isValidate($('#popup_audience_form'));
			var adCreativeValidationCheck = popupAdAudienceAdCreativeValidationCheck(type);
			if(validationCheck && adCreativeValidationCheck){
				if($('#potential_reach_value').val() == ''){
					$('#potential_reach_value').val('0')
				}
				if(type == 'new'){
					$('#campaign_detail_table > tbody').each(function(){
						$(this).find('> tr').each(function(){
							if($(this).attr('data-row') == 'total'){
								ADAUDIENCE_INDEX = $(this).attr('data-id');							
							}
						});
					});
					ADAUDIENCE_INDEX++;
					$('#popup_ad_audience_id').val(ADAUDIENCE_INDEX);
					$('#popup_creative_spec_list').val('[' + popupMakeAdCreativeSpec() + ']');
					$('#popup_audience_name').val($('#popup_audience_age_min').val()+$('#popup_audience_age_max').val()+'_'+$('#popup_audience_gender option:selected').text()+'_'+$('#popup_audience_platform option:selected').text());
				}
				return true;
			}else{
				return false;
			}
		},
		onTaskComplete : function() {
			wise.ajax({
				type : 'GET',
				url : rootPath + '/adv/make/audience.ajax',
				dataType : 'JSON',
				data : $('#popup_audience_form').serialize(),
				success : function(data) {
					if(type == 'edit'){
						editAdAudience(data);
					}else{
						addAdAudicne(data);
					}
				}
			});	
		}
	},{
		audience_spec : $ele.attr('data-spec'),
		type : type,
		product_type : $('#ad_campaign_product_type').val(),
		app_id : $('#ad_campaign_app_id').val()
	});
}

function editAdAudience(data){
	$('#ad_audience_tr_'+data.adAudience.id).attr('data-spec', data.htmlEle);
	$('#ad_audience_name_'+data.adAudience.id).html(data.adAudience.name);
	$('#ad_audience_bid_'+data.adAudience.id).text(data.adAudience.bid);
	$('#ad_audience_budget_'+data.adAudience.id).text(data.adAudience.budget);
	$('#ad_audience_bid_'+data.adAudience.id).attr('data-val', data.adAudience.bid);
	$('#ad_audience_budget_'+data.adAudience.id).attr('data-val', data.adAudience.budget);
	$('#ad_audience_bid_'+data.adAudience.id).number( true, 0 );
	$('#ad_audience_budget_'+data.adAudience.id).number( true, 0 );
	setAdCampaignBudget();
}

function addAdAudicne(data){
	var appendData = '<tbody id="ad_audience_tbody_' + data.adAudience.id + '">';
	appendData += '		<tr data-row="total" data-id="' + data.adAudience.id + '" id="ad_audience_tr_' + data.adAudience.id + '">';
	appendData += '			<td class="name">';
	appendData += '				<button type="button" class="btn-slide" onclick="openAndCloseAdCreative($(this).parent().parent().parent());">-</button>';
	appendData += '				<span id="ad_audience_name_' + data.adAudience.id + '">' + data.adAudience.name + '</span>';
	appendData += '			</td>';
	appendData += '			<td class="bid" id="ad_audience_bid_' + data.adAudience.id + '" >';
	appendData += 				data.adAudience.bid;
	appendData += '			</td>';
	appendData += '			<td id="ad_audience_budget_' + data.adAudience.id + '" data-val="' + data.adAudience.budget + '" class="budget">';
	appendData += 				data.adAudience.budget;
	appendData += '			</td>';
	appendData += '			<td class="edit">';
	appendData += '				<button type="button" class="btn-edit" onclick="showAdAudiencePopup($(this).parent().parent(), \'edit\');">Edit</button>';
	appendData += '			</td>';
	appendData += '			<td class="delete">';
	appendData += '				<button type="button" class="btn-delete" onclick="deleteAdAudienceEle($(this).parent().parent());">Delete</button>';
	appendData += '			</td>';
	appendData += '		</tr>';
	appendData += '</tbody>';
	$('#campaign_detail_table').append(appendData);
	$('#ad_audience_tr_'+data.adAudience.id).attr('data-spec', data.htmlEle);
	$('#ad_audience_bid_'+data.adAudience.id).number( true, 0 );
	$('#ad_audience_budget_'+data.adAudience.id).number( true, 0 );
	setAdCampaignBudget();
	addAdCreative(data.adCreatives, data.adAudience.id);
}

function deleteAdAudienceEle(ele){
	if($('#campaign_detail_table > tbody').length > 1){
		$(ele).parent().remove();
	}else{
		alert('You cannot delete an ad when there is only one ad set left.');
	}
}