function popupChooseSingleImage(ele){
	var $this = $(ele);
	if($this.parent('ul').prop('disabled') == true){
		return;
	}
	$('#popup_ad_crative_image_div_' + tabIndex).tooltip('destroy');
	$('#popup_ad_crative_image_div_' + tabIndex).removeClass('_error');
	if($('#popup_ad_creative_create_type').val() == 'new'){
		var maxSingleImgCount = 6;
		if($('#popup_ad_creative_new_count').val() != ''){
			maxSingleImgCount = parseInt($('#popup_ad_creative_new_count').val());
		}
		var choiceImgSize = $('#popup_image_list_' + tabIndex + ' li._active').size();
		
		if( $this.hasClass('_active')){
			$this.borderInActive();
		} else {
			if(choiceImgSize < maxSingleImgCount) {
				$this.borderActive();
			} else {
				alert('You can select up to ' + maxSingleImgCount + ' images.');
			}
		}
	}else if($('#popup_ad_creative_create_type').val() == 'edit'){
		$('#popup_image_list_' + tabIndex + ' > li').borderInActive();
		$this.borderActive();
	}
}

function popupChooseSingleDataInit(ele){
	$ele = $(ele); 
	$ele.parent().attr('data-val', $(ele).val());
	popupCreativeCreateTypeUseExisting(false);
	for(var i in bandPosts){
		if($ele.find('option:selected').attr('data-pno') == bandPosts[i].postNo){
			$('#popup_ad_creative_body_'+tabIndex).val(bandPosts[i].content);
			$('#popup_ad_creative_body_'+tabIndex).keyup();
			wise.ajax({
				url : rootPath + '/adv/creative/file/upload/url/img.ajax',
				data : {
					url : $ele.find('option:selected').attr('data-img_url')
				},
				success : function(data) {
					if (!data) {
						alert('Image does not meet its requirements.');
					} else {
						var appendData = '';
						appendData += '<li onclick="popupChooseSingleImage(this);" data-img_index=' + data.id + ' class="_active">';
						appendData += '		<input type="checkbox" checked="checked" />';
						appendData += '		<div class="select-image">';
						appendData += '			<img src="' + JSON.parse(data.jsonThumbnailImageUrl)[0] + '" alt="" />';
						appendData += '		</div>';
						appendData += '		<p>' + JSON.parse(data.jsonImageWidth)[0] + 'x' + JSON.parse(data.jsonImageHeight)[0] + '</p>';
						appendData += '</li>';
						$('#popup_image_list_1').prepend(appendData);
					}
				}
			});
		}
	}
}

$('#popup_ad_creative_single_file').change(function(){
	wise.ajax({
		url : rootPath + '/adv/creative/file/upload/single.ajax',
		data : new FormData($("#popup_single_file_form")[0]),
		processData : false,
		contentType : false,
		success : function(data) {
			if (!data) {
				alert('Image does not meet its requirements.');
			} else {
				var appendData = '';
				appendData += '<li onclick="popupChooseSingleImage(this);" data-img_index=' + data.id + '>';
				appendData += '		<input type="checkbox" />';
				appendData += '		<div class="select-image">';
				appendData += '			<img src="' + JSON.parse(data.jsonThumbnailImageUrl)[0] + '" alt="" />';
				appendData += '		</div>';
				appendData += '		<p>' + JSON.parse(data.jsonImageWidth)[0] + 'x' + JSON.parse(data.jsonImageHeight)[0] + '</p>';
				appendData += '</li>';
				$('#popup_image_list_1').prepend(appendData);
			}
		}
	});
});

function popupMakeSingleAdCreativeSpec(productType, id){
	var returnArray = [];
	var returnJson = {
			sponsoredType : 0,
			description:'',
			child:[],
			creativeType:'',
	};
	returnJson.sponsoredType = parseInt($('#popup_ad_creative_sponsored_type_1').val());	
	returnJson.description = $('#popup_ad_creative_body_1').val();	
	returnJson.creativeType = productType+1;
	var $liSingleImage = $('#popup_image_list_1 li._active');
	$.each($liSingleImage, function(){
		var creative_image = $(this).attr('data-img_index');
		var jsonChild = {};
		if(parseInt(productType/10) == 1){
			var url = '';
			switch($('#popup_ad_creative_url_type_1_0').val()){
				case '0' : 
					url = $('#popup_ad_creative_url_1_0').val();
					break;
				case '1' : 
					url = $('#popup_ad_creative_url_band_home_1_0 > li:first-child').attr('data-val');
					break;
				case '2' :
					url = $('#popup_ad_creative_url_band_post_1_0 > select').val();
					break;
			}
			jsonChild = {
					id:id, 
					image:creative_image, 
					url:url, 
					urlType:parseInt($('#popup_ad_creative_url_type_1_0').val()), 
					urlTitle:$('#popup_ad_creative_one_url_title_1_0_1').val(),
					urlSubTitle:'', 
					callToAction:$('#popup_ad_creative_single_image_call_to_action_1_0').val()
				};
		}else if(parseInt(productType/10) == 2){
			jsonChild = {
					id:id, 
					image:creative_image, 
					urlTitle:$('#popup_ad_creative_one_url_title_1_0_1').val(),
					urlSubTitle:'', 
					callToAction:$('#popup_ad_creative_single_image_call_to_action_1_0').val()
				};
		}
		if($('#popup_ad_creative_url_tile_type_1_0').val() == '2'){
			jsonChild.urlTitle = $('#popup_ad_creative_two_url_title_1_0_2').val();
			jsonChild.urlSubTitle = $('#popup_ad_creative_two_url_sub_title_1_0_2').val();
		}
		returnJson.child.push(jsonChild);
		id--;
	});
	returnArray.push(returnJson);
	
	return returnArray;
}


function popupValidationCheckSingleType(validationCheck){
	var imageCheck = $('#popup_image_list_1 > li._active').length;
	if($('#popup_ad_creative_body_1').val().length > 105){
		validationCheck = false;
		$('#popup_ad_creative_body_1').addClass('parsley-error');
		$('#popup_ad_creative_body_1').tooltip({
			animation : false,
			container : 'body',
			placement : 'top-left',
			title : 'Maximum Characters : 105.'
		});
		
	}
	if(imageCheck < 1){
		validationCheck = false;
		$('#popup_ad_crative_image_div_1').addClass('_error');
		$('#popup_ad_crative_image_div_1').tooltip({
			animation : false,
			container : 'body',
			placement : 'top-left',
			title : 'Please select image.'
		});
	}
	if($('#popup_ad_creative_url_type_1_0').val() == '2' && ($('#popup_ad_creative_url_band_post_1_0 > select').val() == null || $('#popup_ad_creative_url_band_post_1_0 > select').val() == '')){
		validationCheck = false;
		$('#popup_ad_creative_url_band_post_1_0').addClass('_error');
		$('#popup_ad_creative_url_band_post_1_0').tooltip({
			animation : false,
			container : 'body',
			placement : 'top-left',
			title : 'Please select exising post.'
		});
	}
	return validationCheck;
}