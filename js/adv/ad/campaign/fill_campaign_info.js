function removeErrorMark(){
	$('#start_hour').removeClass('parsley-error');
	$('#start_min').removeClass('parsley-error');
	$('#start_date').removeClass('parsley-error');
	$('#start_date').tooltip('destroy');
	$('#end_hour').removeClass('parsley-error');
	$('#end_min').removeClass('parsley-error');
	$('#end_date').removeClass('parsley-error');
	$('#end_date').tooltip('destroy');
}

$.fn.extend({ 
    borderActive: function () {
    	this.addClass('_active');
    	$(this).find('input:checkbox').eq(0).attr('checked', 'checked');
    	return this; 
    }
});

$.fn.extend({ 
    borderInActive: function () {
    	this.removeClass('_active');
    	$(this).find('input:checkbox').eq(0).removeAttr('checked');
    	return this; 
    }
});

function makeAdItemSpec(){
	var adItems = new Array();
	$('#ad_audience_ul > li').each(function(){
		if($(this).attr('data-spec')){
			var adAudienceId = $(this).attr('data-id')
			$('#ad_creative_ul > li').each(function(){
				if($(this).attr('id') != 'ad_creative_add_button'){
					var adItem = {
							adAudience : adAudienceId,
							adCreative : $(this).attr('data-id')
					}
					adItems.push(adItem);
				}
			});
			$('#ad_creative_library_ul > li').each(function(){
				if($(this).hasClass('_active')){
					var adItem = {
							adAudience : adAudienceId,
							adCreative : $(this).attr('data-id')
					}
					adItems.push(adItem);
				}
			});
		}
	});
	return adItems;
}

function createAdCampaignSubmit(){
	var validationCheck = checkValidation();
	if(validationCheck){
		$('#ad_audience_specs').val("["+makeAdAudienceSpec()+"]");
		$('#ad_creative_specs').val("["+makeAdCreativeSpec()+"]");
		$('#ad_item_specs').val(JSON.stringify(makeAdItemSpec()));
		$('#create_ad_campaign_form').attr('action', '/adv/create/campaign/adform');
		if($('#scedule_type_on_schedule').is(':checked')){
			$('#on_schedule_date').val('['+schedule_json()+']');
		}else{
			$('#on_schedule_date').val('');
		}
		$('#create_ad_campaign_form').submit();
	}
}
function createAdCampaignBack(){
	$('#ad_audience_specs').val("["+makeAdAudienceSpec()+"]");
	$('#ad_creative_specs').val("["+makeAdCreativeSpec()+"]");
	$('#create_ad_campaign_form').attr('action', '/adv/create/campaign/choosetype');
	if($('#scedule_type_on_schedule').is(':checked')){
		$('#on_schedule_date').val('['+schedule_json()+']');
	}else{
		$('#on_schedule_date').val('');
	}
	$('#create_ad_campaign_form').submit();
}

function checkValidation(){
	var validationCheck = true;
	validationCheck = wise.isValidate($('#create_ad_campaign_form'));
	var adAudienceSize = $('#ad_audience_ul > li').length;
	if(adAudienceSize < 2 ){
		$('#ad_audience_add_btn').tooltip({
			animation : false,
			container : 'body',
			placement : 'right',
			title : 'Please add audience.'
		});
		$('#ad_audience_add_btn').addClass('_error');
		validationCheck = false;
	}
	
	var adCreativeSize = 0;
	adCreativeSize += $('#ad_creative_ul > li').length;
	adCreativeSize += $('#ad_creative_library_ul > li._active').length;
	if(adCreativeSize < 2){
		$('#ad_creative_add_button').tooltip({
			animation : false,
			container : 'body',
			placement : 'right',
			title : 'Please add creative.'
		});
		$('#ad_creative_add_button').addClass('_error');
		validationCheck = false;
	}

	var time = moment($('#end_date').val(), 'YYYY.MM.DD') - moment($('#start_date').val(), 'YYYY.MM.DD');
	if(time == 0){
		var hourTime = moment($('#end_hour').val() + ':' + $('#end_min').val(), 'HH:mm') - moment($('#start_hour').val() + ':' + $('#start_min').val(), 'HH:mm');
		if(hourTime < 1800000){
			$('#end_hour').addClass('parsley-error');
			$('#end_min').addClass('parsley-error');
			$('#end_date').addClass('parsley-error');
			$('#end_date').tooltip({
				animation : false,
				container : 'body',
				placement : 'top-left',
				title : 'End time must be at least 30 minutes later than the start time.'
			});
			validationCheck = false;
		}
	}
	
	var dateDay = moment($('#start_date').val(), 'YYYY.MM.DD') - moment(moment(dt).format('YYYY.MM.DD'));
	if(dateDay < 0){
		$('#start_date').val(moment(dt).format('YYYY.MM.DD'));
	}else if(dateDay == 0){
		var datetime = moment($('#start_hour').val() + ':' + $('#start_min').val(), 'HH:mm') - moment(moment(dt).format('HH:mm'));
		if(dateDay <= 0){
			$('#start_hour').val(moment(dt).format('HH'));
			$('#start_min').val(moment(dt).format('mm'));
		}
	}
	
	if($('#scedule_type_on_schedule').is(':checked') && schedule_json() == ''){
		$('#time_table').find('.day1').tooltip({
			animation : false,
			container : 'body',
			placement : 'top-left',
			title : 'Select schedule.'
		});
		$('#time_table').find('.ui-selectable').addClass('_error');
		validationCheck = false;
	}
	return validationCheck;
}

function selectCampaignDate(ele){
	$('#start_date').val(moment(dt).format('YYYY.MM.DD'));
	$('#end_date').val(moment(dt).add($(ele).val(), 'day').format('YYYY.MM.DD'));
}

function dataPkSeleteCampaignDate(){
	var time = moment($('#end_date').val(), 'YYYY.MM.DD') - moment($('#start_date').val(), 'YYYY.MM.DD');
	if(time < 0){
		$('#end_date').val($('#start_date').val());
	}
	$('#select_campaign_date').select2('destroy');
	$('#select_campaign_date').val('-1').select2({
		minimumResultsForSearch : Infinity,
		theme : 'band'
	});
}

function deleteThisEle(ele){
	$(ele).remove();
	$('#ad_audience_add_btn').show();
}

function selectScheduleType(ele){
	var $ele = $(ele);
	if($ele.val() == '0'){
		$('#time_table').hide();
		$('.ui-selectee').removeClass('ui-selected');
		$('.ui-selectee').removeAttr('prev');
	}else if($ele.val() == '1'){
		$('#time_table').show();
	}
}