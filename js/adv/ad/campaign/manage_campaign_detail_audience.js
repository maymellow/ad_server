function changeAdAudienceStatus(ele, type){
	var $ele = $(ele);
	var status = 0;
	if(type == 'modify'){
		if($ele.is(":checked")){
			$ele.attr('checked', 'checked');
			status = 0;
		}else{
			$ele.removeAttr('checked');
			status = 1;
		}
	}else{
		if($('#campaign_detail_list > tbody').length < 2){
			alert('You cannot delete an ad when there is only one ad Set left.');
			return;
		}
		status = 2;
	}
	wise.ajax({
		type : 'POST',
		url : rootPath + '/adv/change/audience/status.ajax',
		dataType : 'JSON',
		data : {
			ad_campaign_id : $('#ad_campaign_id').val(),
			ad_audience_id : $ele.attr('data-id'),
			ad_audience_status : status
		},
		success : function(data) {
			if (!data) {
				if (status == 1) {
					$ele.attr('checked', 'checked');
				} else if (status == 0) {
					$ele.removeAttr('checked');
				}else if(status == 2){
					alert('delete 실패');
				}
			} else {
				if(status == 2){
					location.reload();
				}else{
					$ele.attr('disabled', 'disabled');
					setTimeout(function() {
						removeCheckBoxDisable($ele);
					}, 2000);						
				}
			}
	
		}
	});
}


function showAudiencePopup(ele){
	var $ele = $(ele);
	var title = 'Add New Audience';
	if($ele.attr('data-type') == 'edit'){
		var title = 'Edit Audience';
	}
	var popup = wisePopupComponent.popup('/adv/audience.popup', {
		addClass : 'popup-add-new-audience',
		popupTitle : title,
		okBtnText : 'OK',
		okBtnCallBack : function() {
			var validationCheck = wise.isValidate($('#popup_audience_form'));
			var adCreativeValidationCheck = popupAdAudienceAdCreativeValidationCheck($ele.attr('data-type'));
			if(validationCheck && adCreativeValidationCheck){
				if($('#potential_reach_value').val() == ''){
					$('#potential_reach_value').val('0');
				}
				if($ele.attr('data-type') == 'new'){
					$('#popup_audience_name').val($('#popup_audience_age_min').val()+$('#popup_audience_age_max').val()+'_'+$('#popup_audience_gender option:selected').text()+'_'+$('#popup_audience_platform option:selected').text());
					$('#popup_creative_spec_list').val('[' + popupMakeAdCreativeSpec() + ']');
					$('#popup_ad_campaign_id').val($('#ad_campaign_id').val());
					return true;
				}else{
					return true;
				}
			}else{
				return false;
			}
		},
		onFormOpenned : function() {
			$('#popup_create_ad_creative_id').val('-1');
			$('#popup_audience_bid_type').select2('destroy');
			$('#popup_audience_platform').select2('destroy');
			if($('#ad_campaign_product_type').val() == 10){
				$('#popup_augience_campaign_type').val('10');
				$('#popup_audience_bid_type > option[value="1"]').text('CPWC'); 
			}else if($('#ad_campaign_product_type').val() == 20){
				$('#popup_augience_campaign_type').val('20');
				$('#popup_audience_bid_type > option[value="1"]').text('CPIC');
				$('#popup_audience_platform > option[value="0"]').remove();
			}
			$('#popup_audience_bid_type').select2({
				minimumResultsForSearch: Infinity,
			    theme : 'band'
			});
			$('#popup_audience_platform').select2({
				minimumResultsForSearch: Infinity,
				theme : 'band'
			});
			if($(ele).attr('data-type') == 'new'){
				$('#popup_ad_audience_name_tr').hide();
				$('#popup_audience_age_max').val('65').trigger('change');
				$('#popup_audience_name').attr('required', false);
			}
		},
		onTaskComplete : function() {
			var url = '/adv/edit/audience/info.ajax';
			if($ele.attr('data-type') == 'new'){
				url = '/adv/add/audience/info.ajax';
			}
			wise.ajax({
				type : 'POST',
				url : rootPath + url,
				dataType : 'JSON',
				data : $('#popup_audience_form').serialize(),
				success : function(data) {
					if(data){
						location.reload();
					}else{
						alert('실패 ㅠㅠ');
					}
				}
			});
		}
	},{
		audience_spec : $ele.attr('data-spec'),
		type : $ele.attr('data-type'),
		product_type : $('#ad_campaign_product_type').val(),
		app_id : $('#ad_campaign_app_id').val()
	});
}
