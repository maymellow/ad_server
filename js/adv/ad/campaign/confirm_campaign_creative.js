function addAdCreative(data, adAudienceId){
	var appendData = '';
	for(var i in data){
		appendData += '		<tr data-row="sub" data-id="' + data[i].adCreative.id + '"  id="ad_creative_tr_' + data[i].adCreative.id + '">';
		appendData += '			<td class="name">';
		appendData += '				<span class="library">'
		appendData += '					<img id="ad_creative_img_' + data[i].adCreative.id + '" src="' + JSON.parse(data[i].adCreative.jsonImageUrl)[0] + '" alt="" />';
		appendData += '				</span>';
		if(data[i].adCreative.adCreativeStatus == 0){
			appendData += '				<span class="_approved" id="ad_creative_body_' + data[i].adCreative.id + '">' + data[i].adCreative.body + '</span>';
		}else{
			appendData += '				<span class="_pending" id="ad_creative_body_' + data[i].adCreative.id + '">' + data[i].adCreative.body + '</span>';
		} 
		appendData += '			</td>';
		appendData += '			<td class="bid">';
		appendData += '			</td>';
		appendData += '			<td class="budget">';
		appendData += '			</td>';
		appendData += '			<td class="edit">';
		if(data[i].adCreative.adCreativeStatus == 0){
			appendData += '				<button type="button" class="btn-edit" onclick="showEditCreativePopup($(this).parent().parent());">Edit</button>';
		}else{
			appendData += '				<button type="button" class="btn-edit" onclick="showEditCreativePopup($(this).parent().parent());">Edit</button>';
		} 
		appendData += '			</td>';
		appendData += '			<td class="delete">';
		appendData += '				<button type="button" class="btn-delete" onclick="deleteAdCreativeEle($(this).parent().parent(), ' + adAudienceId + ');" >Delete</button>';
		appendData += '			</td>';
		appendData += '		</tr>';
	}
	 $('#ad_audience_tbody_' + adAudienceId).append(appendData);
	for(var i in data){
		$('#ad_creative_tr_'+data[i].adCreative.id).attr('data-spec', data[i].htmlEle);
	}
}

function deleteAdCreativeEle(ele, adAudienceId){
	if($('#ad_audience_tbody_'+adAudienceId+' > tr').length > 2){
		$(ele).remove();
	}else{
		alert('You cannot delete an ad when there is only one ad left.');
	}
}

function showNewCreativePopup(){
	var adAudienceIds = new Array();
	var popup = wisePopupComponent.popup('/adv/new/creative.popup', {
		addClass : 'popup-add-new-creative',
		popupTitle : 'Add New Creative',
		okBtnText : 'OK',
		onFormOpenned : function() {
			var getText = function($ele){
				if(typeof $ele.data('title') === 'undefined'){
					return wise.escapeHtml($ele.text());
				}else{
					return wise.escapeHtml($ele.data('title'));
				}
			}
			
			var appendData = '';
			$('#campaign_detail_table > tbody').each(function(){
				$(this).find('> tr').each(function(){
					if($(this).attr('data-row') == 'total'){
						appendData += '<li data-id="' + $(this).attr('data-id') + '">';
						appendData += '<input type="checkbox" id="ad_audience_id_' + $(this).attr('data-id') + '" onchange="popupAdAudienceIdCheck(this);" required data-parsley-check="[1]" />';
						appendData += '<label for="ad_audience_id_' + $(this).attr('data-id') + '"><span>' + getText($('#ad_audience_name_'+ $(this).attr('data-id'))) + '</span></label>';
						appendData += '</li>';
					}
				});
			});
			
			$('#campaign_detail_table > tbody').each(function(){
				$(this).find(' > tr').each(function(){
					if($(this).attr('data-row') == 'sub'){
						if(parseInt($(this).attr('data-id')) < 0){
							if(ADCREATIVE_ID > parseInt($(this).attr('data-id'))){
								ADCREATIVE_ID = parseInt($(this).attr('data-id'));
							}
						}
					}
				});
			});
			ADCREATIVE_ID--;
			$('#popup_create_ad_creative_id').val(ADCREATIVE_ID);
			$('#popup_audience_list').append(appendData);
			$('#popup_audience_list').find('label span').tooltip(ellipsisTooltip);
			
		},
		okBtnCallBack : function() {
			var validationCheck = popupNewAdCreativeValidationCheck();
			if(validationCheck){
				$('#popup_creative_spec_list').val('[' + popupMakeAdCreativeSpec() + ']');
				adAudienceIds = popupMakeAdAudienceId();
				return true;
			}else{
				return false;
			}
		},
		onTaskComplete : function() {
			wise.ajax({
				type : 'POST',
				url : rootPath + '/adv/make/creative/list.ajax',
				dataType : 'JSON',
				data : $('#popup_new_creative_form').serialize(),
				success : function(data) {
					for(var i in adAudienceIds){
						addAdCreative(data, adAudienceIds[i]);
					}
				}
			});
		}
	},{
		product_type : $('#ad_campaign_product_type').val(),
		app_id : $('#ad_campaign_app_id').val()
	});
}

function showEditCreativePopup(ele){
	var adCreativeId = $(ele).attr('data-id');
	var popup = wisePopupComponent.popup('/adv/create/new/creative.popup', {
		addClass : 'popup-upload-new-creative',
		popupTitle : 'Edit Creative',
		okBtnText : 'OK',
		okBtnCallBack : function() {
			return popupAdCreativeValidationCheck();
		},
		onFormOpenned : function() {
			$('#popup_ad_creative_new_count').val(1);
		},
		onTaskComplete : function() {
			$('#campaign_detail_table > tbody').each(function(){
				$(this).find(' > tr').each(function(){
					if($(this).attr('data-row') == 'sub'){
						if(parseInt($(this).attr('data-id')) < 0){
							if(ADCREATIVE_ID > parseInt($(this).attr('data-id'))){
								ADCREATIVE_ID = parseInt($(this).attr('data-id'));
							}
						}
					}
				});
			});
			ADCREATIVE_ID--;
			var returnArray = createNewAdCreativeMakeSpec(parseInt($('#ad_campaign_product_type').val()), ADCREATIVE_ID);
			wise.ajax({
				type : 'POST',
				url : rootPath + '/adv/make/creative/js_json.ajax',
				dataType : 'JSON',
				data : {
					spec : JSON.stringify(returnArray)
				},
				success : function(data) {
					$('#ad_creative_body_'+adCreativeId).removeClass();
					$('#ad_creative_body_'+adCreativeId).addClass('_pending');
					$('#ad_creative_body_'+adCreativeId).html(data[0].adCreative.body);
					$('#ad_creative_body_'+adCreativeId).attr('id', 'ad_creative_body_'+data[0].adCreative.id);
					$('#ad_creative_img_'+adCreativeId).attr('src', JSON.parse(data[0].adCreative.jsonImageUrl)[0]);
					$('#ad_creative_img_'+adCreativeId).attr('id', 'ad_creative_img_'+data[0].adCreative.id);
					$('#ad_creative_tr_'+adCreativeId).attr('data-spec', data[0].htmlEle);
					$('#ad_creative_tr_'+adCreativeId).attr('data-id', data[0].adCreative.id);
					$('#ad_creative_tr_'+adCreativeId).attr('id', 'ad_creative_tr_'+data[0].adCreative.id);
				}
			});
		}
	}, {
		ad_creative_id : adCreativeId < 0 ? null : adCreativeId,
		ad_creative_spec : $(ele).attr('data-spec'),
		productType : parseInt($('#ad_campaign_product_type').val()/10)
	});
}