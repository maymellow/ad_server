$(document).on('change', '#popup_audience_age_min,#popup_audience_age_max', function() {
	var _this = $(this);
	var _idValue = _this.attr('id');
	var _startSelect = $('#popup_audience_age_min');
	var _endSelect = $('#popup_audience_age_max');
	
	if (parseInt(_startSelect.val()) > parseInt(_endSelect.val())) {
		if (_idValue == 'popup_audience_age_min') {
			_endSelect.val(_startSelect.val()).trigger('change');
		}else if(_idValue == 'popup_audience_age_max'){
			_startSelect.val(_endSelect.val()).trigger('change');
		}
	}

});

function popupSelectAdAudicneBidType(ele){
	if($(ele).val() == '0'){
		$('#popup_audience_bid').attr('data-parsley-min', '500');
		$('#popup_audience_bid').attr('data-parsley-max', '10000');
	}else{
		$('#popup_audience_bid').attr('data-parsley-min', '70');
		$('#popup_audience_bid').attr('data-parsley-max', '5000');
	}
}

function popupChooseCreatedAdCreative(ele) {
	var $ele = $(ele);
	$('#popup_ad_creative_new_button').tooltip('destroy');
	$('#popup_ad_creative_new_button').removeClass('_error');
	if ($ele.hasClass('_active')) {
		$ele.removeClass('_active');
	} else {
		$ele.addClass('_active');
	}
	popupDrawPreview();
}

function popupMakeAdCreativeSpec(){
	var creativeList = new Array();
	$('#popup_new_ad_creative_ul > li').each(function() {
		if($(this).attr('id') != 'popup_ad_creative_new_button'){
			creativeList.push($(this).attr('data-spec'));
		}
	});
	$('#popup_creatied_creative_li > li').each(function() {
		if ($(this).hasClass('_active')) {
			creativeList.push($(this).attr('data-spec'));
		}
	});
	return creativeList;
}

function popupDrawNewAdCreative(val){
	var adCreativeId = -1;
	var appendData = '';
	for(var i in val){
		appendData += '<li class="item" id="popup_new_ad_creative' + val[i].adCreative.id + '" data-id="' + val[i].adCreative.id + '" data-spec="' + val[i].htmlEle + '">';
		appendData += '		<div class="image">';
		appendData += '			<img id="popup_new_ad_creative_img' + val[i].adCreative.id + '" src="' + JSON.parse(val[i].adCreative.jsonImageUrl)[0] + '" alt="">';
		appendData += '		</div>';
		appendData += '		<div class="button__wrap">';
		appendData += '			<button type="button" class="btn-modify" onclick="popupEditNewAdCreative($(this).parent().parent());">Modify</button>';
		appendData += '			<button type="button" class="btn-close" onclick="popupDeleteNewAdCreative($(this).parent().parent());">close</button>';
		appendData += '		</div>';
		appendData += '</li>';
	}
	adCreativeId = val[i].adCreative.id-1;
	$('#popup_create_ad_creative_id').val(adCreativeId);
	$('#popup_new_ad_creative_ul').append(appendData);
	$('#popup_ad_creative_new_button').appendTo($('#popup_new_ad_creative_ul'));
}

function popupDrawPreview() {
	var creativeSpecList = popupMakeAdCreativeSpec();
	if(creativeSpecList.length < 1){
		$('#popup_ad_creative_preview_empty').show();
		$('#popup_ad_creative_preview').hide();
		return;
	}
	wise.ajax({
		type : 'POST',
		url : rootPath + '/adv/get/creative/preview.ajax',
		dataType : 'HTML',
		data : {
			spec : '[' + creativeSpecList.toString() + ']'
		},
		success : function(data) {
			$('#popup_ad_creative_preview_empty').hide();
			$('#popup_ad_creative_preview').show();
			$('#popup_ad_creative_preview').html(data);
			previewFunction();
		}
	});
}

var p2 = $.extend({}, wisePopupComponent);
function popupAddNewCreative(){
	$('#popup_ad_creative_new_button').tooltip('destroy');
	$('#popup_ad_creative_new_button').removeClass('_error');
	var popup = p2.popup('/adv/create/new/creative.popup', {
		popupComponentName:'p2',
		addClass : 'popup-upload-new-creative',
		popupTitle : 'Upload New Creative',
		okBtnText : 'OK',
		okBtnCallBack : function() {
			return popupAdCreativeValidationCheck();
		},
		onTaskComplete : function() {
			var returnArray = createNewAdCreativeMakeSpec(parseInt($('#ad_campaign_product_type').val()), parseInt($('#popup_create_ad_creative_id').val()));
			wise.ajax({
				type : 'POST',
				url : rootPath + '/adv/make/creative/js_json.ajax',
				dataType : 'JSON',
				data : {
					spec : JSON.stringify(returnArray)
				},
				success : function(data) {
					popupDrawNewAdCreative(data);
					popupDrawPreview();
				}
			});
		}
	},{
		productType : parseInt($('#ad_campaign_product_type').val()/10)
	});
}

function popupEditNewAdCreative(ele){
	var $ele = $(ele);
	var popup = p2.popup('/adv/create/new/creative.popup', {
		popupComponentName:'p2',
		addClass : 'popup-upload-new-creative',
		popupTitle : 'Edit Creative',
		okBtnText : 'OK',
		okBtnCallBack : function() {
			return popupAdCreativeValidationCheck();
		},
		onFormOpenned : function() {
			$('#popup_ad_creative_new_count').val(1);
		},
		onTaskComplete : function() {
			var returnArray = createNewAdCreativeMakeSpec(parseInt($('#popup_ad_creative_produect_type').val())*10, $ele.attr('data-id'));
			wise.ajax({
				type : 'POST',
				url : rootPath + '/adv/make/creative/js_json.ajax',
				dataType : 'JSON',
				data : {
					spec : JSON.stringify(returnArray)
				},
				success : function(data) {
					$('#popup_new_ad_creative'+$ele.attr('data-id')).attr('data-spec', data[0].htmlEle);
					$('#popup_new_ad_creative_img'+$ele.attr('data-id')).attr('src', JSON.parse(data[0].adCreative.jsonImageUrl)[0]);
					popupDrawPreview();
				}
			});
		}
	}, {
		ad_creative_spec : $ele.attr('data-spec')
	});
}

function popupDeleteNewAdCreative(ele){
	$(ele).remove();
	popupDrawPreview();
}

function popupAdAudienceAdCreativeValidationCheck(type){
	var validationCheck = true;
	var adCreativeSize = 0;
	if(type == 'new'){
		adCreativeSize += $('#popup_new_ad_creative_ul > li').length;
		adCreativeSize += $('#popup_creatied_creative_li > li._active').length;
		if(adCreativeSize < 2){
			$('#popup_ad_creative_new_button').tooltip({
				animation : false,
				container : 'body',
				placement : 'right',
				title : 'Please add creative.'
			});
			$('#popup_ad_creative_new_button').addClass('_error');
			validationCheck = false;
		}
	}
	if(parseInt($('#popup_audience_bid').val())%10){
		$('#popup_audience_bid').addClass('parsley-error');
		$('#popup_audience_bid').tooltip({
			animation : false,
			container : 'body',
			placement : 'top-left',
			title : 'You must enter numbers in 10s. (e.g. 90, 100, 200, etc)'
		});
		validationCheck = false;
	}
	return validationCheck;
}