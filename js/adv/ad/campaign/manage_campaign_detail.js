var MODIFY_VALUE;
$(function(){
	$('#modify_table_div').find('p.modify-show').tooltip(ellipsisTooltip);
	$('#campaign_detail_list').find('.dotdotdot_ele').tooltip(ellipsisTooltip);
	/*$('#head_campaign_name').tooltip(ellipsisTooltip);*/
});
function showModifyCampaignInfo(){
	MODIFY_VALUE = {
			status : $('#ad_campaign_status').val(),
			budget : $('#budget').val(),
			service : $('#service').val(),
			startDate : $('#campaign_start_date').val(),
			startHour : $('#campaign_start_hour').val(),
			startMin : $('#campaign_start_min').val(),
			endDate : $('#campaign_end_date').val(),
			endHour : $('#campaign_end_hour').val(),
			endMin : $('#campaign_end_min').val(),
			scheduleType : $(':radio[name="scheduleType"]:checked').val()
	}
	$('#delivery_option_'+$('#span_delivery').attr('data-val')).trigger('click');
	$('#modify_table_div').addClass('_modify');
}

function modifyCampaignInfo(){
	var check = validationCheck();
	if(check){
		$('#campaign_end').val($('#campaign_end_date').val()+' '+$('#campaign_end_hour').val()+':'+$('#campaign_end_min').val());
		$('#on_schedule_date').val('['+schedule_json()+']');
		wise.ajax({
			url : rootPath + '/adv/change/campaign/data.ajax',
			data : $('#campaign_data').serialize(),
			success : function(data) {
				if(data){
					if($('#ad_campaign_status').val() == 2){
						location.href = rootPath + '/adv/manage/campaign';
					}else{
						location.reload();
					}
				}else{
					alert('fail');
				}
			}
		});
	}
}

function validationCheck(){
	var validationCheck = wise.isValidate($('#campaign_data'));
	var time = moment($('#campaign_end_date').val(), 'YYYY.MM.DD') - moment($('#campaign_start_date').val(), 'YYYY.MM.DD');
	if(time == 0){
		var hourTime = moment($('#campaign_end_hour').val() + ':' + $('#campaign_end_min').val(), 'HH:mm') - moment($('#campaign_start_hour').val() + ':' + $('#campaign_start_min').val(), 'HH:mm');
		if(hourTime < 1800000){
			$('#campaign_end_hour').addClass('parsley-error');
			$('#campaign_end_min').addClass('parsley-error');
			$('#campaign_end_hour').tooltip({
				animation : false,
				container : 'body',
				placement : 'right',
				title : 'End time must be at least 30 minutes later than the start time.'
			});
			validationCheck = false;
		}
	}
	
	var nowTime = moment($('#campaign_end_date').val() + ' ' + $('#campaign_end_hour').val() + ':' + $('#campaign_end_min').val(), 'YYYY.MM.DD HH:mm') - dt;
	if(nowTime < 0){
		$('#campaign_end_hour').addClass('parsley-error');
		$('#campaign_end_min').addClass('parsley-error');
		$('#campaign_end_hour').tooltip({
			animation : false,
			container : 'body',
			placement : 'right',
			title : 'End time cannot be earlier than the time right now.'
		});
		validationCheck = false;
	}
	if(parseInt($('#agency_service_data').val()) < (parseInt($('#service').val()) - MODIFY_VALUE.service)){
		$('#service').addClass('parsley-error');
		$('#service').addClass('parsley-error');
		$('#service').tooltip({
			animation : false,
			container : 'body',
			placement : 'right',
			title : 'Entered service amount cannot be bigger than the service amount you possess right now.'
		});
		validationCheck = false;
	}
	if($('#schedule_type_1').is(':checked') && schedule_json() == ''){
		$('#time_table').find('.day1').tooltip({
			animation : false,
			container : 'body',
			placement : 'top-left',
			title : 'Select schedule.'
		});
		$('#time_table').find('.ui-selectable').addClass('_error');
		validationCheck = false;
	}
	return validationCheck;
}

function cancelModifyCampaignInfo(){
	$('.ui-selectee').removeClass('ui-selected');
	$('.ui-selectee').attr('prev', 'off');
	$('#schedule_type_'+MODIFY_VALUE.scheduleType).trigger('click');
	if(MODIFY_VALUE.scheduleType == 1){
		setSchedule($('#on_schedule_date').val());
	}
	$('#ad_campaign_status').val(MODIFY_VALUE.status).trigger('change');
	$('#budget').val(MODIFY_VALUE.budget);
	$('#service').val(MODIFY_VALUE.service);
	$('#campaign_start_date').val(MODIFY_VALUE.startDate);
	$('#campaign_start_hour').val(MODIFY_VALUE.startHour);
	$('#campaign_start_min').val(MODIFY_VALUE.startMin);
	$('#campaign_end_date').val(MODIFY_VALUE.endDate);
	$('#campaign_end_hour').val(MODIFY_VALUE.endHour);
	$('#campaign_end_min').val(MODIFY_VALUE.endMin);
	$('#campaign_name').val($('#span_campaign_name').text());
	$('#modify_table_div').removeClass('_modify');
}

function checkScheduleType(ele){
	var $ele = $(ele);
	if($ele.val() == '0'){
		$('#time_table').hide();
	}else if($ele.val() == '1'){
		$('#time_table').show();
	}
}

function dataPkSeleteCampaignDate(){
	var time = moment($('#end_date').val(), 'YYYY.MM.DD') - moment($('#start_date').val(), 'YYYY.MM.DD');
	if(time < 0){
		$('#end_date').val($('#start_date').val());
	}
}

function openAllCreative(){
	$('#campaign_detail_list > tbody').removeClass('_fold');
}
function closeAllCreative(){
	$('#campaign_detail_list > tbody').addClass('_fold');
}
function showAndHideAdList(ele){
	var $ele = $(ele);
	if($ele.hasClass('_fold')){
		$ele.removeClass('_fold');
	}else{
		$ele.addClass('_fold');
	}
}


function changeAdItemStatus(ele, type){
	var $ele = $(ele);
	var status = 0;
	if(type == 'modify'){
		if($ele.is(":checked")){
			$ele.attr('checked', 'checked');
			status = 0;
		}else{
			$ele.removeAttr('checked');
			status = 1;
		}
	}else{
		if($('#ad_audience_tbody_'+$ele.attr('data-ad_audience_id')+' > tr').length < 3){
			alert('You cannot delete an ad when there is only one ad left.');
			return;
		}
		status = 2;
	}
	wise.ajax({
		url : rootPath + '/adv/change/aditem/status.ajax',
		data : {
			ad_audience_id : $ele.attr('data-ad_audience_id'),
			ad_item_id : $ele.attr('data-id'),
			ad_item_status : status
		},
		success : function(data) {
			if (!data) {
				if (status == 1) {
					$ele.attr('checked', 'checked');
				} else if (status == 0) {
					$ele.removeAttr('checked');
				}else if(status == 2){
					alert('delete 실패');
				}
			} else {
				if(status == 2){
					location.reload();
				}else{
					$ele.attr('disabled', 'disabled');
					setTimeout(function() {
						removeCheckBoxDisable(ele);
					}, 2000);						
				}
			}
	
		}
	});
}

function removeCheckBoxDisable(ele) {
	$(ele).removeAttr('disabled');
}
