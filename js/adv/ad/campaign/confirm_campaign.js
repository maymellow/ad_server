var ADAUDIENCE_INDEX = 0;
var ADCREATIVE_INDEX = 0;
var MODIFY_VALUE;
var ADCREATIVE_ID = -1;

function removeErrorMark() {
	$('#campaign_start_hour').removeClass('parsley-error');
	$('#campaign_start_min').removeClass('parsley-error');
	$('#campaign_start_date').removeClass('parsley-error');
	$('#campaign_start_date').tooltip('destroy');
	$('#campaign_end_hour').removeClass('parsley-error');
	$('#campaign_end_min').removeClass('parsley-error');
	$('#campaign_end_date').removeClass('parsley-error');
	$('#campaign_end_date').tooltip('destroy');
}

function cancelCreateCampaign() {
	$(location).attr('href', '/adv/manage/campaign');
}
$(function() {
//	window.onbeforeunload = function() {
//		event.returnValue = '"작성한 내용이 사라집니다."';
//	};
	$('#modify_table_div').find('.name__tr p.modify-show').tooltip(ellipsisTooltip);
	$('#campaign_detail_table').find('.dotdotdot_ele').tooltip(ellipsisTooltip);
});

function makeAdCampaignInfo() {
	var adAudiences = new Array();
	var adCreatives = new Array();
	var adItems = new Array();
	var adCreativeIds = new Array();
	$('#campaign_detail_table > tbody').each(function() {
		var adAudienceId = 0;
		$(this).find('> tr').each(function() {
			if ($(this).attr('data-row') == 'total') {
				adAudiences.push($(this).attr('data-spec'));
				adAudienceId = parseInt($(this).attr('data-id'));
			} else {
				if (!isInArray(adCreativeIds, $(this).attr('data-id'))) {
					adCreatives.push($(this).attr('data-spec'));
					adCreativeIds.push($(this).attr('data-id'));
				}
				var adItem = {
					adAudienceId : adAudienceId,
					adCreativeId : parseInt($(this).attr('data-id'))
				}
				adItems.push(adItem);
			}
		});
	});
	$('#ad_audience_specs').val('[' + adAudiences + ']');
	$('#ad_item_specs').val(JSON.stringify(adItems));
	if (adCreatives != null && adCreatives.length > 0) {
		$('#ad_creative_specs').val('[' + adCreatives + ']');
	}
}

function createAdCampaign() {
	if (checkValidation()) {
		makeAdCampaignInfo();
		if ($('#schedule_type_1').is(':checked')) {
			$('#on_schedule_date').val('[' + schedule_json() + ']');
		} else {
			$('#on_schedule_date').val('');
		}
		$('#campaign_data').submit();
	}
}

function showModifyCampaignInfo() {
	removeErrorMark();
	MODIFY_VALUE = {
		startDate : $('#campaign_start_date').val(),
		startHour : $('#campaign_start_hour').val(),
		startMin : $('#campaign_start_min').val(),
		endDate : $('#campaign_end_date').val(),
		endHour : $('#campaign_end_hour').val(),
		endMin : $('#campaign_end_min').val(),
		budget : $('#budget').val(),
		scheduleType : $(':radio[name="scheduleType"]:checked').val()
	}
	$('#modify_table_div').addClass('_modify');
}

function cancelModifyCampaignInfo() {
	removeErrorMark();
	$('.ui-selectee').removeClass('ui-selected');
	$('.ui-selectee').attr('prev', 'off');
	$('#schedule_type_' + MODIFY_VALUE.scheduleType).trigger('click');
	if (MODIFY_VALUE.scheduleType == 1) {
		setSchedule($('#on_schedule_date').val());
	}
	$('#campaign_start_date').val(MODIFY_VALUE.startDate);
	$('#campaign_start_hour').val(MODIFY_VALUE.startHour);
	$('#campaign_start_min').val(MODIFY_VALUE.startMin);
	$('#campaign_end_date').val(MODIFY_VALUE.endDate);
	$('#campaign_end_hour').val(MODIFY_VALUE.endHour);
	$('#campaign_end_min').val(MODIFY_VALUE.endMin);
	$('#budget').val(MODIFY_VALUE.budget);
	$('#delivery_option_' + $('#delivery_option').val()).trigger('click');
	$('#ad_campaign_status_select').val($('#ad_campaign_status').val()).trigger('change');
	$('#campaign_name').val($('#span_campaign_name').text());
	$('#modify_table_div').removeClass('_modify');
}

function modifyCampaignInfo() {
	removeErrorMark();
	var formValidationCheck = wise.isValidate($('#campaign_data'));
	var timeValidationCheck = checkValidation();
	if (timeValidationCheck && formValidationCheck) {
		$('#span_campaign_name').text($('#campaign_name').val());
		$('#span_budget').text($('#budget').val());
		$('#span_budget').number(true, 0);
		$('#span_delivery').attr('data-val', $(':radio[name="deliveryOption"]:checked').val());
		$('#span_delivery').text($(':radio[name="deliveryOption"]:checked').attr('data-text'));
		$('#delivery_option').val($(':radio[name="deliveryOption"]:checked').val());
		$('#schedule_type').val($(':radio[name="scheduleType"]:checked').val());
		var campaignTime = '';
		campaignTime = $('#campaign_start_date').val() + ' ' + $('#campaign_start_hour').val() + ' : ' + $('#campaign_start_min').val() + ' ~ ';
		campaignTime += $('#campaign_end_date').val() + ' ' + $('#campaign_end_hour').val() + ' : ' + $('#campaign_end_min').val();
		$('#ad_campaign_status').val($('#ad_campaign_status_select').val());
		$('#campaign_time').text(campaignTime);
		$('#modify_table_div').removeClass('_modify');
		$('#on_schedule_date').val('[' + schedule_json() + ']');
		if ($('#budget').val() != MODIFY_VALUE.budget) {
			setAdAudienceBudget($('#budget').val());
		}
	}
}

function checkScheduleType(ele) {
	var $ele = $(ele);
	if ($ele.val() == '0') {
		$('#time_table').hide();
	} else if ($ele.val() == '1') {
		$('#time_table').show();
	}
}

function dataPkSeleteCampaignDate() {
	var time = moment($('#campaign_end_date').val(), 'YYYY.MM.DD') - moment($('#campaign_start_date').val(), 'YYYY.MM.DD');
	if (time < 0) {
		$('#campaign_end_date').val($('#campaign_start_date').val());
	}
}

function openAllCreative() {
	$('#campaign_detail_table > tbody').removeClass('_fold');
}

function closeAllCreative() {
	$('#campaign_detail_table > tbody').addClass('_fold');
}

function openAndCloseAdCreative(ele) {
	var $ele = $(ele);
	if ($ele.hasClass('_fold')) {
		$ele.removeClass('_fold');
	} else {
		$ele.addClass('_fold');
	}
}

function setAdCampaignBudget() {
	var totalBudget = 0;
	$('#campaign_detail_table > tbody').each(function() {
		$(this).find('> tr').each(function() {
			if ($(this).attr('data-row') == 'total') {
				totalBudget += parseInt($('#ad_audience_budget_' + $(this).attr('data-id')).attr('data-val'));
			}
		});
	});
	$('#span_budget').text(totalBudget);
	$('#budget').val(totalBudget);
	$('#span_budget').number(true, 0);
}

function setAdAudienceBudget(val) {
	var adAudienceCount = $('#campaign_detail_table > tbody').length;
	var tmpVal = val % adAudienceCount;
	val -= tmpVal;
	$('#campaign_detail_table > tbody').each(function() {
		var $this = $(this);
		$this.find('> tr').each(function() {
			if ($this.attr('data-row') == 'total') {
				var data = JSON.parse($this.attr('data-spec'));
				var comVal = (val / adAudienceCount) + tmpVal
				data.budget = comVal;
				$('#ad_audience_budget_' + $this.attr('data-id')).attr('data-val', comVal);
				$('#ad_audience_budget_' + $this.attr('data-id')).text(comVal);
				tmpVal = 0;
				$this.attr('data-spec', JSON.stringify(data));
				$('#ad_audience_budget_' + $this.attr('data-id')).number(true, 0);
			}
		});
	});
}

function isInArray(array, search) {
	return array.indexOf(search) >= 0;
}

function checkValidation() {
	var validationCheck = true;
	var time = moment($('#campaign_end_date').val(), 'YYYY.MM.DD') - moment($('#campaign_start_date').val(), 'YYYY.MM.DD');
	if (time == 0) {
		var hourTime = moment($('#campaign_end_hour').val() + ':' + $('#campaign_end_min').val(), 'HH:mm') - moment($('#campaign_start_hour').val() + ':' + $('#campaign_start_min').val(), 'HH:mm');
		if (hourTime < 1800000) {
			$('#campaign_end_hour').addClass('parsley-error');
			$('#campaign_end_min').addClass('parsley-error');
			$('#campaign_end_date').addClass('parsley-error');
			$('#campaign_end_date').tooltip({
				animation : false,
				container : 'body',
				placement : 'top-left',
				title : 'End time must be at least 30 minutes later than the start time.'
			});
			validationCheck = false;
		}
	}
	var dateDay = moment($('#campaign_start_date').val(), 'YYYY.MM.DD') - moment(moment(dt).format('YYYY.MM.DD'));
	if (dateDay < 0) {
		$('#campaign_start_date').val(moment(dt).format('YYYY.MM.DD'));
	} else if (dateDay == 0) {
		var datetime = moment($('#campaign_start_hour').val() + ':' + $('#campaign_start_min').val(), 'HH:mm') - moment(moment(dt).format('HH:mm'));
		if (dateDay <= 0) {
			$('#campaign_start_hour').val(moment(dt).format('HH'));
			$('#campaign_start_min').val(moment(dt).format('mm'));
		}
	}

	if ($('#schedule_type_1').is(':checked') && schedule_json() == '') {
		$('#time_table').find('.day1').tooltip({
			animation : false,
			container : 'body',
			placement : 'top-left',
			title : 'Select schedule.'
		});
		$('#time_table').find('.ui-selectable').addClass('_error');
		validationCheck = false;
	}
	return validationCheck;
}