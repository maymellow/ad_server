function initVideoType() {
	$('#popup_select_video_thumbnail').parent().tooltip('destroy');
	$('#popup_select_video_thumbnail').parent().removeClass('_error');
	$('#popup_select_video_thumbnail > li').children('img').remove();
	$('#popup_select_video_thumbnail > li').removeClass('_active');
}

function popupChooseVideoImage(ele) {
	var $ele = $(ele);
	if ($ele.parent('ul').prop('disabled') == true) {
		return;
	}
	$('#popup_ad_crative_image_div_3').tooltip('destroy');
	$('#popup_ad_crative_image_div_3').removeClass('_error');
	$('#popup_select_video_thumbnail > li').children('img').remove();
	$('#popup_select_video_thumbnail > li').borderInActive();
	if ($ele.hasClass('_active')) {
		$ele.borderInActive();
	} else {
		$('#popup_image_list_3 > li').borderInActive();
		$ele.borderActive();
		popupGetChooseVideoThumbnail($ele.attr('data-img_index'));
	}
}

function popupGetChooseVideoThumbnail(file_index) {
	wise.ajax({
		type : 'GET',
		url : rootPath + '/adv/get/video/thumbnail/list.ajax',
		data : {
			file_index : file_index
		},
		success : function(data) {
			var index = 0;
			$('#popup_select_video_thumbnail > li').each(function() {
				if (!$(this).hasClass('custom-image')) {
					if (JSON.parse(data.jsonThumbnailImageUrl).length <= index) {
						return false;
					} else {
						$(this).append('<img src="' + JSON.parse(data.jsonThumbnailImageUrl)[index] + '" >');
					}
					index++;
				}
			});
		}
	});
}

function popupChooseVideoThumbnail(ele) {
	var $ele = $(ele);
	if ($ele.parent('ul').prop('disabled') == true) {
		return;
	}
	$('#popup_select_video_thumbnail').parent().tooltip('destroy');
	$('#popup_select_video_thumbnail').parent().removeClass('_error');
	if ($ele.children('img').attr('src')) {
		if ($ele.hasClass('_active')) {
			$ele.removeClass('_active');
		} else {
			$('#popup_select_video_thumbnail > li').removeClass('_active');
			$ele.addClass('_active');
		}
	} else {
		return;
	}
}

function popupChooseVideoDataInit(ele) {
	var $ele = $(ele);
	$ele.parent().attr('data-val', $ele.val());
	popupCreativeCreateTypeUseExisting(false);
	for ( var i in bandPosts) {
		if ($ele.find('option:selected').attr('data-pno') == bandPosts[i].postNo) {
			$('#popup_ad_creative_body_' + tabIndex).val(bandPosts[i].body);
			$('#popup_ad_creative_body_' + tabIndex).keyup();
			wise.ajax({
				type : 'GET',
				url : rootPath + '/api/band_post_video_info.ajax',
				createDim : false,
				beforeSend : function() {
					dimComponent.createForAjax();
				},
				data : {
					bandId : $('#popup_ad_creative_band_id').val(),
					postNo : bandPosts[i].postNo
				},
				success : function(data) {
					wise.ajax({
						url : rootPath + '/adv/creative/file/upload/url/video.ajax',
						data : {
							streamUrl : data['480p']
						},
						success : function(data) {
							if (data) {
								videoProgress(data.streamUrl);
							} else {
								alert('Video does not meet its requirements.');
							}
						}
					});
				}
			});
		}
	}
}

$('#popup_ad_creative_video_file').change(function() {
	wise.ajax({
		url : rootPath + '/adv/creative/file/upload/video.ajax',
		data : new FormData($("#popup_video_file_form")[0]),
		processData : false,
		contentType : false,
		createDim : false,
		beforeSend : function() {
			dimComponent.createForAjax();
		},
		success : function(data) {
			if (data) {
				videoProgress(data.streamUrl);
			} else {
				alert('Video does not meet its requirements.');
			}
		}
	});
});

function videoProgress(videoId) {
	wise.ajax({
		type : 'GET',
		url : rootPath + '/band/video_upload/progress.ajax',
		data : {
			videoId : videoId
		},
		createDim : false,
		success : function(data) {
			if (data.success) {
				videoComplete(data);
			} else {
				setTimeout(function() {
					videoProgress(videoId);
				}, 3000);
			}
		}
	});
}

function videoComplete(data) {
	wise.ajax({
		url : rootPath + '/adv/creative/file/video/coimplete.ajax',
		data : {
			streamUrl : data.highResUrl,
			lowStreamUrl : data.lowResUrl,
			thumbnailUrl : data.thumbnailUrl
		},
		createDim : false,
		success : function(data) {
			if (data) {
				var appendData = '';
				appendData += '<li onclick="popupChooseVideoImage(this);" data-img_index=' + data.id + '>';
				appendData += '		<input type="checkbox" />';
				appendData += '		<div class="select-image">';
				appendData += '			<img src="' + JSON.parse(data.jsonThumbnailImageUrl)[0] + '" alt="" />';
				appendData += '		</div>';
				appendData += '		<p>' + JSON.parse(data.jsonImageWidth)[0] + 'x' + JSON.parse(data.jsonImageHeight)[0] + '</p>';
				appendData += '</li>';
				$('#popup_image_list_3').prepend(appendData);
			}
		},
		complete : function() {
			dimComponent.remove();
		}
	});
}

$('#popup_ad_creative_video_thumbnail_file').change(function() {
	wise.ajax({
		url : rootPath + '/adv/creative/file/upload/video/thumbnail.ajax',
		data : new FormData($("#popup_video_thumbnail_file_form")[0]),
		processData : false,
		contentType : false,
		success : function(data) {
			if (!data) {
				alert('Video does not meet its requirements.');
			} else {
				wise.ajax({
					url : rootPath + '/adv/creative/video/thumbnail/file/save.ajax',
					data : {
						file_index : $('#popup_image_list_3 > li._active').attr('data-img_index'),
						thumbnail_url : JSON.parse(data.jsonThumbnailImageUrl)[0],
						img_url : JSON.parse(data.jsonImageUrl)[0],
						img_width : JSON.parse(data.jsonImageWidth)[0],
						img_height : JSON.parse(data.jsonImageHeight)[0]
					},
					success : function(data) {
						$('#popup_select_video_thumbnail > li').children('img').remove();
						popupGetChooseVideoThumbnail($('#popup_image_list_3 > li._active').attr('data-img_index'));
					}
				});
			}
		}
	});
});

function popupMakeVideoAdCreativeSpec(productType, id) {
	var returnArray = [];
	var returnJson = {
		sponsoredType : 0,
		description : '',
		child : [],
		creativeType : '',
	};
	returnJson.sponsoredType = parseInt($('#popup_ad_creative_sponsored_type_3').val());
	returnJson.description = $('#popup_ad_creative_body_3').val();
	returnJson.creativeType = productType + 3;
	var jsonChild = {};
	if (parseInt(productType / 10) == 1) {
		var url = '';
		switch ($('#popup_ad_creative_url_type_3_0').val()) {
		case '0':
			url = $('#popup_ad_creative_url_3_0').val();
			break;
		case '1':
			url = $('#popup_ad_creative_url_band_home_3_0 > li:first-child').attr('data-val');
			break;
		case '2':
			url = $('#popup_ad_creative_url_band_post_3_0 > select').val();
			break;
		}
		jsonChild = {
			id : id,
			video : $('#popup_image_list_3 > li._active').attr('data-img_index'),
			url : url,
			urlType : $('#popup_ad_creative_url_type_3_0').val(),
			urlTitle : $('#popup_ad_creative_one_url_title_3_0_1').val(),
			urlSubTitle : '',
			callToAction : $('#popup_ad_creative_single_image_call_to_action_3_0').val(),
			thumbnailIndex : $('#popup_select_video_thumbnail > li._active').index()
		};
	} else if (parseInt(productType / 10) == 2) {
		jsonChild = {
			id : id,
			video : $('#popup_image_list_3 > li._active').attr('data-img_index'),
			urlTitle : $('#popup_ad_creative_one_url_title_3_0_1').val(),
			urlSubTitle : '',
			callToAction : $('#popup_ad_creative_single_image_call_to_action_3_0').val(),
			thumbnailIndex : $('#popup_select_video_thumbnail > li._active').index()
		};
	}
	if ($('#popup_ad_creative_url_tile_type_3_0').val() == '2') {
		jsonChild.urlTitle = $('#popup_ad_creative_two_url_title_3_0_2').val();
		jsonChild.urlSubTitle = $('#popup_ad_creative_two_url_sub_title_3_0_2').val();
	}
	returnJson.child.push(jsonChild);
	returnArray.push(returnJson);
	return returnArray;
}

function popupValidationCheckVideoType(validationCheck) {
	if ($('#popup_ad_creative_body_3').val().length > 105) {
		validationCheck = false;
		$('#popup_ad_creative_body_3').addClass('parsley-error');
		$('#popup_ad_creative_body_3').tooltip({
			animation : false,
			container : 'body',
			placement : 'top-left',
			title : 'Maximum Characters : 105.'
		});

	}
	if ($('#popup_image_list_3 > li._active').attr('data-img_index')) {
		if (!$('#popup_select_video_thumbnail > li._active').children('img').attr('src')) {
			validationCheck = false;
			$('#popup_select_video_thumbnail').parent().addClass('_error');
			$('#popup_select_video_thumbnail').parent().tooltip({
				animation : false,
				container : 'body',
				placement : 'top-left',
				title : 'Please select image.'
			});
		}
	} else {
		validationCheck = false;
		$('#popup_ad_crative_image_div_3').addClass('_error');
		$('#popup_ad_crative_image_div_3').tooltip({
			animation : false,
			container : 'body',
			placement : 'top-left',
			title : 'Please select video.'
		});
	}
	if ($('#popup_ad_creative_url_type_3_0').val() == '2' && ($('#popup_ad_creative_url_band_post_3_0').val() == null || $('#popup_ad_creative_url_band_post_3_0').val() == '')) {
		validationCheck = false;
		$('#popup_ad_creative_url_band_post_3_0').addClass('_error');
		$('#popup_ad_creative_url_band_post_3_0').tooltip({
			animation : false,
			container : 'body',
			placement : 'top-left',
			title : 'Please select exising post.'
		});
	}
	return validationCheck;
}