function getChartData() {
	wise.ajax({
		url : rootPath + '/adv/manage/detail/campaign/chart.ajax',
		type : 'GET',
		data : {
			ad_campaign_id : $('#ad_campaign_id').val()
		},
		dataType : 'JSON',
		success : function(data) {
			makeForChart(data);
		}
	});
}

function makeForChart(data) {
	var chartData = new Array();
	var dateArray = new Array();
	var spenddata = new Array();
	for ( var i in data) {
		dateArray.push(moment(data[i].dateTime).format('MM/DD'));
		spenddata.push(data[i].spend);
	}
	chartData = {
		dateArray : dateArray,
		data : spenddata
	}
	drawChart(chartData);
}

function drawChart(data) {
	var chartOption = defaultChartOption();
	chartOption.colors = [ '#39CF6C' ];
	chartOption.chart = {
		type : 'column',
		height : '170',
		width : '370',
		style:{
     		'fontFamily':'"Roboto Condensed","Nanum Barun Gothic", "맑은 고딕", sans-serif'
         }
	};
	chartOption.xAxis = {
		categories : data.dateArray,
		tickLength : 0,
		labels : {
			rotation : 0
		},
		lineColor: '#d7d7d7'
	};
	chartOption.yAxis = {
		gridLineColor : 'transparent',
		labels : {
			enabled : false
		}
	};
	chartOption.yAxis.visible = false;
	chartOption.legend.enabled = false;
	chartOption.tooltip = {
		backgroundColor : "#404040",
		borderColor : "#404040",
		borderWidth : 1,
		shadow : false,
		useHTML : true,
		borderRadius : 10,
		style : {
			color : '#fff',
			padding : '0',
			margin : '0'
		},
		headerFormat : '',
		pointFormatter : function() {
			return '<span class="number">' + Highcharts.numberFormat(this.y, '0', '.') + '</span>';
		}
	};
	var column_temp = {
		name : '',
		pointWidth : 6,
		data : []
	};
	chartOption.series[0] = column_temp;
	chartOption.series[0].data = data.data;
	$('#ad_campaign_spend_graph').highcharts(chartOption);
}