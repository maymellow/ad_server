var OBJECTIVE_TYPE_IMAGE = 1;
var OBJECTIVE_TYPE_WEBSITE_CLICK = 2;
var OBJECTIVE_TYPE_VIDEO = 3;
var OBJECTIVE_TYPE_APP_INSTALL = 11;

var WC_CHART_DATA;
var AI_CHART_DATA;
var DATA_ARRAY;
var STATS_MAP = [ 'spend', 'impressions' ];

var COLUMN_NAME_MAP = {
	'spend' : 'Spent',
	'impressions' : 'Impression',
	'clicks' : 'Click',
	'ctr' : 'CTR',
	'cpc' : 'CPC',
	'view' : 'View',
	'vtr' : 'VTR',
	'cpv' : 'CPV',
	'install' : 'Install',
	'cpi' : 'CPI'

}

function getGraphData(endCallBack) {
	wise.ajax({
		url : rootPath + "/adv/manage/campaign/chart.ajax",
		type : 'GET',
		data : $('#search_form').serialize(),
		dataType : 'JSON',
		success : function(data) {
			WC_CHART_DATA = data.wc;
			AI_CHART_DATA = data.ai;
			DATA_ARRAY = data.dataArray;
			sendDrawAllChart();
		}
	});
}

function sendDrawAllChart() {
	STATS_MAP = [ 'spend', 'impressions', 'clicks', 'ctr', 'cpc' ];
	var chartData = sumData(WC_CHART_DATA);
	drawHighChart('wc', chartData, DATA_ARRAY);

	STATS_MAP = [ 'spend', 'impressions', 'clicks', 'ctr', 'cpc' ];
	chartData = sumData(AI_CHART_DATA);
	drawHighChart('ai', chartData, DATA_ARRAY);
}

function sumData(data) {
	var totalSumData = new Array();
	var dailySumData = new Array();
	for ( var i in DATA_ARRAY) {
		var dailySumMap = {};
		sumReportStatsData(dailySumMap, data[DATA_ARRAY[i]]);
		dailySumData.push(dailySumMap);
		sumReportStatsData(totalSumData, dailySumMap);
	}
	var forChartData = new Array();
	forChartData.dailySumData = makeForChartData(dailySumData, STATS_MAP, COLUMN_NAME_MAP);
	makeDataForChartSummary(totalSumData);
	forChartData.totalSumData = totalSumData;
	return forChartData;
}

function makeDataForChartSummary(data) {
	var completeSource = new Array();
	for ( var k in STATS_MAP) {
		var value = data[STATS_MAP[k]];
		if (STATS_MAP[k] == 'cpc' || STATS_MAP[k] == 'ctr') {
			value = calcActionValue(STATS_MAP[k], data);
		}
		data[STATS_MAP[k]] = value;
	}
}

var chartOption;
function drawHighChart(containerId, data, categoryData) {
	chartOption = defaultChartOption();
	chartOption.totalSumMap = data.totalSumData;
	chartOption.colors = [ '#39cf6c', '#F3F3F3', '#2F9D27', '#6F6F6F', '#A5DE9F' ];
	chartOption.chart = {
		height : '200',
		style:{
     		'fontFamily':'"Roboto Condensed","Nanum Barun Gothic", "맑은 고딕", sans-serif'
         }
	};
	chartOption.xAxis = {
		categories : categoryData,
		tickLength : 0,
		lineColor: '#d7d7d7'
	};
	chartOption.tooltip = {
		backgroundColor : "#404040",
		borderColor : "#404040",
		borderWidth : 1,
		shadow : false,
		useHTML : true,
		borderRadius : 10,
		style : {
			color : '#fff',
			padding : '0',
			margin : '0'
		},
		headerFormat : '',
		pointFormatter : function() {
			if ('CTR' == this.series.name) {
				return '<span>' + Highcharts.numberFormat(this.y, 2, '.') + ' %</span>';
			} else if ('CPC' == this.series.name || 'CPM' == this.series.name) {
				return '<span>' + Highcharts.numberFormat(this.y, 1, '.') + '</span>';
			} else {
				return '<span>' + Highcharts.numberFormat(this.y, 0, '.') + '</span>';
			}
		}
	};
	chartOption.legend = {
		layout : 'vertical',
		align : 'right',
		verticalAlign : 'top',
		itemMarginTop : 7,
		useHTML : true,
		symbolPadding : 5,
		margin : 50,
		padding : 0,
		y : 0,
		labelFormatter : function() {
			var count = 0;
			var rate_count = 0;
			for (var i = 0; i < this.yData.length; i++) {
				count += this.yData[i];
			}
			if ('CTR' == this.name) {
				return '<p class="legend"><span>' + this.name + '</span><span class="number">' + Highcharts.numberFormat(data.totalSumData.ctr, 2, '.') + '%</span></p>';
			} else if ('CPC' == this.name) {
				if(containerId == 'wc'){
					return '<p class="legend"><span>CPWC<span class="number">' + Highcharts.numberFormat(data.totalSumData.cpc, 1, '.') + '</span></p>';
				}else if(containerId == 'ai'){
					return '<p class="legend"><span>CPIC<span class="number">' + Highcharts.numberFormat(data.totalSumData.cpc, 1, '.') + '</span></p>';
				}
			} else if ('CPM' == this.name) {
				return '<p class="legend"><span>' + this.name + '<span class="number">' + Highcharts.numberFormat(data.totalSumData.cpm, 1, '.') + '</span></p>';
			} else if ('Spent' == this.name) {
				return '<p class="legend"><span>' + this.name + '<span class="number">' + Highcharts.numberFormat(count, 0, '.') + '</span></p>';
			} else {
				return '<p class="legend"><span>' + this.name + '<span class="number">' + Highcharts.numberFormat(count, 0, '.') + '</span></p>';
			}
		}
	};
	chartOption.plotOptions.line = {
		states : {
			hover : {
				enabled : false
			}
		}
	};
	for (var i = 0; i < data.dailySumData.length; i++) {
		var reportStats = data.dailySumData[i];
		var oppsTemp = { // 오른쪽 초기값들
			tickPixelInterval : 35, 
			gridLineColor : '#F1F1F1',
			opposite : true,
			title : {
				text : ''
			},
			labels : {
				style : {
					color : '',
					display : 'none'
				}
			},
			min : 0
		};
		var column_temp = {
			type : 'column',
			name : '',
			yAxis : '',
			zIndex : 2,
			data : [],
			min : 0
		};
		var line_temp = {
			type : 'line',
			name : '',
			yAxis : '',
			zIndex : 3,
			data : [],
			min : 0
		};
		var area_temp = {
			type : 'area',
			name : '',
			yAxis : '',
			zIndex : 1,
			data : [],
			color : '',
			min : 0,
			fillOpacity : 0.2,
			marker : {
				enabled : false
			}
		};
		switch (data.dailySumData[i].name) {
		case 'Spent':
			chartOption.series[i] = column_temp;
			chartOption.series[i].pointWidth = 6;
			break;
		case 'Impression':
			chartOption.series[i] = area_temp;
			break;
		default:
			chartOption.series[i] = line_temp;
		}
		chartOption.yAxis[i + 1] = oppsTemp;
		chartOption.series[i].name = data.dailySumData[i].name;
		chartOption.series[i].yAxis = i + 1;
		chartOption.series[i].data = data.dailySumData[i].data;
		chartOption.yAxis[i + 1].labels.style.color = chartOption.colors[i];
	}
	$('#' + containerId + '_graph').highcharts(chartOption);
}
