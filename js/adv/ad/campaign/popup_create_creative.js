$('.upload-new-creative__tab').on('click', 'a', function (e) {
    e.preventDefault();

    var _this = $(this);
    
    // _active 모양 표시
    _this
        .parent().addClass('_active')
        .siblings()
        .removeClass('_active');

    // 탭 컨텐츠 보여주기
    $(_this.attr('href')).show()
        .siblings()
        .hide();

    tabIndex = _this.parent().attr('data-val');
    initAdCreativeForm(true);
    switch(tabIndex){
	    case '2':
	    	initMultiType();
	    	break;
	    case '3':
	    	initVideoType();
	    	break;
    }
});

function initAdCreativeForm(creativeTypeCheck){
	if(creativeTypeCheck){
		$('#popup_ad_creative_create_new_' + tabIndex).prop('checked', true);
	}
	popupCreativeCreateTypeUseExisting(false);
	$('#popup_ad_creative_use_existing_select_div_'+tabIndex).find('.select2:first').tooltip('destroy');
	$('#popup_ad_creative_use_existing_select_div_' + tabIndex).hide();
	$('#popup_ad_creative_use_existing_select_div_' + tabIndex).attr('data-val', '-1');
	$('#popup_ad_creative_use_existing_select_div_' + tabIndex + ' > select').children('option').remove();
	$('#popup_ad_creative_sponsored_type_' + tabIndex).select2('destroy');
	$('#popup_ad_creative_sponsored_type_' + tabIndex).val('0');
	$('#popup_ad_creative_sponsored_type_' + tabIndex).select2({
		minimumResultsForSearch: Infinity,
	    theme : 'band'
	});
	$('#popup_ad_creative_body_' + tabIndex).val('').keyup();
	$('#popup_ad_creative_body_' + tabIndex).removeClass('parsley-error');
	$('#popup_ad_creative_body_' + tabIndex).tooltip('destroy');
	$('#popup_ad_crative_image_div_' + tabIndex).tooltip('destroy');
	$('#popup_ad_crative_image_div_' + tabIndex).removeClass('_error');
	$('#popup_image_list_' + tabIndex + ' > li').borderInActive();
	$('#popup_ad_creative_url_tile_type_' + tabIndex + '_0').select2('destroy');
	$('#popup_ad_creative_url_tile_type_' + tabIndex + '_0').val('1').trigger('change');
	$('#popup_ad_creative_url_tile_type_' + tabIndex + '_0').select2({
		minimumResultsForSearch: Infinity,
		theme : 'band'
	});
	$('#popup_landing_title_wrap_' + tabIndex + '_0').children('.main-only').find('input').val('').keyup();
	$('#popup_ad_creative_single_image_call_to_action_' + tabIndex + '_0').select2('destroy');
	if($('#popup_ad_creative_produect_type').val() == '1'){
		$('#popup_ad_creative_single_image_call_to_action_' + tabIndex + '_0').val('5');
		$('#popup_ad_creative_url_type_' + tabIndex + '_0').select2('destroy');
		$('#popup_ad_creative_url_type_' + tabIndex + '_0').val('0').trigger('change');
		$('#popup_ad_creative_url_type_' + tabIndex + '_0').select2({
			minimumResultsForSearch: Infinity,
		    theme : 'band'
		});
	}else if($('#popup_ad_creative_produect_type').val() == '2'){
		$('#popup_ad_creative_single_image_call_to_action_' + tabIndex + '_0').val('4');
	}
	$('#popup_ad_creative_single_image_call_to_action_' + tabIndex + '_0').select2({
		minimumResultsForSearch: Infinity,
		theme : 'band'
	});
}

function popupInputText(ele, id, count){
	$('#'+id).text(count - $(ele).val().length);
}

$.fn.extend({ 
    borderActive: function () {
    	this.addClass('_active');
    	$(this).find('input:checkbox').eq(0).attr('checked', 'checked');
    	return this; 
    }
});

$.fn.extend({ 
    borderInActive: function () {
    	this.removeClass('_active');
    	$(this).find('input:checkbox').eq(0).removeAttr('checked');
    	return this; 
    }
});

function popupCreativeFocusCheck(){
	if($('#popup_ad_creative_use_existing_'+tabIndex).is(':checked') && $('#popup_ad_creative_use_existing_select_div_'+tabIndex + ' > select').val() == ''){
		$('#popup_band_existing_error_tooltip').show();
		$('#popup_ad_creative_use_existing_select_div_'+tabIndex).find('.select2:first').focus();
	}
}

function popupCreativeCreateTypeUseExisting(check){
	$('#tabForm' + tabIndex).find('.popup_disabled_ele').prop('disabled', check);
}

function popupChooseAdCreativeCreateType(ele){
	var _type = $(ele).attr('data-type');
	$('#popup_band_existing_error_tooltip').hide();
	initAdCreativeForm(tabIndex, false);
    switch(tabIndex){
	    case '2':
	    	initMultiType();
	    	break;
	    case '3':
	    	initVideoType();
	    	break;
    }
	$('#popup_ad_creative_use_existing_select_div_' + tabIndex + ' > select').children('option').remove();
	if(_type == 'new'){
		$('#popup_ad_creative_use_existing_select_div_' + tabIndex).hide();
		$('#popup_ad_creative_create_new_' + tabIndex).prop('checked', true);
		popupCreativeCreateTypeUseExisting(false);
	}else if(_type == 'use'){
		$('#popup_ad_creative_use_existing_' + tabIndex).prop('checked', true);
		$('#popup_ad_creative_use_existing_select_div_' + tabIndex).show();
		popupGetBandPostList($('#popup_ad_creative_use_existing_select_div_' + tabIndex + ' > select'));
		popupCreativeCreateTypeUseExisting(true);
	}
}

var bandPosts = {};
function popupGetBandPostList(ele, val){
	var $ele = $(ele);
	var type = '';
	bandPosts = {};
	switch(tabIndex){
		case '1' : type = 'singleImage';
			break;
		case '2' : type = 'multiImage';
			break;
		case '3' : type = 'video';
			break;
	}
	wise.ajax({
		type : 'GET',
		url : rootPath + '/api/band_posts.ajax',
		data : {
			bandId : $('#popup_ad_creative_band_id').val(),
			type : type
		},
		success : function(data) {
			$ele.select2('destroy');
			$ele.append('<option></option>');
			bandPosts = data;
			var appendData = '';
			for(var i in bandPosts){
				//  web url 읽어와서 value 값으로 수정해야함
				appendData += '<option value="www.naver.com' + i + '" data-pno="' + bandPosts[i].postNo + '" data-img_url="' + bandPosts[i].images[0].url + '" ';
				if(bandPosts[i].content == '' || !bandPosts[i].content){
					appendData += '>No content</option>';
				}else{
					appendData += '>' + bandPosts[i].content + '</option>';
				}
			}
			$ele.append(appendData);
			$ele.select2({
				minimumResultsForSearch: Infinity,
				templateResult: formatState,
				theme : "existing",
				placeholder : 'Select existing post'
			});
		}
	});
}

function formatState (state) {
	if (!state.id) { return state.text; }
	var $state = $(
		'<p class="existing-img"><img src="' + state.element.getAttribute('data-img_url') + '"/></p> <p class="existing-text">'+state.text + '</p>'
	);
	return $state;
}

function popupchooseBandPost(ele){
	var _val  = $(ele).parent().attr('data-val');
	$('#popup_band_existing_error_tooltip').hide();
	if(_val != '-1'){
		if(!confirm('Are you sure you want to select a different post? The current information won’t be saved.')){
			$('#popup_ad_creative_use_existing_select_div_' + tabIndex + ' > select').select2('destroy');
			$('#popup_ad_creative_use_existing_select_div_' + tabIndex + ' > select').val($(ele).parent().attr('data-val'));
			$('#popup_ad_creative_use_existing_select_div_' + tabIndex + ' > select').select2({
				minimumResultsForSearch: Infinity,
				theme : 'band'
			});
			return;
		}
	}
	switch(tabIndex){
		case '1' : 
			popupChooseSingleDataInit(ele);
			break;
		case '2' : 
			popupChooseMultiDataInit(ele);
			break;
		case '3' : 
			popupChooseVideoDataInit(ele);
			break;
	}
}

function popupSelectLandingType(ele){
	var _val = $(ele).val();
	var landingTypeCount = $(ele).attr('data-val');
	$('#popup_ad_creative_url_'+ tabIndex + '_' + landingTypeCount).val('');
	switch(_val){
		case '0' :
			$('#popup_ad_creative_url_'+ tabIndex + '_' + landingTypeCount).attr('required', true);
			$('#popup_ad_creative_url_'+ tabIndex + '_' + landingTypeCount).tooltip('destroy');
			$('#popup_ad_creative_url_'+ tabIndex + '_' + landingTypeCount).removeClass('parsley-error');
			$('#popup_ad_creative_url_'+ tabIndex + '_' + landingTypeCount).parent().show();
			$('#popup_ad_creative_url_band_home_'+ tabIndex + '_' + landingTypeCount).parent().hide();
			$('#popup_ad_creative_url_band_post_'+ tabIndex + '_' + landingTypeCount).hide();
			break;
		case '1' :
			$('#popup_ad_creative_url_'+ tabIndex + '_' + landingTypeCount).attr('required', false);
			$('#popup_ad_creative_url_band_home_'+ tabIndex + '_' + landingTypeCount).parent().show();
			$('#popup_ad_creative_url_'+ tabIndex + '_' + landingTypeCount).parent().hide();
			$('#popup_ad_creative_url_band_post_'+ tabIndex + '_' + landingTypeCount).hide();
			$('#popup_ad_creative_url_band_home_url_'+ tabIndex + '_' + landingTypeCount).text($('#popup_ad_creative_band_home_url').val());
			$('#popup_ad_creative_url_band_home_'+ tabIndex + '_' + landingTypeCount).children('li').attr('data-val', $('#popup_ad_creative_band_home_url').val());
			break;
		case '2' :
			$('#popup_ad_creative_url_'+ tabIndex + '_' + landingTypeCount).attr('required', false);
			$('#popup_ad_creative_url_band_post_'+ tabIndex + '_' + landingTypeCount).show();
			$('#popup_ad_creative_url_band_home_'+ tabIndex + '_' + landingTypeCount).parent().hide();
			$('#popup_ad_creative_url_'+ tabIndex + '_' + landingTypeCount).parent().hide();
			$('#popup_ad_creative_url_band_post_'+ tabIndex + '_' + landingTypeCount).children('select').children().remove();
			//TODO band 게시물 url 가져오는 작업 .........................
			popupGetBandPostList($('#popup_ad_creative_url_band_post_'+ tabIndex + '_' + landingTypeCount).children('select'));
			break;
	} 
}

function popupCreativeFileUpload(dataType) {
	switch(dataType){
		case 'single' :
			$('#popup_ad_creative_single_file').click();
			break;
		case 'multi' :
			$('#popup_ad_creative_multi_file').click();
			break;
		case 'video' :
//			alert('아직 지원되지 않습니다.');
			$('#popup_ad_creative_video_file').click();
			break;
		case 'video_thumbnail' : 
			if($('#popup_select_video_thumbnail').prop('disabled')==true){
				return;
			}
			if($('#popup_image_list_3 > li._active').attr('data-img_index')){
				$('#popup_ad_creative_video_thumbnail_file').click();
			}else{
				$('#popup_ad_crative_image_div_3').addClass('_error');
				$('#popup_ad_crative_image_div_3').tooltip({
					animation : false,
					container : 'body',
					placement : 'top-left',
					title : 'Please select video.'
				});
			}
		break;
	}
}

function popupSelectUrlTitle(ele){
	var _val = $(ele).val();
	var data_val = $(ele).attr('data-val');
	if(_val == '1'){
		$('#popup_landing_title_wrap_' + tabIndex + '_' + data_val).children('.main-only').show();
		$('#popup_landing_title_wrap_' + tabIndex + '_' + data_val).children('.main-sub').hide();
		$('#popup_landing_title_wrap_' + tabIndex + '_' + data_val).children('.main-only').find('input').val('').keyup();
		$('#popup_landing_title_wrap_' + tabIndex + '_' + data_val).children('.main-only').find('textarea').val('').keyup();
		$('#popup_landing_title_wrap_' + tabIndex + '_' + data_val).children('.main-only').find('input').attr('required', true);
		$('#popup_landing_title_wrap_' + tabIndex + '_' + data_val).children('.main-only').find('textarea').attr('required', true);
		$('#popup_landing_title_wrap_' + tabIndex + '_' + data_val).children('.main-only').find('input').attr('maxlength', 28);
		$('#popup_landing_title_wrap_' + tabIndex + '_' + data_val).children('.main-only').find('textarea').attr('maxlength', 28);
		$('#popup_landing_title_wrap_' + tabIndex + '_' + data_val).children('.main-sub').find('input').attr('required', false);
		$('#popup_landing_title_wrap_' + tabIndex + '_' + data_val).children('.main-sub').find('input').removeAttr('maxlength');
		$('#popup_landing_title_wrap_' + tabIndex + '_' + data_val).children('.main-only').find('input').removeClass('parsley-error');
		$('#popup_landing_title_wrap_' + tabIndex + '_' + data_val).children('.main-only').find('input').tooltip('destroy');
	}else if(_val == '2'){
		$('#popup_landing_title_wrap_' + tabIndex + '_' + data_val).children('.main-only').hide();
		$('#popup_landing_title_wrap_' + tabIndex + '_' + data_val).children('.main-sub').show();
		$('#popup_landing_title_wrap_' + tabIndex + '_' + data_val).children('.main-sub').find('input').val('').keyup();
		$('#popup_landing_title_wrap_' + tabIndex + '_' + data_val).children('.main-only').find('input').attr('required', false);
		$('#popup_landing_title_wrap_' + tabIndex + '_' + data_val).children('.main-only').find('input').removeAttr('maxlength');
		$('#popup_landing_title_wrap_' + tabIndex + '_' + data_val).children('.main-only').find('textarea').attr('required', false);
		$('#popup_landing_title_wrap_' + tabIndex + '_' + data_val).children('.main-only').find('textarea').removeAttr('maxlength');
		$('#popup_landing_title_wrap_' + tabIndex + '_' + data_val).children('.main-sub').find('input').attr('required', true);
		$('#popup_ad_creative_two_url_title_' + tabIndex + '_' + data_val+'_2').attr('maxlength', 14);
		$('#popup_ad_creative_two_url_sub_title_' + tabIndex + '_' + data_val+'_2').attr('maxlength', 128);
		$('#popup_landing_title_wrap_' + tabIndex + '_' + data_val).children('.main-sub').find('input').removeClass('parsley-error');
	}
}

function popupRemoveParsleyError(ele){
	var $ele = $(ele);
	$ele.tooltip('destroy');
	$ele.removeClass('_error');
}

function createNewAdCreativeMakeSpec(productType, id){
	var returnArray = [];
	var returnJson = {
			description:'',
			child:[],
			creativeType:'',
	};
	returnJson.description = $('#popup_ad_creative_body_'+tabIndex).val();	
	
	switch(tabIndex) {
		case '1': //single image
			returnArray = popupMakeSingleAdCreativeSpec(productType, id);
			break;
		case '2' : // 멀티
			returnArray = popupMakeMultiAdCreativeSpec(productType, id);
			break;
		case '3' : // 비디오
			returnArray = popupMakeVideoAdCreativeSpec(productType, id);
			break;
	}
	return returnArray;
}

function popupAdCreativeValidationCheck(){
	var validationCheck = true;
	validationCheck =  wise.isValidate($('#tabForm' + tabIndex));
	switch(tabIndex){
		case '1' : 
			validationCheck = popupValidationCheckSingleType(validationCheck);
			break;
		case '2' : 
			validationCheck = popupValidationCheckMultiType(validationCheck);
			break;
		case '3' : 
			validationCheck = popupValidationCheckVideoType(validationCheck);
			break;
	}
	return validationCheck;
}