function popupChooseCreatedAdCreative(ele, type) {
	var $ele = $(ele);
	$('#ad_creative_new').removeClass('_error');
	$('#ad_creative_new').tooltip('destroy');
	if ($ele.hasClass('_active')) {
		$ele.removeClass('_active');
	} else {
		$ele.addClass('_active');
	}
	if (type == 'creative') {
		popupDrawPreview();
	}
}

function popupMakeAdCreativeSpec() {
	var creativeList = new Array();
	$('#popup_new_creative_li > li').each(function() {
		if ($(this).attr('id') != 'ad_creative_new') {
			creativeList.push($(this).attr('data-spec'));
		}
	});
	$('#popup_creatied_creative_li > li').each(function() {
		if ($(this).hasClass('_active')) {
			creativeList.push($(this).attr('data-spec'));
		}
	});
	return creativeList;
}

function popupDrawPreview() {
	var creativeSpecList = popupMakeAdCreativeSpec();
	if (creativeSpecList.length < 1) {
		$('#popup_ad_creative_preview_empty').show();
		$('#popup_ad_creative_preview').hide();
		return;
	}
	wise.ajax({
		type : 'POST',
		url : rootPath + '/adv/get/creative/preview.ajax',
		dataType : 'HTML',
		data : {
			spec : '[' + creativeSpecList.toString() + ']'
		},
		success : function(data) {
			$('#popup_ad_creative_preview_empty').hide();
			$('#popup_ad_creative_preview').show();
			$('#popup_ad_creative_preview').html(data);
			previewFunction();
		}
	});
}

function popupMakeAdAudienceId() {
	var adAudienceList = new Array();
	$('#popup_audience_list > li').each(function() {
		if ($(this).find(' > input:checkbox').is(':checked')) {
			adAudienceList.push($(this).attr('data-id'));
		}
	});
	return adAudienceList;
}

function popupDeleteNewAdCreative(ele) {
	$(ele).remove();
	popupDrawPreview();
}

var p2 = $.extend({}, wisePopupComponent);
function popupCreateNewCreative() {
	$('#ad_creative_new').removeClass('_error');
	$('#ad_creative_new').tooltip('destroy');
	var popup = p2.popup('/adv/create/new/creative.popup', {
		popupComponentName : 'p2',
		addClass : 'popup-upload-new-creative',
		popupTitle : 'Upload New Creative',
		okBtnText : 'OK',
		okBtnCallBack : function() {
			return popupAdCreativeValidationCheck();
		},
		onFormOpenned : function() {
			$('#popup_ad_creative_id').val('-1');
		},
		onTaskComplete : function() {
			var returnArray = createNewAdCreativeMakeSpec(parseInt($('#ad_campaign_product_type').val()), parseInt($('#popup_create_ad_creative_id').val()));
			wise.ajax({
				type : 'POST',
				url : rootPath + '/adv/make/creative/js_json.ajax',
				dataType : 'JSON',
				data : {
					spec : JSON.stringify(returnArray)
				},
				success : function(data) {
					popupDrawNewAdCreative(data);
					popupDrawPreview();
				}
			});
		}
	}, {
		productType : parseInt($('#ad_campaign_product_type').val() / 10),
		app_id : $('#popup_ad_campaign_app_id').val()
	});
}

var p3 = $.extend({}, wisePopupComponent);
function popupEditNewAdCreative(ele) {
	var $ele = $(ele);
	var popup = p3.popup('/adv/create/new/creative.popup', {
		popupComponentName:'p3',
		addClass : 'popup-upload-new-creative',
		popupTitle : 'Edit Creative',
		okBtnText : 'OK',
		okBtnCallBack : function() {
			return popupAdCreativeValidationCheck();
		},
		onFormOpenned : function() {
			$('#popup_ad_creative_new_count').val(1);
		},
		onTaskComplete : function() {
			var returnArray = createNewAdCreativeMakeSpec(parseInt($('#popup_ad_creative_produect_type').val())*10, $ele.attr('data-id'));
			wise.ajax({
				type : 'POST',
				url : rootPath + '/adv/make/creative/js_json.ajax',
				dataType : 'JSON',
				data : {
					spec : JSON.stringify(returnArray)
				},
				success : function(data) {
					$('#popup_new_ad_creative'+$ele.attr('data-id')).attr('data-spec', data[0].htmlEle);
					$('#popup_new_ad_creative_img'+$ele.attr('data-id')).attr('src', JSON.parse(data[0].adCreative.jsonImageUrl)[0]);
					popupDrawPreview();
				}
			});
		}
	}, {
		ad_creative_spec : $ele.attr('data-spec'),
		app_id : $('#popup_ad_campaign_app_id').val()
	});
}

function popupDrawNewAdCreative(val) {
	var adCreativeId = -1;
	var appendData = '';
	for ( var i in val) {
		appendData += '<li class="item" id="popup_new_ad_creative' + val[i].adCreative.id + '" data-id="' + val[i].adCreative.id + '" data-spec="' + val[i].htmlEle + '">';
		appendData += '		<div class="image">';
		appendData += '			<img id="popup_new_ad_creative_img' + val[i].adCreative.id + '" src="' + JSON.parse(val[i].adCreative.jsonImageUrl)[0] + '" alt="">';
		appendData += '		</div>';
		appendData += '		<div class="button__wrap">';
		appendData += '			<button type="button" class="btn-modify" onclick="popupEditNewAdCreative($(this).parent().parent());">Modify</button>';
		appendData += '			<button type="button" class="btn-close" onclick="popupDeleteNewAdCreative($(this).parent().parent());">close</button>';
		appendData += '		</div>';
		appendData += '</li>';
	}
	adCreativeId = val[i].adCreative.id - 1;
	$('#popup_create_ad_creative_id').val(adCreativeId);
	$('#popup_new_creative_li').append(appendData);
	$('#ad_creative_new').appendTo($('#popup_new_creative_li'));
}


// TODO 왜 있는 함수 인지 모르겠네!
function learnRegExp(s) {
	var regexp = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
	var regexp2 = /^(http|https|mobileapp):\/\/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;

	var regexp3 = /:\/\//;
	var s1 = regexp.test(s);
	var s2 = regexp2.test(s);
	var s3 = regexp3.test(s);

	var result = '';
	if (s1 || s2) {
		result = true;
	} else {
		if (type == '41' || type == '43' || type == '45' || type == '46') {
			if (s3) {
				result = true;
			} else {
				result = false;
			}
		} else {
			result = false;
		}
	}
	return result;
}

function popupAdAudienceIdCheck() {
	$('#popup_audience_list').parent().removeClass('_error');
	$('#popup_audience_list').parent().tooltip('destroy');
}

function popupNewAdCreativeValidationCheck() {
	var validationCheck = true;
	var adCreativeSize = 0;
	var adAudienceSize = 0;
	$('#popup_audience_list > li').each(function() {
		if ($(this).find(' > input:checkbox').is(':checked')) {
			adAudienceSize++;
		}
	});
	adCreativeSize += $('#popup_new_creative_li > li').length;
	adCreativeSize += $('#popup_creatied_creative_li > li._active').length;
	if (adAudienceSize < 1) {
		$('#popup_audience_list').parent().addClass('_error');
		$('#popup_audience_list').parent().tooltip({
			animation : false,
			container : 'body',
			placement : 'top-left',
			title : 'Please select audience you want to add.'
		});
		validationCheck = false;
	}
	if (adCreativeSize < 2) {
		$('#ad_creative_new').addClass('_error');
		$('#ad_creative_new').tooltip({
			animation : false,
			container : 'body',
			placement : 'right',
			title : 'Please add creative.'
		});
		validationCheck = false;
	}
	return validationCheck;
}