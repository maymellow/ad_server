var STATS_ARRAY = [ 'website', 'appInstall' ];
var COLUMN_NAME_MAP = {
	'image' : 'Image',
	'website' : 'Website Click',
	'video' : 'Video',
	'appInstall' : 'App Install'
}
var ORDER_ARRAY;

function downloadXlsx(){
	$('#export_data').submit();
}

function exportDataSetting() {
	$('#export_search_start_date').val($('#search_start_date').val());
	$('#export_search_end_date').val($('#search_end_date').val());
	$('#export_view_type').val($('input[name=viewType]:checked').val());
}

function searchData() {
	exportDataSetting();
	getSalesStatusData();
}

function getSalesStatusData() {
	var viewType = $('input[name=viewType]:checked').val();
	wise.ajax({
		url : rootPath + "/adv_r/sales/status/data.ajax",
		type : 'POST',
		data : $('#search_form').serialize(),
		dataType : 'JSON',
		success : function(data) {
			reprocessingData(data);
		}
	});
}

function reprocessingData(data) {
	var totalSumData = {};
	for ( var a in data.adInsightMap) {
		sumReportStatsData(totalSumData, data.adInsightMap[a]);
	}
	data['totalData'] = totalSumData;
	drawSalesTable(data);
	drawChart(data);
}

function drawSalesTable(data) {
	defaultChartOption();
	var appendTable = '<tfoot>';
	appendTable += '		<tr>';
	appendTable += '			<td class="date"><span>';
	appendTable += '				Total';
	appendTable += '			</span></td>';
	appendTable += '			<td class="total-sales"><span>';
	appendTable += Highcharts.numberFormat(data.totalData.totalSales, '0', '.');
	appendTable += '			</span></td>';
	appendTable += '			<td class="website"><span>';
	appendTable += Highcharts.numberFormat(data.totalData.website, '0', '.');
	appendTable += '			</span></td>';
	appendTable += '			<td class="app-install"><span>';
	appendTable += Highcharts.numberFormat(data.totalData.appInstall, '0', '.');
	appendTable += '			</span></td>';
	appendTable += '			<td class="service"><span>';
	appendTable += Highcharts.numberFormat(data.totalData.service, '0', '.');
	appendTable += '			</span></td>';
	appendTable += '		</tr>';
	appendTable += '</tfoot>';
	appendTable += '<tbody>';
	for (var dateIndex = data.dateArray.length - 1; dateIndex >= 0; dateIndex--) {
		var date = data.dateArray[dateIndex];
		appendTable += '		<tr>';
		appendTable += '			<td class="date"><span>';
		appendTable += date;
		appendTable += '			</span></td>';
		appendTable += '			<td class="total-sales"><span>';
		appendTable += Highcharts.numberFormat(data.adInsightMap[date].totalSales, '0', '.');
		appendTable += '			</span></td>';
		appendTable += '			<td class="website"><span>';
		appendTable += Highcharts.numberFormat(data.adInsightMap[date].website, '0', '.');
		appendTable += '			</span></td>';
		appendTable += '			<td class="app-install"><span>';
		appendTable += Highcharts.numberFormat(data.adInsightMap[date].appInstall, '0', '.');
		appendTable += '			</span></td>';
		appendTable += '			<td class="service"><span>';
		appendTable += Highcharts.numberFormat(data.adInsightMap[date].service, '0', '.');
		appendTable += '			</span></td>';
		appendTable += '		</tr>';
	}
	appendTable += '</tbody>';

	if ($('#status_table').hasClass('dataTable')) {
		$('#status_table').DataTable().destroy();
	}
	$('#status_table > tbody').remove();
	$('#status_table > tfoot').remove();
	$('#status_table').append(appendTable);
	var table = $('#status_table').dataTable({
		'bFilter' : false,
		"order" : [ [ 0, "desc" ] ],
		'info' : false,
		'paging' : false,
		'bLengthChange' : false,
		'retrieve' : true,
		'autoWidth' : false
	});
}

function drawChart(data) {
	var totalSalesData = makeForTotalSalesChartData(data);
	var dateArray = new Array();
	for ( var i in data.dateArray) {
		if ($('input[name=viewType]:checked').val() == 'month') {
			dateArray.push(moment(data.dateArray[i]).format('YYYY/MM'));
		} else {
			dateArray.push(moment(data.dateArray[i]).format('MM/DD'));
		}
	}
	var chartData = {
		insightData : makeForDailyDataAboutOrder(makeForChartData(data.adInsightMap, STATS_ARRAY, COLUMN_NAME_MAP)),
		dateArray : dateArray
	}
	drawTotalSalesChart(totalSalesData);
	drawDailySalesChart(chartData);
}
function makeForTotalSalesChartData(data) {
	var allTotalData = 0;
	for ( var i in data.totalData) {
		if (STATS_ARRAY.indexOf(i) > -1) {
			allTotalData += data.totalData[i];
		}
	}
	var tmpArray = [];
	for ( var i in STATS_ARRAY) {
		var value = 0;
		if (data.totalData[STATS_ARRAY[i]] != 0 && allTotalData != 0) {
			value = Math.round(data.totalData[STATS_ARRAY[i]] / allTotalData * 100)
		}
		tmpArray.push({
			key : STATS_ARRAY[i],
			value : value,
			realValue : data.totalData[STATS_ARRAY[i]]
		});
	}
	tmpArray.sort(function(a, b) {
		return (a.value > b.value) ? -1 : (a.value < b.value) ? 1 : 0;
	});
	var totalSalesChart = new Array();
	ORDER_ARRAY = new Array;
	for ( var i in tmpArray) {
		totalSalesChart.push({
			name : tmpArray[i].value.toFixed(1) + '%',
			subName : COLUMN_NAME_MAP[tmpArray[i].key],
			realValue : tmpArray[i].realValue,
			y : tmpArray[i].value
		});
		ORDER_ARRAY.push(COLUMN_NAME_MAP[tmpArray[i].key]);
	}
	return totalSalesChart;
}
function makeForDailyDataAboutOrder(dailyData) {
	var completeData = new Array();
	for ( var i in ORDER_ARRAY) {
		for ( var dataI in dailyData) {
			if (dailyData[dataI].name == ORDER_ARRAY[i]) {
				completeData.push(dailyData[dataI]);
			}
		}
	}
	return completeData;
}
function drawTotalSalesChart(data) {
	var chartOption = defaultChartOption();
	chartOption.colors = [ '#26904a', '#144224', '#909090', '#e1e1e1' ];
	chartOption.chart = {
		style : {
			'fontFamily' : '"Roboto Condensed","Nanum Barun Gothic", "맑은 고딕", sans-serif'
		},
		backgroundColor : "transparent",
		plotBackgroundColor : null,
		plotBorderWidth : null,
		plotShadow : false,
		events : {
			load : function() {
				var bgX = this.plotLeft + 10;
				var bgY = 3;
				var _style = this.container.style;
				_style.backgroundImage = 'url(' + rootPath + '/images/bg_chart_ededed_188x188.png)', _style.backgroundRepeat = 'no-repeat', _style.backgroundPosition = bgX + 'px 4px'
			}
		},
		type : 'pie',
		width : '360',
		height : '200'
	};
	chartOption.credits.enabled = false;
	chartOption.exporting.enabled = false;
	chartOption.tooltip = {
		backgroundColor : "#404040",
		borderColor : "#404040",
		borderWidth : 1,
		shadow : false,
		useHTML : true,
		borderRadius : 10,
		style : {
			color : '#fff',
			padding : '0',
			margin : '0'
		},
		headerFormat : '',
		pointFormatter : function() {
			return '<span class="chart-title">' + this.subName + '</span><span class="number">' + Highcharts.numberFormat(this.realValue, '0', '.') + '</span><span class="percentage">' + this.name + '</span>';
		}
	};
	chartOption.legend = {
		layout : 'vertical',
		align : 'right',
		verticalAlign : 'middle',
		useHTML : true,
		symbolRadius : 6,
		symbolWidth : 12,
		symbolHeight : 12,
		itemMarginTop : 10,
		x : 15,
		labelFormatter : function() {
			return this.subName;
		}
	};
	chartOption.plotOptions.pie = {
		size : 190,
		cursor : 'pointer',
		allowPointSelect : false,
		dataLabels : {
			format : '{y}%',
			enabled : true,
			distance : -33,
			style : {
				fontSize : '13px',
				color : "#313131"
			}
		},
		showInLegend : true,
		states : {
			hover : {
				enabled : false
			}
		}
	};
	chartOption.series[0] = {
		innerSize : '86%',
		colorByPoint : true,
		data : data
	}
	$('#total_sales_graph').highcharts(chartOption);
}
function drawDailySalesChart(data) {
	var chartOption = defaultChartOption();
	chartOption.colors = [ '#26904a', '#144224', '#909090', '#e1e1e1' ];
	chartOption.chart = {
		type : 'column',
		height : '200',
		style : {
			'fontFamily' : '"Roboto Condensed","Nanum Barun Gothic", "맑은 고딕", sans-serif'
		}
	};
	chartOption.xAxis = {
		categories : data.dateArray,
		tickLength : 0,
		labels : {
			rotation : 0
		},
		lineColor: '#d7d7d7'
	};
	chartOption.yAxis = {
		gridLineColor : 'transparent',
		labels : {
			enabled : false
		}
	};
	chartOption.yAxis.visible = false;
	chartOption.legend.enabled = false;
	chartOption.tooltip = {
		backgroundColor : "#404040",
		borderColor : "#404040",
		borderWidth : 1,
		shadow : false,
		useHTML : true,
		borderRadius : 10,
		style : {
			color : '#fff',
			padding : '0',
			margin : '0'
		},
		headerFormat : '',
		pointFormatter : function() {
			return '<span class="chart-title">' + this.series.name + '</span><span class="number">' + Highcharts.numberFormat(this.y, '0', '.') + '</span><span class="percentage">' + (this.y / this.stackTotal * 100).toFixed(1) + '%</span>';
		}
	};
	$.each(data.insightData, function(i) {
		var column_temp = {
			name : '',
			pointWidth : 6,
			data : []
		};
		chartOption.series[i] = column_temp;
		chartOption.series[i].name = data.insightData[i].name;
		chartOption.series[i].data = data.insightData[i].data;
	});
	$('#sales_graph').highcharts(chartOption);
}