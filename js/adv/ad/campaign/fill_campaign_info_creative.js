var ADCREATIVEID = -1;
function chooseAdCreatedCreative(ele){
	var $ele = $(ele);
	$('#ad_creative_add_button').tooltip('destroy');
	$('#ad_creative_add_button').removeClass('_error');
	if($ele.hasClass('_active')){
		$ele.borderInActive();
	}else{
		if(makeAdCreativeSpec().length >= 6){
			alert('creative 6개 이상 ㄴㄴ');
			return;
		}
		$ele.borderActive();
	}
	if(makeAdCreativeSpec().length >= 6){
		$('#ad_creative_add_button').hide();
	}else{
		$('#ad_creative_add_button').show();
	} 
	drawPreview();
}

function drawNewAdCreatives(data){
	var appendData = '';
	for(var i in data){
		appendData += '<li class="item" id="ad_creative_' + data[i].adCreative.id + '" data-id="' + data[i].adCreative.id + '" data-spec="' + data[i].htmlEle + '">';
		appendData += '		<div class="image">';
		appendData += '			<img id="new_ad_creative_img' + data[i].adCreative.id + '" src ="' + JSON.parse(data[i].adCreative.jsonImageUrl)[0] + '" alt=""/>';
		appendData += '		</div>';
		appendData += '		<div class="button__wrap">';
		appendData += '			<button type="button" class="btn-modify" onclick="showEditAdCreative($(this).parent().parent());">Modify</button>';
		appendData += '			<button type="button" class="btn-close" onclick="deleteNewAdCreativeEle($(this).parent().parent());">close</button>';
		appendData += '		</div>';
		appendData += '</li>';
		ADCREATIVEID = data[i].adCreative.id;
	}
	ADCREATIVEID--;
	$('#ad_creative_ul').append(appendData);
	$('#ad_creative_add_button').appendTo($('#ad_creative_ul'));
	if(makeAdCreativeSpec().length >= 6){
		$('#ad_creative_add_button').hide();
	}
}

function drawNewAdCreative(data){
	var appendData = '';
		appendData += '<li class="item" id="ad_creative_' + data.id + '" data-id="' + data.id + '">';
		appendData += '		<div class="image">';
		appendData += '			<img id="new_ad_creative_img' + data.id + '" src ="' + JSON.parse(data.jsonImageUrl)[0] + '" alt=""/>';
		appendData += '		</div>';
		appendData += '		<div class="button__wrap">';
		appendData += '			<button type="button" class="btn-modify" onclick="showEditAdCreative($(this).parent().parent());">Modify</button>';
		appendData += '			<button type="button" class="btn-close" onclick="deleteNewAdCreativeEle($(this).parent().parent());">close</button>';
		appendData += '		</div>';
		appendData += '</li>';
	ADCREATIVEID = data.id;
	ADCREATIVEID--;
	$('#ad_creative_ul').append(appendData);
	$('#ad_creative_'+data.id).attr('data-spec', JSON.stringify(data));
	$('#ad_creative_add_button').appendTo($('#ad_creative_ul'));
	if(makeAdCreativeSpec().length >= 6){
		$('#ad_creative_add_button').hide();
	}
}

function deleteNewAdCreativeEle(ele){
	$(ele).remove();
	if(makeAdCreativeSpec().length < 6){
		$('#ad_creative_add_button').show();
	}
	drawPreview();
}

function drawPreview() {
	var creativeSpecList = makeAdCreativeSpec();
	if(creativeSpecList.length <1){
		$('#ad_creative_preview_empty').show();
		$('#ad_creative_preview').hide();
	}else{
		wise.ajax({
			type : 'POST',
			url : rootPath + '/adv/get/creative/preview.ajax',
			dataType : 'HTML',
			data : {
				spec : '[' + creativeSpecList.toString() + ']'
			},
			success : function(data) {
				$('#ad_creative_preview_empty').hide();
				$('#ad_creative_preview').show();
				$('#ad_creative_preview').html(data);
				previewFunction();
			}
		});
	}
}

function makeAdCreativeSpec(){
	var creativeSpecs = new Array();
	$('#ad_creative_ul > li').each(function(){
		if($(this).attr('id') != 'ad_creative_add_button'){
			creativeSpecs.push($(this).attr('data-spec'));
		}
	})
	$('#ad_creative_library_ul > li').each(function(){
		if($(this).hasClass('_active')){
			creativeSpecs.push($(this).attr('data-spec'));
		}
	});
	return creativeSpecs;
}

function showEditAdCreative(ele){
	var popup = wisePopupComponent.popup('/adv/create/new/creative.popup', {
		addClass : 'popup-upload-new-creative',
		popupTitle : 'Edit Creative',
		okBtnText : 'OK',
		okBtnCallBack : function() {
			return popupAdCreativeValidationCheck();
		},
		onFormOpenned : function() {
			$('#popup_ad_creative_new_count').val(1);
		},
		onTaskComplete : function() {
			var returnArray = createNewAdCreativeMakeSpec(productType, $(ele).attr('data-id'));
			wise.ajax({
				type : 'POST',
				url : rootPath + '/adv/make/creative/js_json.ajax',
				dataType : 'JSON',
				data : {
					spec : JSON.stringify(returnArray)
				},
				success : function(data) {
					$('#ad_creative_'+$(ele).attr('data-id')).attr('data-spec', data[0].htmlEle);
					$('#new_ad_creative_img'+$(ele).attr('data-id')).attr('src', JSON.parse(data[0].adCreative.jsonImageUrl)[0]);
					drawPreview();
				}
			});
		}
	}, {
		ad_creative_spec : $(ele).attr('data-spec'),
		productType : parseInt(productType/10)
	});
}

function showCreateNewAdCreative(){
	$('#ad_creative_add_button').tooltip('destroy');
	$('#ad_creative_add_button').removeClass('_error');
	if(makeAdCreativeSpec().length >= 6){
		alert('creative 6개 이상 ㄴㄴ');
		return;
	}
	var popup = wisePopupComponent.popup('/adv/create/new/creative.popup', {
		addClass : 'popup-upload-new-creative',
		popupTitle : 'Upload New Creative',
		okBtnText : 'OK',
		okBtnCallBack : function() {
			return popupAdCreativeValidationCheck();
		},
		onFormOpenned : function() {
			$('#popup_ad_creative_new_count').val(6-makeAdCreativeSpec().length);
			$('#popup_ad_creative_id').val('-1');
		},
		onTaskComplete : function() {
			var returnArray = createNewAdCreativeMakeSpec(productType, ADCREATIVEID);
			wise.ajax({
				type : 'POST',
				url : rootPath + '/adv/make/creative/js_json.ajax',
				dataType : 'JSON',
				data : {
					spec : JSON.stringify(returnArray)
				},
				success : function(data) {
					drawNewAdCreatives(data);
					drawPreview();
				}
			});
		}
	},{
		productType : parseInt(productType/10)
	});
}