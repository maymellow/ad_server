function initMultiType(){
	var callToActionInitVal = 5;
	if($('#popup_ad_creative_produect_type').val() == '2'){
		callToActionInitVal = 4;
	}
	$('#popup_choose_multi_data > tr').each(function(){
		var trCount = $(this).attr('data-count');
		if(parseInt(trCount) < 3){
			$(this).css('left', '0px');
			$('#popup_select_multi_image_2_'+trCount).tooltip('destroy');
			$('#popup_select_multi_image_2_'+trCount).removeClass('_error');
			$('#popup_select_multi_image_2_'+trCount).children('img').remove();
			$('#popup_ad_creative_url_tile_type_2_'+trCount).select2('destroy');
			$('#popup_ad_creative_url_tile_type_2_'+trCount).val('1').trigger('change');
			$('#popup_ad_creative_url_tile_type_2_'+trCount).select2({
				minimumResultsForSearch: Infinity,
				theme : 'band'
			});
			$('#popup_landing_title_wrap_2_'+trCount).children('.main-only').find('input').val('').keyup();
			$('#popup_ad_creative_single_image_call_to_action_2_'+trCount).select2('destroy');
			$('#popup_ad_creative_single_image_call_to_action_2_'+trCount).val(callToActionInitVal);
			if($('#popup_ad_creative_produect_type').val() == '1'){
				$('#popup_ad_creative_url_type_2_'+trCount).select2('destroy');
				$('#popup_ad_creative_url_type_2_'+trCount).val('0').trigger('change');
				$('#popup_ad_creative_url_type_2_'+trCount).select2({
					minimumResultsForSearch: Infinity,
				    theme : 'band'
				});
			}
			$('#popup_ad_creative_single_image_call_to_action_2_'+trCount).select2({
				minimumResultsForSearch: Infinity,
				theme : 'band'
			});
		}else{
			$(this).remove();
		}
	});
	$('#popup_multi_apply').prop('checked', true);
	$('#popup_multi_apply').val(callToActionInitVal);
	_index = 0;
	$('#btn-next').hide();
	$('#btn-prev').hide();
}

function popupChooseMultiDataInit(ele){
//	$(ele).parent().attr('data-val', $(ele).val());
//	popupCreativeCreateTypeUseExisting(false);
//	for(var i in bandPosts){
//		if($(ele).find('option:selected').attr('data-bid') == bandPosts[i].postId){
//			$('#popup_ad_creative_body_'+tabIndex).val(bandPosts[i].body);
//			$('#popup_ad_creative_body_'+tabIndex).keyup();
//			for(var k in bandPosts[i].photos){
//				wise.ajax({
//					url : rootPath + '/adv/creative/file/upload/url.ajax',
//					data : {
//						url : bandPosts[i].photos[k].photoUrl
//					},
//					success : function(data) {
//						var appendData = '';
//						appendData += '<li onclick="popupChooseMultiImage(this);" data-img_index=' + data.id + '>';
//						appendData += '		<div class="select-image">';
//						appendData += '			<img src="' + JSON.parse(data.jsonThumbnailImageUrl)[0] + '" alt="" />';
//						appendData += '		</div>';
//						appendData += '		<p>' + JSON.parse(data.jsonImageWidth)[0] + 'x' + JSON.parse(data.jsonImageHeight)[0] + '</p>';
//						appendData += '</li>';
//						$('#popup_image_list_2').prepend(appendData);
//					}
//				});
//			}
//		}
//	}
}

var imgIdPreFix = 'cre',
selectedImgIdPreFix = 'cloneImg',
trCount = 3;
function popupChooseMultiImage(ele){
	var $ele = $(ele);
	if($ele.parent('ul').prop('disabled')==true){
		return;
	}
	$('#popup_choose_multi_data').find('.image').tooltip('destroy');
	$('#popup_choose_multi_data').find('.image').removeClass('_error');
	var multiData = $('#popup_choose_multi_data > tr');
	var multiDataSize = multiData.length;
	var $cloneImg = $ele.find('img').clone();
	$cloneImg.attr('data-img_index', $ele.attr('data-img_index'));
	var drawCheck = true;
	var drawIndex = -1;
	$('#popup_choose_multi_data > tr').each(function(){
		var dataIndex = $(this).attr('data-count');
		trCount = parseInt(dataIndex);
		if(!$('#popup_select_multi_image_2_'+dataIndex).find('img').attr('src')){
			$('#popup_select_multi_image_2_'+dataIndex).append($cloneImg);
			$('#popup_select_multi_image_2_'+dataIndex).addClass('_selected');
			drawCheck = false;
			drawIndex++;
			return false;
		}
		drawIndex++;
	});
	
	if(drawCheck){
		if(multiDataSize >= 5){
			alert('You can select up to 5 images');
		}else{
			trCount++;
			var appendData = '<tr data-count="' + trCount + '" style="left:'+multiData[0].style.left+'">';
			appendData += '		<td>';
			appendData += '			<div class="image _selected" id="popup_select_multi_image_2_' + trCount + '" onclick="popupChooseMultiImageDelete(this);">';
			appendData += '				<button type="button" class="btn-delete" onclick="popupDeleteAddMultiData($(this).parent().parent().parent());">close</button>';
			appendData += '			</div>';
			if($('#popup_ad_creative_produect_type').val() == '1'){
				appendData += '		 <td class="landing-type__article">';
				appendData += '		 	<div class="type__wrap">';
				appendData += '		 		<div class="type__selected">';
				appendData += '		 			<select id="popup_ad_creative_url_type_2_' + trCount + '" data-val="' + trCount + '" onchange="popupSelectLandingType(this);" style="width:180px;">';
				appendData += '		 				<option value="0" selected>Website</option>';
				if($('#popup_ad_creative_band_check').val() == 'true'){
					appendData += '		 				<option value="1">Band Home</option>';
					appendData += '		 				<option value="2">Band Post</option>';
				}
				appendData += '		 			</select>';
				appendData += '		 		</div>';
				appendData += '		 		<div class="website">';
				appendData += '		 			<input type="text" id="popup_ad_creative_url_2_' + trCount + '" required pattern="/^[-a-zA-Z0-9@:%_\\+.~#?&//=]{2,256}\\.[a-z]{2,4}\\b(\\/[-a-zA-Z0-9@:%_\\+.~#?&//=]*)?/gi" />';
				appendData += '		 		</div>';
				appendData += '		 		<div class="band-home" style="display:none;">';
				appendData += '		 			<ul id="popup_ad_creative_url_band_home_2_' + trCount + '">';
				appendData += '		 				<li><span><a id="popup_ad_creative_url_band_home_url_2_' + trCount + '"></a></span></li>';
				appendData += '		 			</ul>';
				appendData += '		 		</div>';
				appendData += '		 		<div id="popup_ad_creative_url_band_post_2_' + trCount + '" style="display:none;" onclick="popupRemoveParsleyError(this);">';
				appendData += '		 			<select class="existing-post" style="width:100%">';
				appendData += '		 			</select>';
				appendData += '			 	</div>';
				appendData += '			 </div>';
				appendData += '		 </td>';
			}
			appendData += '		</td>';
			appendData += '		<td class="landing-title__article">';
			appendData += '			<div class="landing-title__wrap" id="popup_landing_title_wrap_2_' + trCount + '">';
			appendData += '				<div class="landing-title__selected">';
			appendData += '					<select data-val="' + trCount + '" id="popup_ad_creative_url_tile_type_2_' + trCount + '" onchange="popupSelectUrlTitle(this);" style="width:180px">';
			appendData += '						<option value="1" selected>Main Only</option>';
			appendData += '						<option value="2">Main and Sub</option>';
			appendData += '					</select>';
			appendData += '				</div>';
			appendData += '				<div class="main-only">';
			appendData += '					<textarea class="popup_disabled_ele" id="popup_ad_creative_one_url_title_2_' + trCount + '_1" onkeyup="wise.textCount($(this));" cols="30" rows="10" data-max_length="20" maxlength="128" required></textarea>';
			appendData += '					<span class="max-length">20</span>';
			appendData += '				</div>';
			appendData += '				<div class="main-sub" style="display:none;">';
			appendData += '					<p class="main">';
			appendData += '						<input type="text" id="popup_ad_creative_two_url_title_2_' + trCount + '_2" onkeyup="wise.textCount($(this));" data-max_length="10" maxlength="128" placeholder="Please enter main title" />';
			appendData += '						<span class="max-length">10</span>';
			appendData += '					</p>';
			appendData += '					<p class="sub">';
			appendData += '						<input type="text" id="popup_ad_creative_two_url_sub_title_2_' + trCount + '_2" onkeyup="wise.textCount($(this));" data-max_length="13" maxlength="128" placeholder="Please enter sub title" />';
			appendData += '						<span class="max-length">13</span>';
			appendData += '					</p>';
			appendData += '				</div>';
			appendData += '			</div>';
			appendData += '		</td>';
			appendData += '		<td>';
			appendData += '			<select id="popup_ad_creative_single_image_call_to_action_2_' + trCount + '" style="width: 100%;" class="call_to_action" onchange="popupSelectMultiCallToAction(this);">';
			if($('#popup_ad_creative_produect_type').val() == '1'){
				appendData += '			<option value="1">Book Now</option>';
				appendData += '			<option value="2">Contact Us</option>';
				appendData += '			<option value="3">Download</option>';
				appendData += '			<option value="5" selected>Learn More</option>';
				appendData += '			<option value="8">Shop Now</option>';
				appendData += '			<option value="9">Sign Up</option>';
				appendData += '			<option value="11">Watch More</option>';
			}else if($('#popup_ad_creative_produect_type').val() == '2'){
				appendData += '			<option value="1">Book Now</option>';
				appendData += '			<option value="3">Download</option>';
				appendData += '			<option value="4" selected>Install Now</option>';
				appendData += '			<option value="5">Learn More</option>';
				appendData += '			<option value="6">Listen Now</option>';
				appendData += '			<option value="7">Play Game</option>';
				appendData += '			<option value="8">Shop Now</option>';
				appendData += '			<option value="9">Sign Up</option>';
				appendData += '			<option value="10">Use App</option>';
				appendData += '			<option value="11">Watch More</option>';
				appendData += '			<option value="12">Watch Video</option>';
			}
			appendData += '			</select>';
			appendData += '		</td>';
			appendData += '</tr>';
			
			$('#popup_choose_multi_data').append(appendData);
			$('#popup_select_multi_image_2_'+trCount).append($cloneImg);
			if($('#popup_multi_apply').is(':checked')){
				$('#popup_ad_creative_single_image_call_to_action_2_' + trCount).val($('#popup_multi_apply').val());
			}
			$('#popup_choose_multi_data').find('select').select2({
				minimumResultsForSearch: Infinity,
			    theme : 'band'
			});
			drawIndex++;
			multiDataSize++;
		}
	}
	if(multiDataSize > 3){
		if (_index == multiDataSize-3){
			return;
		}
		if(_index < drawIndex){
			while(_index < drawIndex){
				popupMultiMoveNext();
				if (_index == multiDataSize-3){
					break;
				}
			}
		}else if(_index > drawIndex){
			while(_index > drawIndex){
				popupMultiMovePrev();
				if (_index == multiDataSize-3){
					break;
				}
			}
		}
	}
}

function popupChooseMultiImageDelete(ele){
	var $ele = $(ele);
	$ele.removeClass('_selected');
	$ele.children('img').remove();
}

function popupDeleteAddMultiData(ele){
	$(ele).remove();
	popupMultiMovePrev();
}

var _index = 0;
function buttonCheck(){
	var _total = $('#popup_choose_multi_data > tr').length;
  if (_index == 0){
      $('#btn-prev').hide();
    }
    else {
      $('#btn-prev').show();
    }
    
    if (_index == _total-3){
      $('#btn-next').hide();
    }
    else {
      $('#btn-next').show();
    }
}
	
function popupMultiMoveNext(){
	var _moveSize = $('#popup_choose_multi_data > tr:last-child').outerWidth(true);
	$('#popup_choose_multi_data > tr').animate({
        'left': '-=' + _moveSize + 'px'
    },0);
	_index++;
	buttonCheck();
}

function popupMultiMovePrev(){
	var _moveSize = $('#popup_choose_multi_data > tr:last-child').outerWidth(true);
	$('#popup_choose_multi_data > tr').animate({
        'left': '+=' + _moveSize + 'px'
    },0)
    _index--;
	buttonCheck();
}


function popupSelectMultiCallToAction(ele){
	var _val = $(ele).val();
	if($('#popup_multi_apply').is(':checked')){
		$('#popup_multi_apply').val(_val);
		$('#popup_choose_multi_data').find('.call_to_action').each(function(){
			var $this = $(this);
			$this.select2('destroy');
			$this.val(_val);
			$this.select2({
				minimumResultsForSearch: Infinity,
			    theme : 'band'
			});
		});
	}
}

$('#popup_ad_creative_multi_file').change(function(){
	wise.ajax({
		url : rootPath + '/adv/creative/file/upload/multi.ajax',
		data : new FormData($("#popup_multi_file_form")[0]),
		processData : false,
		contentType : false,
		success : function(data) {
			if (!data) {
				alert('Image does not meet its requirements.');
			} else {
				var appendData = '';
				appendData += '<li onclick="popupChooseMultiImage(this);" data-img_index=' + data.id + '>';
				appendData += '		<div class="select-image">';
				appendData += '			<img src="' + JSON.parse(data.jsonThumbnailImageUrl)[0] + '" alt="" />';
				appendData += '		</div>';
				appendData += '		<p>' + JSON.parse(data.jsonImageWidth)[0] + 'x' + JSON.parse(data.jsonImageHeight)[0] + '</p>';
				appendData += '</li>';
				$('#popup_image_list_2').prepend(appendData);
			}
		}
	})
});

function popupMakeMultiAdCreativeSpec(productType, id){
	var returnArray = [];
	var returnJson = {
			sponsoredType : 0,
			description:'',
			child:[],
			creativeType:'',
	};
	returnJson.sponsoredType = parseInt($('#popup_ad_creative_sponsored_type_2').val());	
	returnJson.description = $('#popup_ad_creative_body_2').val();	
	returnJson.creativeType = productType+2;
	$('#popup_choose_multi_data > tr').each(function(){
		var trIndex = $(this).attr('data-count');
		var jsonChild = {};
		if(parseInt(productType/10) == 1){
			var url = '';
			switch($('#popup_ad_creative_url_type_2_' + trIndex).val()){
				case '0' : 
					url = $('#popup_ad_creative_url_2_' + trIndex).val();
					break;
				case '1' : 
					url = $('#popup_ad_creative_url_band_home_2_' + trIndex + ' > li:first-child').attr('data-val');
					break;
				case '2' :
					url = $('#popup_ad_creative_url_band_post_2_' + trIndex + ' > select').val();
					break;
			}
			jsonChild = {
					id:id, 
					image:$('#popup_select_multi_image_2_' + trIndex).find('img').attr('data-img_index'), 
					url:url, 
					urlType:parseInt($('#popup_ad_creative_url_type_2_' + trIndex).val()),
					urlTitle:$('#popup_ad_creative_one_url_title_2_' + trIndex + '_1').val(),
					urlSubTitle:'', 
					callToAction:$('#popup_ad_creative_single_image_call_to_action_2_' + trIndex).val()
				};
		}else if(parseInt(productType/10) == 2){
			jsonChild = {
					id:id, 
					image:$('#popup_select_multi_image_2_' + trIndex).find('img').attr('data-img_index'), 
					urlTitle:$('#popup_ad_creative_one_url_title_2_' + trIndex + '_1').val(),
					urlSubTitle:'', 
					callToAction:$('#popup_ad_creative_single_image_call_to_action_2_' + trIndex).val()
				};
		}
		if($('#popup_ad_creative_url_tile_type_2_' + trIndex).val() == '2'){
			jsonChild.urlTitle = $('#popup_ad_creative_two_url_title_2_' + trIndex + '_2').val();
			jsonChild.urlSubTitle = $('#popup_ad_creative_two_url_sub_title_2_' + trIndex + '_2').val();
		}
		returnJson.child.push(jsonChild);
	});
	returnArray.push(returnJson);
	return returnArray;
}

function popupValidationCheckMultiType(validationCheck){
	$('#popup_choose_multi_data > tr').each(function(){
		var trCount = $(this).attr('data-count');
		if(!$('#popup_select_multi_image_2_'+trCount).children('img').attr('src')){
			validationCheck = false;
			$('#popup_select_multi_image_2_'+trCount).addClass('_error');
			$('#popup_select_multi_image_2_'+trCount).tooltip({
				animation : false,
				container : 'body',
				placement : 'top',
				title : 'Please select image.'
			});
		}
		if($('#popup_ad_creative_url_type_2_'+trCount).val() == '2' && ($('#popup_ad_creative_url_band_post_2_'+trCount).val() == null || $('#popup_ad_creative_url_band_post_2_'+trCount).val() == '')){
			validationCheck = false;
			$('#popup_ad_creative_url_band_post_2_'+trCount).addClass('_error');
			$('#popup_ad_creative_url_band_post_2_'+trCount).tooltip({
				animation : false,
				container : 'body',
				placement : 'top-left',
				title : 'Please select exising post.'
			});
		}
	});
	return validationCheck;
}