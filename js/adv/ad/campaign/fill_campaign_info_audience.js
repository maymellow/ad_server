var ADAUDIENCE_INDEX = 1;

function makeAdAudienceSpec() {
	var adAudienceSpecs = new Array();
	$('#ad_audience_ul > li').each(function() {
		if ($(this).attr('data-spec')) {
			adAudienceSpecs.push($(this).attr('data-spec'));
		}
	});
	return adAudienceSpecs;
}

function showAdAudiencePopup(ele) {
	var $ele = $(ele);
	$ele.tooltip('destroy');
	$ele.removeClass('_error');
	if (makeAdAudienceSpec().length >= 15) {
		return;
	}
	var title = 'Create New Audience';
	if ($ele.attr('data-type') == 'edit') {
		title = 'Edit Audience';
	}
	var popup = wisePopupComponent.popup('/adv/audience.popup', {
		addClass : 'popup-add-new-audience',
		popupTitle : title,
		okBtnText : 'OK',
		okBtnCallBack : function() {
			var validCheck = wise.isValidate($('#popup_audience_form'));
			var adCreativevalidCheck = popupAdAudienceAdCreativeValidationCheck('edit');
			if (validCheck && adCreativevalidCheck) {
				return true;
			}
			return false;
		},
		onFormOpenned : function() {
			if ($ele.attr('data-type') == 'new') {
				$('#popup_ad_audience_name_tr').hide();
				$('#popup_audience_name').attr('required', false);
				$('#popup_audience_age_max').val('65').trigger('change');
			}

			if (productType == 10) {
				$('#popup_augience_campaign_type').val('10');
				$('#popup_audience_bid_type > option[value="1"]').text('CPWC');
			} else if (productType == 20) {
				$('#popup_augience_campaign_type').val('20');
				$('#popup_audience_platform').select2('destroy');
				$('#popup_audience_platform > option[value="0"]').remove();
				$('#popup_audience_platform').select2({
					minimumResultsForSearch : Infinity,
					theme : 'band'
				});
				$('#popup_audience_bid_type > option[value="1"]').text('CPIC');
			}
		},
		onTaskComplete : function() {
			if ($('#potential_reach_value').val() == '') {
				$('#potential_reach_value').val('0')
			}
			if ($ele.attr('data-type') == 'new') {
				$('#ad_audience_ul > li').each(function() {
					if ($(this).attr('id') != 'ad_audience_add_btn') {
						ADAUDIENCE_INDEX = $(this).attr('data-id');
					}
				});
				ADAUDIENCE_INDEX++;
				$('#popup_ad_audience_id').val(ADAUDIENCE_INDEX);
				$('#popup_audience_name').val($('#popup_audience_age_min').val() + $('#popup_audience_age_max').val() + '_' + $('#popup_audience_gender option:selected').text() + '_' + $('#popup_audience_platform option:selected').text());
			}
			wise.ajax({
				type : 'GET',
				url : rootPath + '/adv/make/audience.ajax',
				dataType : 'JSON',
				data : $('#popup_audience_form').serialize(),
				success : function(data) {
					if ($ele.attr('data-type') == 'new') {
						drawAdAudicne(data);
					} else {
						editAdAudience(data);
					}
					if ($('#ad_audience_ul > li').length >= 16) {
						$('#ad_audience_add_btn').hide();
					}
				}
			});
		}
	}, {
		audience_spec : $ele.attr('data-spec'),
		type : 'edit',
		product_type : productType,
		app_id : appId
	});
}
function drawAdAudicne(data) {
	var genderText = adAudienceText('gender_'+data.adAudience.targeting.gender);
	var platform = adAudienceText('platform_'+data.adAudience.targeting.platform);

	var appendData = '<li class="item" id="ad_audience_li_' + data.adAudience.id + '" data-id="' + data.adAudience.id + '" data-type="edit">';
	appendData += '<ul>';
	appendData += '<li id="ad_audience_potential_reach_' + data.adAudience.id + '"><p>0 People</p></li>';
	appendData += '<li id="ad_audience_age_gender_' + data.adAudience.id + '" ><p>' + genderText + ', ' + data.adAudience.targeting.ageMin + '-' + data.adAudience.targeting.ageMax + '</p></li>';
	appendData += '<li id="ad_audience_platform_' + data.adAudience.id + '"><p>' + platform + '</p></li>';
	appendData += '</ul>';
	appendData += '<div class="button__wrap">';
	appendData += '<button type="button" class="btn-modify" onclick="showAdAudiencePopup($(this).parent().parent());">modify</button>';
	appendData += '<button type="button" class="btn-close" onclick="deleteThisEle($(this).parent().parent());">delete</button>';
	appendData += '</div>';
	appendData += '</li>';
	$('#ad_audience_ul').append(appendData);
	$('#ad_audience_li_' + data.adAudience.id).attr('data-spec', data.htmlEle);
	$('#ad_audience_add_btn').appendTo($('#ad_audience_ul'));
}

function editAdAudience(data) {
	var genderText = adAudienceText('gender_'+data.adAudience.targeting.gender);
	var platform = adAudienceText('platform_'+data.adAudience.targeting.platform);
	$('#ad_audience_li_' + data.adAudience.id).attr('data-spec', data.htmlEle);
	$('#ad_audience_potential_reach_' + data.adAudience.id).text('0 People');
	$('#ad_audience_age_gender_' + data.adAudience.id).text(genderText + ', ' + data.adAudience.targeting.ageMin + '-' + data.adAudience.targeting.ageMax);
	$('#ad_audience_platform_' + data.adAudience.id).text(platform);
}

function adAudienceText(key) {
	switch (key) {
		case 'gender_0':
			return 'All';
			break;
		case 'gender_1':
			return 'Male';
			break;
		case 'gender_2':
			return 'Female';
			break;
		case 'platform_0':
			return 'All Mobile';
			break;
		case 'platform_1':
			return 'Android';
			break;
		case 'platform_2':
			return 'IOS';
			break;
	}
}